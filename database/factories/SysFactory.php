<?php

use Faker\Generator as Faker;

$factory->define(App\Sys::class, function (Faker $faker) {

    $faker = \Faker\Factory::create('zh_CN');

    return [
        'name' => $faker->name,
        'display_name' => $faker->name,
        'description' => $faker->name
    ];
});
