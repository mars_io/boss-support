<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('customer');
        Schema::dropIfExists('customers');
        Schema::dropIfExists('customer_renter');
        Schema::dropIfExists('customer_lord');
        Schema::create('customers', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');

            $table->string('name')
                ->comment('姓名');

            $table->string('phone')
                ->nullable()
                ->comment('手机号');

            $table->unsignedTinyInteger('sex')
                ->default(1)
                ->comment('性别');

            $table->unsignedInteger('idtype')
                ->nullable()
                ->comment('证件类型');

            $table->string('idcard')
                ->nullable()
                ->comment('证件号码');

            $table->boolean('is_corp')
                ->default(false)
                ->comment('是否机构');

            $table->boolean('is_agent')
                ->default(false)
                ->comment('是否代理人');

            $table->unsignedInteger('user_id')
                ->nullable()
                ->comment('开单用户');

            $table->unsignedInteger('org_id')
                ->nullable()
                ->comment('负责部门');

            $table->string('py')
                ->nullable()
                ->comment('拼音简写');

            $table->string('pinyin')
                ->nullable()
                ->comment('拼音全写');

            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('customer_renter', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('renter_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('customer_lord', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('lord_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
