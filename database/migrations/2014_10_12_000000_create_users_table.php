<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('avatar')->nullable();
            $table->string('phone')->unique();
            $table->timestamp('phone_check_code_ttl')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('ding_user_id')->nullable();
            $table->timestamp('is_on_job')->nullable();
            $table->timestamp('is_enable')->nullable();
            $table->string('py')->nullable();
            $table->string('pinyin')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
