<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            //flow_id(工作流id),
            // flow_type(工作流类型 eg. App\Models\Reports),
            // title (待办标题)
            // begin_at (任务开始时间)
            // notice_at (任务提醒/催办时间 e.g. 任务开始时间前多长时间提醒)
            // finish_at (任务完成时间)
            // finish_type (1: 同意，2：拒绝，3：转交)
            // important_level (重要程度)
            // is_cc (是否抄送)
            // cc_user_id (转交人)
            Schema::dropIfExists('tasks');

            $table->increments('id');
            $table->unsignedInteger('flow_id');
            $table->string('flow_type');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('house_id');
            $table->string('title');
            $table->timestamp('begin_at')->nullable();
            $table->timestamp('notice_at')->nullable();
            $table->timestamp('finish_at')->nullable();
            $table->string('finish_type')->nullable();
            $table->unsignedTinyInteger('important_level');
            $table->boolean('is_cc')->default(false);
            //$table->unsignedInteger('cc_user_id')->nullable();
            $table->timestamp('read_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
