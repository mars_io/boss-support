<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateContractRenterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('contract_renter');
        Schema::create('contract_renter', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->increments('id');

            $table->string('contract_number')
                ->nullable()
                ->comment('合同编号');

            $table->unsignedInteger('rentable_id')
                ->nullable()
                ->comment('房屋id | 房间id');

            $table->string('rentable_type')
                ->nullable()
                ->comment('出租类型，整租，合租');

            $table->unsignedTinyInteger('type')
                ->nullable()
                ->comment('合同类型（e.g.:是新租，续租，etc.)');

            $table->unsignedInteger('sign_month')
                ->nullable()
                ->comment('签约月数');

            $table->unsignedInteger('sign_remainder_day')
                ->nullable()
                ->comment('签约天数(签约月数零头天数)');

            $table->timestamp('sign_at')
                ->nullable()
                ->comment('合同签约时间');

            $table->timestamp('start_at')
                ->nullable()
                ->comment('合同开始时间');

            $table->timestamp('end_at')
                ->nullable()
                ->comment('合同结束时间');

            $table->timestamp('end_real_at')
                ->nullable()
                ->comment('合同实际结束时间');

            $table->unsignedTinyInteger('end_type')
                ->nullable()
                ->comment('合同结束类型：正常到期，转租，调租，退租，清退');

            $table->unsignedInteger('end_handover_id')
                ->nullable()
                ->comment('合约退租交接人');

            $table->unsignedInteger('duration_days')
                ->nullable()
                ->comment('租房天数');

            $table->json('month_price')
                ->nullable()
                ->comment('租房月单价');

            $table->unsignedInteger('penalty_price')
                ->nullable()
                ->comment('违约金');

            $table->unsignedInteger('mortgage_price')
                ->nullable()
                ->comment('押金金额');

            $table->unsignedTinyInteger('pay_bet')
                ->nullable()
                ->comment('押月数');

            $table->unsignedInteger('purchase_way')
                ->nullable()
                ->comment('支付方式');

            $table->unsignedTinyInteger('pay_way')
                ->nullable()
                ->comment('缴费方式（e.g.: 月付，双月付，季付etc.，半年付，年付）');

            $table->json('pay_account_info')
                ->nullable()
                ->comment('转款账户信息（e.g.: 银行账户信息，存折信息，支付宝信息etc.');

            $table->boolean('is_joint')
                ->default(false)
                ->comment('是否合租');

            $table->boolean('is_agency')
                ->default(false)
                ->comment('是否中介单');

            $table->boolean('is_corp')
                ->default(true)
                ->comment('是否以公司名义签单, 0: 个人, 1: 公司');

            $table->json('agency_info')
                ->nullable()
                ->comment('中介详细信息');

            $table->json('album')
                ->nullable()
                ->comment('租房合同相关的照片');

            $table->text('remark')
                ->nullable()
                ->comment('合同备注');

            $table->json('receipt_number')
                ->nullable()
                ->comment('收据编号');

            $table->timestamp('final_payment_at')
                ->nullable()
                ->comment('尾款补齐时间');

            $table->unsignedInteger('money_sum')
                ->nullable()
                ->comment('总金额');

            $table->json('money_table')
                ->nullable()
                ->comment('分金额');

            $table->unsignedInteger('property_payer')
                ->nullable()
                ->comment('物业费付款方');

            $table->unsignedInteger('property_price')
                ->nullable()
                ->comment('物业费金额');

            $table->string('city_name')
                ->nullable()
                ->comment('城市名称');

            $table->json('doc_status')
                ->nullable()
                ->comment('资料审核状态');

            $table->json('visit_status')
                ->nullable()
                ->comment('客户回访状态');

            $table->unsignedTinyInteger('generate_from')
                ->default(1)
                ->comment('合同来源，1：喜报， 2：租赁管理');

            $table->unsignedInteger('user_id')
                ->nullable()
                ->comment('负责人，默认报备人');

            $table->unsignedInteger('org_id')
                ->nullable()
                ->comment('负责部门，默认报备部门');

            $table->unsignedInteger('sign_user_id')
                ->nullable()
                ->comment('开单人，默认报备人');

            $table->unsignedInteger('sign_org_id')
                ->nullable()
                ->comment('开单部门，默认报备部门');

            $table->string('remark_clause')
                ->nullable()
                ->comment('合同备注条款');

            $table->unsignedInteger('boss2_contract_id')
                ->nullable()
                ->comment('boss2合同id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_renter');
    }
}
