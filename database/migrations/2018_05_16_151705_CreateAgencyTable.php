<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->increments('id');

            $table->string('house_id')
                ->comment('房屋id');

            $table->unsignedInteger('contract_able_id')
                ->nullable()
                ->comment('合同id');

            $table->string('contract_able_type')
                ->nullable()
                ->comment('合同标示，收房合同或者是租房合同');

            $table->unsignedInteger('agency_price')
                ->nullable()
                ->comment('中介费');

            $table->unsignedInteger('agency_before_price')
                ->nullable()
                ->comment('之前收租报备的中介费');

            $table->string('agency_name')
                ->nullable()
                ->comment('中介名称');

            $table->string('agency_username')
                ->nullable()
                ->comment('中介联系人');

            $table->string('agency_phone')
                ->nullable()
                ->comment('中介联系电话');

            $table->json('agency_account_info')
                ->nullable()
                ->comment('付款信息');

            $table->json('album')
                ->nullable()
                ->comment('报备图片');

            $table->string('remark')
                ->nullable()
                ->comment('备注');

            $table->unsignedInteger('user_id')
                ->nullable()
                ->comment('报备人');

            $table->unsignedInteger('sign_user_id')
                ->nullable()
                ->comment('开单人');

            $table->unsignedInteger('sign_org_id')
                ->nullable()
                ->comment('开单部门');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agency', function (Blueprint $table) {
            //
        });
    }
}
