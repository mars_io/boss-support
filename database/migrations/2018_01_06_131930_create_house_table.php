<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('house');
        Schema::dropIfExists('houses');
        Schema::create('houses', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            //房源编号，户型，类型，面积，物业地址Id，座栋，门牌号，水表底数，电表底数，燃气表底数
            //电视卡号，水卡号，电卡卡号，天然气卡号，照片（水电燃气卡度数照片，产权证照片，房屋照片）
            //业主配置，是否合租房

           //id, name, building, unit, house_number, village_name, village_id, py, pinyin

            $table->string('name')
                ->nullable()
                ->comment('房屋名称 eg. 城市花园3-2-301');

            $table->string('building')
                ->nullable()
                ->comment('座栋');

            $table->string('unit')
                ->nullable()
                ->comment('单元');

            $table->string('house_number')
                ->nullable()
                ->comment('门牌号');

            $table->string('village_name')
                ->nullable()
                ->comment('物业地址');

            $table->string('village_id')
                ->nullable()
                ->comment('物业id');

            $table->unsignedInteger('room')
                ->nullable()
                ->comment('室');

            $table->unsignedInteger('hall')
                ->nullable()
                ->comment('厅');

            $table->string('area')
                ->nullable()
                ->comment("建筑面积");

            $table->unsignedInteger('floor')
                ->nullable()
                ->comment("当前楼层");

            $table->unsignedInteger('floors')
                ->nullable()
                ->comment("总楼层");

            $table->unsignedInteger('toilet')
                ->nullable()
                ->comment('卫');

            $table->json('house_res')
                ->nullable()
                ->comment('房屋资源，沙发，暖气等');

            $table->unsignedInteger('decoration')
                ->nullable()
                ->comment('装修');

            $table->unsignedInteger('house_identity')
                ->nullable()
                ->comment('房屋类型，住宅，商住两用');

            $table->unsignedInteger('rent_type')
                ->nullable()
                ->comment('出租性质');

            $table->unsignedInteger('house_grade')
                ->nullable()
                ->comment('房屋星级');

            $table->json('house_state')
                ->nullable()
                ->comment('房屋卫生情况，家电情况');


            $table->unsignedInteger('house_feature')
                ->nullable()
                ->comment('房屋特色');

            $table->unsignedInteger('suggest_price')
                ->nullable()
                ->comment('建议价格');

            $table->timestamp('lord_start_at')
                ->nullable()
                ->comment('收房开始时间');

            $table->unsignedTinyInteger('rent_start_at')
                ->nullable()
                ->comment('房屋状态 - 待出租，已出租 - 房屋出租时间');

            //签了收房合同开始后，才开始倒计时
            $table->timestamp('ready_end_at')
                ->nullable()
                ->comment('空置期结束时间');

            $table->timestamp('again_rent_at')
                ->nullable()
                ->comment('再次出租时间');

            $table->unsignedInteger('rent_end_than_days')
                ->nullable()
                ->comment('租房结束时间晚于收房结束时间');

            $table->timestamp('lord_end_at')
                ->nullable()
                ->comment('剩余合同时长');

            $table->timestamp('warning_init_at')
                ->nullable()
                ->comment('预警初始时间');

            $table->unsignedInteger('user_id')
                ->nullable()
                ->comment('负责人，默认报备人');

            $table->unsignedInteger('org_id')
                ->nullable()
                ->comment('负责部门，默认报备部门');

            $table->string('city_name')
                ->nullable()
                ->comment('房屋所在城市');

            $table->boolean('is_nrcy')
                ->default(false)
                ->comment('是否未收先租的房屋');

            $table->json('album')
                ->nullable()
                ->comment('房屋相册');

            $table->string('py')
                ->nullable()
                ->comment('拼音简写');

            $table->string('pinyin')
                ->nullable()
                ->comment('拼音全写');

            $table->unsignedInteger('boss2_house_id')
                ->nullable()
                ->comment('Boss2迁移过来的数据，在boss2中的房屋id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house');
    }
}
