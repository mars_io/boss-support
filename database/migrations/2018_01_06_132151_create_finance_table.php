<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('finance');

        //账户表
        Schema::create('finance_account', function (Blueprint $table) {
            //TODO: 科目字段个性化配置
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->increments('id');

            $table->string('name')
                ->nullable()
                ->comment('账户名称');

            $table->unsignedTinyInteger('type')
                ->nullable()
                ->comment('账户类型');

            $table->string('city')
                ->nullable()
                ->comment('城市');

            $table->string('bank')
                ->nullable()
                ->comment('银行');

            $table->string('branch')
                ->nullable()
                ->comment('支行');

            $table->unsignedInteger('init_money')
                ->nullable()
                ->comment('初始金额');

            $table->unsignedInteger('current_money')
                ->nullable()
                ->comment('当前金额');

            $table->unsignedInteger('user_id')
                ->nullable()
                ->comment('创建者id');

            $table->string('remark')
                ->nullable()
                ->comment('备注');

            $table->integer('parent_id')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
            $table->integer('depth')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });


        //科目类型表
        Schema::create('finance_subject_type', function (Blueprint $table) {
            //TODO: 科目字段个性化配置
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->increments('id');

            $table->string('name')
                ->nullable()
                ->comment('科目类型名称');

            $table->string('code')
                ->nullable()
                ->comment('科目类型代码');

            $table->unsignedInteger('user_id')
                ->nullable()
                ->comment('创建者id');

            $table->integer('parent_id')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
            $table->integer('depth')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        //科目表
        Schema::create('finance_subject', function (Blueprint $table) {
            //TODO: 科目字段个性化配置
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->increments('id');

            $table->string('name')
                ->comment('科目名称');

            $table->string('code')
                ->comment('科目代码');

            $table->unsignedTinyInteger('type')
                ->comment('科目类型');

            $table->unsignedInteger('user_id')
                ->comment('创建者id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance');
    }
}
