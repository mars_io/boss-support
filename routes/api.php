<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/****************公共接口 *****************/

Route::prefix('v1')->group(function() {
    Route::group(['namespace' => 'Api', 'middleware'=>['crossDomain', 'formatResponse']], function(){
        Route::resource('/sms', "SmsController");
    });
});

/****************认证： 第三方应用/服务认证 *****************/
Route::middleware('clientCheck')->prefix('v1')->group(function() {
    Route::group(['namespace' => 'Api', 'middleware'=>['crossDomain', 'formatResponse']], function(){

        Route::resource('users', 'UsersController');
        Route::post('users/batch', 'UsersController@batch');

        Route::resource('organizations', 'OrganizationsController');
        Route::post('organizations/batch', 'OrganizationsController@batch');
        Route::post('organizations/isChild', 'OrganizationsController@isChild');

        Route::resource('reports', 'ReportsController');

        Route::resource('systems', 'SystemsController');

        Route::resource('modules', 'ModulesController');

        Route::resource('permissions', 'PermissionController');

        Route::resource('roles', 'RoleController');

        Route::resource('files', 'FilesController');
        Route::post('files/batch', 'FilesController@batch');

        Route::resource('messages', 'MessagesController');

        Route::resource('process', 'ProcessController');
        Route::post('process/flow', 'ProcessController@flow');
        Route::post('process/cc', 'ProcessController@cc');
        Route::post('process/op', 'ProcessController@op');
        Route::post('process/compact_house', 'ProcessController@compact_house');

        Route::resource('position/type', 'PositionTypesController');
        Route::resource('positions', 'PositionsController');
        Route::post('positions/batch', 'PositionsController@batch');


        Route::resource('session', 'SessionController');

//        Route::resource('houses', 'HousesController');
//        Route::post('houses/batch', 'HousesController@batch');
//
//        Route::resource('lord', 'ContractLordController');
//
//        Route::resource('renter', 'ContractRenterController');
//
//        Route::post('lord/batch', 'ContractLordController@batch');
//        Route::put('lord/op_doc/{id}', 'ContractLordController@op_doc');
//        Route::put('lord/op_visit/{id}', 'ContractLordController@op_visit');
//
//        Route::post('renter/batch', 'ContractRenterController@batch');
//        Route::put('renter/op_doc/{id}', 'ContractRenterController@op_doc');
//        Route::put('renter/op_visit/{id}', 'ContractRenterController@op_visit');
//
//        Route::resource('customers', 'CustomersController');
//
//        Route::post('customers/batch', 'CustomersController@batch');
//
//        Route::resource('agency', 'AgencyController');

        Route::resource('comments', 'CommentsController');

        Route::resource('sns', 'SnsController');

        Route::resource('powers', 'PowersController');

        //Route::resource('charts', 'ChartsController');

        //Route::post('sync/house', 'SyncController@syncHouse');

        //Route::post('sync/contract', 'SyncController@syncContract');

    });

});

//TODO: 所有权限检查放到路由里检查（操作和数据权限）
Route::middleware('auth:api')->prefix('s1')->group(function() {
    Route::group(['namespace' => 'Api', 'middleware'=>['crossDomain', 'formatResponse']], function(){
        Route::resource('users', 'UsersController');
        Route::post('users/batch', 'UsersController@batch');

        Route::resource('organizations', 'OrganizationsController');
        Route::post('organizations/batch', 'OrganizationsController@batch');

        Route::resource('reports', 'ReportsController');

        Route::resource('systems', 'SystemsController');

        Route::resource('modules', 'ModulesController');

        Route::resource('permissions', 'PermissionController');

        Route::resource('roles', 'RoleController');

        Route::resource('files', 'FilesController');
        Route::post('files/batch', 'FilesController@batch');

        Route::resource('messages', 'MessagesController');

        Route::resource('process', 'ProcessController');
        Route::post('process/flow', 'ProcessController@flow');
        Route::post('process/cc', 'ProcessController@cc');
        Route::post('process/op', 'ProcessController@op');

        Route::resource('position/type', 'PositionTypesController');
        Route::resource('positions', 'PositionsController');

        Route::resource('session', 'SessionController');

//        Route::resource('houses', 'HousesController');
//        Route::post('houses/batch', 'HousesController@batch');
//
//        Route::resource('lord', 'ContractLordController');
//
//        Route::resource('renter', 'ContractRenterController');
//
//        Route::resource('customers', 'CustomersController');
//        Route::post('customers/batch', 'CustomersController@batch');

        Route::resource('comments', 'CommentsController');

        Route::resource('powers', 'PowersController');
        Route::get('powers/getRole/{id}', 'PowersController@getRole');
        Route::put('powers/withRole/{id}', 'PowersController@withRole');

        //Route::resource('charts', 'ChartsController');
    });

});


