<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/sns_login', 'SNSLoginController@index');

Route::get('/human', 'HumanController@index');

//Route::get('/house', 'HouseController@index');

//Route::get('/report', 'ReportsController@index');



Route::group(['prefix' => 'admin'], function () {

});
