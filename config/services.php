<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\Users::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'sms' => [
        'host_code' => env('SMS_CODE_HOST', 'sms.houses.org'),
        'host_notice'   => env('SMS_NOTICE_HOST', 'sms.houses.org'),
        'host_large'   => env('SMS_LARGE_HOST', 'sms.houses.org'),
        'method' => env('SMS_REQUEST_METHOD', 'get'),
        'code' => env('SMS_APPCODE', '7fc03b0c2a094161a8563d717f666329'),
        'from_web_name' => env('SMS_FROM_WEB_NAME', 'House Apartment'),
        'from_boss_name' => env('SMS_FROM_BOSS_NAME', 'Boss小秘书'),
    ],

    'dingtalk' => [
        //默认为Dev环境，组织架构Corp信息
        'corp_id' => env('DING_CORP_ID', 'dingd2e027985e84c5c2f4657eb6378f'),
        'corp_secret' => env('DING_CORP_SECRET', '0-xgfwIVpPN7QNLSTBXZO4jA4P2z83iDUvtfBntiLnVPoRNKa'),
        'agent_id' => env('DING_AGENT_ID', '1519717'),
        'app_id' => env('DING_APP_ID', 'dingoabecagukzyegm'),
        'app_secret' => env('DING_APP_SECRET', '8Mr8o2cLnL1vSzwziv_s-c3IcWTXXWoqSmCBcpby88iA8qy2qWsqrUj8Wa_1s'),
        //Marvin， Luxy， Wangk
        'master_id' => env('DING_MASTER_ID', ['06634441623325261', 'manager628', '112335332939321']),
    ],

];
