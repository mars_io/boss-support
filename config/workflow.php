<?php

return [

    /**************************客服回访流程*******************************/
    'visit_verify' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['visit_status']
        ],
        'supports'      => ['App\Models\ContractLord', 'App\Models\ContractRenter'],
        'places'        => [
            'draft',
            'wait_customer_service_review',
            'published',
        ],
        'transitions'   => [
            'to_customer_service_review' => [
                'node'  =>  [
                    'guard' =>  [],
                    'task'  =>  []
                ],
                'from' => 'draft',
                'to'   => 'wait_customer_service_review'
            ],
            'to_customer_service_publish' => [
                'node'  =>  [
                    'guard' =>  ['tel-officer'],
                    'task'  =>  []
                ],
                'from' => 'wait_customer_service_review',
                'to'   => 'published'
            ]
        ]
    ],

    /**************************合同资料审核流程*******************************/
    'contract_doc_verify' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['doc_status']
        ],
        'supports'      => ['App\Models\ContractLord', 'App\Models\ContractRenter'],
        'places'        => [
            'draft',
            'wait_contract_review',
            'wait_house_review',
            'published',
        ],
        'transitions'   => [
            'to_contract_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['data-online-officer']
                ],
                'from' => 'draft',
                'to'   => 'wait_contract_review'
            ],
            'to_contract_approved' => [
                'node'  =>  [
                    'guard' =>  ['data-online-officer'],
                    'task'  =>  ['data-online-officer']
                ],
                'from' => 'wait_contract_review',
                'to'   => 'wait_house_review'
            ],

            'to_contract_rejected' => [
                'node'  =>  [
                    'guard' =>  ['data-online-officer'],
                    'task'  =>  ['data-online-officer']
                ],
                'from' => ['wait_contract_review', 'wait_house_review'],
                'to'   => 'draft'
            ],

            'to_house_approved' => [
                'node'  =>  [
                    'guard' =>  ['data-online-officer'],
                    'task'  =>  ['data-online-officer']
                ],
                'from' => 'wait_house_review',
                'to'   => 'published'
            ],

            'to_house_rejected' => [
                'node'  =>  [
                    'guard' =>  ['data-online-officer'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'published',
                'to'   => 'wait_house_review'
            ],
            'to_cancelled' => [
                'from' => 'wait_contract_review',
                'to'   => 'draft'
            ],
        ]
    ],


    /**************************房屋质量报备*******************************/

    'fangwuzhiliangbaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'appraiser-officer_review',
            'appraiser-officer_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager'],

                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'

            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['appraiser-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'appraiser-officer_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],
            'to_appraiser-officer_approved' => [
                'node'  =>  [
                    'guard' =>  ['appraiser-officer'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        'control-manager',
                        'appraiser-officer',
                        'information-officer',
                        'processing-officer'
                    ]
                ],
                'from' => 'appraiser-officer_review',
                'to'   => 'published'
            ],
            'to_appraiser-officer_rejected' => [
                'node'  =>  [
                    'guard' =>  ['appraiser-officer'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'appraiser-officer_review',
                'to'   => 'appraiser-officer_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'appraiser-officer_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],

    /**************************收房报备*******************************/

    'shoufangbaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'verify-manager_review',
            'verify-manager_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
                //'to'    =>  'published'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['verify-manager']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'verify-manager_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],
            'to_verify-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['verify-manager'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        //'upload-tracking-officer',
                        //'upload-tracking-master',
                        //'contract-administration-officer',
                        //'contract-administration-master',
                        'verify-manager',
                        'fund-officer',
                        'verify-officer',
                        'information-officer',
                        'control-manager',
                        //'customer-information-input-officer',
                        //'customer-information-input-master'
                    ]
                ],
                'from' => 'verify-manager_review',
                'to'   => 'published'
            ],
            'to_verify-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['verify-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'verify-manager_review',
                'to'   => 'verify-manager_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'verify-manager_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],


    /**************************租房报备*******************************/

    'zufangbaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        //'hr-director',
                        //'upload-tracking-officer',
                        //'upload-tracking-master',
                        //'contract-administration-officer',
                        //'contract-administration-master',
                        'fund-master',
                        'fund-officer',
                        'verify-officer',
                        //'information-officer',
                        'deposit-intermediary-officer',
                        'accounts-payable-officer',
                        'verify-manager',
                        'control-manager',
                        //'customer-information-input-officer',
                        //'customer-information-input-master'
                    ]
                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],


    /**************************转租报备*******************************/

    'zhuanzubaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        //'hr-director',
                        'upload-tracking-officer',
                        'upload-tracking-master',
                        'contract-administration-officer',
                        'contract-administration-master',
                        'fund-master',
                        'fund-officer',
                        'verify-officer',
                        'deposit-intermediary-officer',
                        'accounts-payable-officer',
                        'verify-manager',
                        'information-officer',
                        'control-manager',
                        'customer-information-input-officer',
                        'customer-information-input-master'
                    ]

                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],


    /**************************续租报备*******************************/


    'xuzubaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        'hr-director',
                        'upload-tracking-officer',
                        'upload-tracking-master',
                        'contract-administration-officer',
                        'contract-administration-master',
                        'fund-master',
                        'fund-officer',
                        'verify-officer',
                        'verify-manager',
                        'deposit-intermediary-officer',
                        'accounts-payable-officer',
                        'information-officer',
                        'control-manager',
                        'customer-information-input-officer',
                        'customer-information-input-master'
                    ]

                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],


    /**************************调租报备*******************************/

    'tiaozubaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        'hr-director',
                        'upload-tracking-officer',
                        'upload-tracking-master',
                        'contract-administration-officer',
                        'contract-administration-master',
                        'fund-master',
                        'fund-officer',
                        'verify-officer',
                        'verify-manager',
                        'deposit-intermediary-officer',
                        'accounts-payable-officer',
                        'information-officer',
                        'control-manager',
                        'customer-information-input-officer',
                        'customer-information-input-master'
                    ]

                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],

    /**************************未收先租报备*******************************/

    'weishouxianzubaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        'hr-director',
                        'upload-tracking-officer',
                        'upload-tracking-master',
                        'contract-administration-officer',
                        'contract-administration-master',
                        'fund-master',
                        'fund-officer',
                        'verify-officer',
                        'verify-manager',
                        'deposit-intermediary-officer',
                        'accounts-payable-officer',
                        'information-officer',
                        'control-manager',
                        'customer-information-input-officer',
                        'customer-information-input-master'
                    ]
                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],

    /**************************未收先租确认报备*******************************/

    'weishouxianzuqueren' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        'hr-director',
                        'upload-tracking-officer',
                        'upload-tracking-master',
                        'contract-administration-officer',
                        'contract-administration-master',
                        'fund-master',
                        'fund-officer',
                        'verify-officer',
                        'verify-manager',
                        'deposit-intermediary-officer',
                        'accounts-payable-officer',
                        'information-officer',
                        'control-manager',
                        'customer-information-input-officer',
                        'customer-information-input-master'
                    ]

                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],

    /**************************续收报备*******************************/

    'xushoubaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'verify-manager_review',
            'verify-manager_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['verify-manager']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'verify-manager_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_verify-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['verify-manager'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        'hr-director',
                        'upload-tracking-officer',
                        'upload-tracking-master',
                        'contract-administration-officer',
                        'contract-administration-master',
                        'verify-manager',
                        'fund-officer',
                        'verify-officer',
                        'information-officer',
                        'control-manager',
                        'customer-information-input-officer',
                        'customer-information-input-master'
                    ]

                ],
                'from' => 'verify-manager_review',
                'to'   => 'published'
            ],
            'to_verify-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['verify-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'verify-manager_review',
                'to'   => 'verify-manager_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'verify-manager_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],


    /**************************充公报备*******************************/

    'chonggongbaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-officer_review',
            'market-marketing-manager_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-officer_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  ['hr-director', 'upload-tracking-officer', 'upload-tracking-master', 'contract-administration-officer', 'contract-administration-master', 'verify-manager', 'fund-officer', 'verify-officer', 'information-officer', 'control-manager']

                ],
                'from' => 'market-marketing-officer_review',
                'to'   => 'published'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-officer_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_cancelled' => [
                'node' =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                    ],
                'from' => 'market-marketing-officer_review',
                'to'   => 'cancelled'
            ],
        ]
    ],


    /**************************中介费报备*******************************/


    'zhongjiefeibaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'verify-manager_review',
            'verify-manager_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['verify-manager']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'verify-manager_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_verify-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['verify-manager'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  ['finance-manager', 'information-officer', 'control-manager', 'appraiser-officer']
                ],
                'from' => 'verify-manager_review',
                'to'   => 'published'
            ],
            'to_verify-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['verify-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'verify-manager_review',
                'to'   => 'verify-manager_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'verify-manager_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],


    /**************************炸单报备*******************************/


    'zhadanbaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        'fund-master',
                        'fund-officer',
                        'verify-officer',
                        'finance-manager',
                        'hr-director',
                        'upload-tracking-officer',
                        'upload-tracking-master',
                        'customer-information-input-officer',
                        'contract-administration-officer',
                        'contract-administration-master',
                        'information-officer',
                        'control-manager'
                    ]
                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],


    /**************************尾款房租报备*******************************/


    'weikuanfangzubaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  ['finance-manager', 'verify-manager', 'verify-officer', 'fund-master']
                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],


    /**************************退款报备*******************************/

    'tuikuanbaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'verify-manager_review',
            'verify-manager_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['verify-manager']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'verify-manager_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_verify-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['verify-manager'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  [
                        'finance-manager',
                        'verify-officer',
                        'rent-refund-officer'
                    ]
                ],
                'from' => 'verify-manager_review',
                'to'   => 'published'
            ],
            'to_verify-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['verify-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'verify-manager_review',
                'to'   => 'verify-manager_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'verify-manager_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],

    /**************************退租报备*******************************/

    'tuizubaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  ['fund-master', 'finance-manager', 'customer-manager', 'operation-officer', 'operation-master', 'settlement-retiring-officer', 'settlement-officer', 'information-officer', 'control-manager']
                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],

    /**************************特殊事项报备*******************************/


    'teshushixiangbaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  ['fund-master', 'verify-manager', 'fund-officer', 'information-officer', 'control-manager']
                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],

    /**************************清退报备*******************************/


    'qingtuibaobei' => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'multiple_state',
            'arguments' => ['place']
        ],
        'supports'      => ['App\Models\Process'],
        'places'        => [
            'draft',
            'market-marketing-manager_review',
            'market-marketing-manager_rejected',
            'fund-master_review',
            'fund-master_rejected',
            'published',
            'cancelled'
        ],
        'transitions'   => [
            'to_market-marketing-manager_review' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-officer'],
                    'task'  =>  ['market-marketing-manager']
                ],
                'from' => 'draft',
                'to'   => 'market-marketing-manager_review'
            ],

            'to_market-marketing-manager_approved' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['fund-master']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'fund-master_review'
            ],

            'to_market-marketing-manager_rejected' => [
                'node'  =>  [
                    'guard' =>  ['market-marketing-manager'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'market-marketing-manager_review',
                'to'   => 'market-marketing-manager_rejected'
            ],

            'to_fund-master_approved' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer'],
                    'cc'    =>  ['fund-master', 'fund-officer', 'finance-manager', 'customer-manager', 'operation-officer', 'operation-master', 'settlement-retiring-officer', 'settlement-officer', 'information-officer', 'control-manager']
                ],
                'from' => 'fund-master_review',
                'to'   => 'published'
            ],
            'to_fund-master_rejected' => [
                'node'  =>  [
                    'guard' =>  ['fund-master'],
                    'task'  =>  ['market-marketing-officer']
                ],
                'from' => 'fund-master_review',
                'to'   => 'fund-master_rejected'
            ],
            'to_cancelled' => [
                'from' => ['market-marketing-manager_review', 'fund-master_review'],
                'to'   => 'cancelled'
            ],
        ]
    ],

];