<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Power;

/**
 * Class PowerTransformer
 * @package namespace App\Transformers;
 */
class PowerTransformer extends TransformerAbstract
{

    /**
     * Transform the Power entity
     * @param App\Models\Power $model
     *
     * @return array
     */
    public function transform(Power $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
