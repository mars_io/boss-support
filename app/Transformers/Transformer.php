<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/2/002
 * Time: 12:06
 */

namespace App\Transformers;


abstract class Transformer
{

    public function transformCollection($items) {
        return array_map([$this, 'transform'], $items);
    }

    public abstract function transform($item);


}