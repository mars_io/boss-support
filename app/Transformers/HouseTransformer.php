<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\House;

/**
 * Class HouseTransformer
 * @package namespace App\Transformers;
 */
class HouseTransformer extends TransformerAbstract
{

    /**
     * Transform the House entity
     * @param App\Models\House $model
     *
     * @return array
     */
    public function transform(House $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
