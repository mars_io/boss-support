<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Guzzle\Http\Client;
use GuzzleHttp\Exception\RequestException;
use App\Models\Files;
use Illuminate\Support\Facades\Storage;

class SyncBoss2Images implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $timeout = 1800;

    /**
     * @var 主体的Model
     */
    protected $master;


    /**
     * @var 远程图片的urls数组，一维或者二维
     */
    protected $imagesUrls;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($master, $imagesUrls)
    {
        $this->master = $master;
        $this->imagesUrls = $imagesUrls;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //TODO，图片抓取及处理关联
        //1： 将远程图片抓取为文件流
        //2： 将文件流图片命名上传到七牛
        //2： 将图片关联到本地文件表中，
        //3： 将文件表中的文件关联到主体表中
        try {

            $master_class = get_class($this->master);

            if ($master_class == 'App\Models\Houses') {
                $album = $this->setImages($this->imagesUrls);
                $this->master->album = $album;
                $this->master->save();
            }

            if ($master_class == 'App\Models\ContractLord' || $master_class  == 'App\Models\ContractRenter') {
                foreach ($this->imagesUrls as $category => $images) {
                    $album[$category] = $this->setImages($this->imagesUrls[$category]);
                }
                $this->master->album = $album;
                $this->master->save();
            }

            logger("images_sync_wish", [$master_class . "-" . $this->master->id, $this->imagesUrls]);

        } catch (RequestException $e) {
            echo 'fetch fail';
        }
    }

    public function setImages($urls) {

        if (!$urls) {
            return [];
        }

        $album = [];
        $client = new \GuzzleHttp\Client(['verify' => false]);

        foreach ($urls as $url) {
            if ($url) {
                $data = $client->request('get', $url)->getBody()->getContents();
                if (!$data) {
                    logger("images_sync_failed", [$url]);
                }
                $disk = Storage::disk('qiniu');
                $fileName = "boss2_" . str_random(16) . ".jpg";
                $res = $disk->put($fileName, $data);
                if ($res) {
                    $fileUrl = getenv('QINIU_DOMAIN') . $fileName;
                    $rawName = $fileName;
                    $type = "";
                    $size = "";
                }

                $info = [
                    'bucket' => config('filesystems.disks.qiniu.bucket'),
                    'host' => config('filesystems.disks.qiniu.domain'),
                    'ext' => $type,
                    'mime' => $type,
                    'size' => $size
                ];

                $data = Files::create([
                    'name' => $fileName,
                    'raw_name' => $rawName,
                    'display_name' => $rawName,
                    'info' => $info,
                    'user_id' => $this->master->user_id,
                    'uri' => $fileUrl
                ]);
                $album[] = $data['id'];
            }

        }
        return $album;
    }
}
