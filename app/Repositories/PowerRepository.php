<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PowerRepository
 * @package namespace App\Repositories;
 */
interface PowerRepository extends RepositoryInterface
{
    //
}
