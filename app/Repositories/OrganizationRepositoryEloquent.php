<?php

namespace App\Repositories;

use App\Criteria\HasFieldCriteria;
use App\Exceptions\AbortException;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\organizationRepository;
use App\Models\Organization;
use App\Validators\OrganizationValidator;
use App\Http\Requests\OrganizationAddRequest;
use EasyDingTalk\Application;

/**
 * Class OrganizationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OrganizationRepositoryEloquent extends BaseRepository implements OrganizationRepository
{
    /**
     * @var dingding instance
     */
    public $ding;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Organization::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return OrganizationValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $options = [
            'corp_id' => config('services.dingtalk.corp_id'),
            'corp_secret' => config('services.dingtalk.corp_secret'),
        ];
        $this->ding = new Application($options);
    }

    /**
     * 获取指定组织架构id下面的子组织架构
     * @param $id
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getOrganization($id, $is_recursion)
    {

        // TODO: Implement getOrganizationById() method.
        //return $this->all(['name','id']);
        //return $this->findByField('name', '11111', ['id','name','sort']);

        //如果递归查询
        if ($is_recursion) {
            //TODO: 需要做递归查询，可以尝试使用Criteria抽象实现
            return $this->pushCriteria(new HasFieldCriteria('id', $id))->first(['parent_id']);

        } else {
            return $this->pushCriteria(new HasFieldCriteria('id', $id))->all();
        }


    }

    /**
     * 添加组织架构信息
     * @param OrganizationAddRequest $request
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function addOrganization($request)
    {
        // TODO: 添加部门，同步钉钉
        // TODO: 批量从钉钉组织架构同步部门和人员
        try {

            $ding_department_id = Organization::where('id', $request->input('parent_id'))->first()->ding_department_id;

            $result = $this->ding->department->create([
                'name' => $request->input('name'),
                'parentid' => $ding_department_id,
                'order' => $request->input('order')
            ]);

            $res = $this->create(
                [
                    'name' => $request->input('name'),
                    'parent_id' => $request->input('parent_id'),
                    'ding_department_id' => $result['id'],
                    'order' => $request->input('order'),
                    'is_enable' => $request->input('is_enable', 1)
                ]
            );

            return $res;
        } catch (\Exception $exception) {
            throw new AbortException('401', $exception->getMessage());
        }
    }

    /**
     * 修改组织架构信息（包括状态）
     * @param $request
     * @return mixed|void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function setOrganization($request)
    {
        //TODO: 如果跨一级部门调整部门归属，需要修改该角色归属
        try {
            $ding_department_id = Organization::where('id', $request->input('id'))->first()->ding_department_id;
            if ($parent_id = $request->input('parent_id')) {
                $ding_parent_id = Organization::where('id', $request->input('parent_id'))->first()->ding_department_id;
            }
            $result = $this->ding->department->update([
                'id' => $ding_department_id,
                'name' => $request->input('name'),
                'order' => $request->input('order'),
                'parentid' => $ding_parent_id
            ]);

           //TODO: 如果钉钉执行成功，才能修改本地

            /** @noinspection PhpInconsistentReturnPointsInspection */
            return $this->update(
                [
                    'name' => $request->input('name'),
                    'order' => $request->input('order'),
                    'parent_id' => $request->input('parent_id'),
                    'is_enable' => $request->input('is_enable')
                ], $request->input('id'));

        } catch(\Exception $exception) {
            throw new AbortException('401', $exception->getMessage());
        }
    }

    /**
     * 删除组织架构
     * @param $id
     * @return mixed
     */
    public function deleteOrganization($request)
    {
        //TODO: 判断该部门是否存在部门资源，如果不存在，执行删除
        try{
            $ding_department_id = Organization::where('id', $request->input('id'))->first()->ding_department_id;
            if ($ding_department_id) {
                $this->ding->department->delete($ding_department_id);
            }

        }catch(\Exception $exception) {

            throw new AbortException(401, $exception->getMessage());
        }

        return $this->delete($request->input('id'));
    }
}
