<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryCriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Symfony\Component\Workflow\DefinitionBuilder;
use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\Workflow;
use Symfony\Component\Workflow\MarkingStore\SingleStateMarkingStore;


/**
 * Interface ReportRepository
 * @package namespace App\Repositories;
 */
interface ReportRepository extends BaseRepositoryInterface
{

    //收房报备
    public function reportHouseTrusteeship();

    //续收报备
    public function reportKeepTrusteeship();

    //租房报备
    public function reportHouseRent();

    //续租报备
    public function reportKeepRent();

    //未收先租报备
    public function reportEmptyRent();

    //转租报备
    public function reportSubletRent();

    //调租报备
    public function reportAdjustRent();

    //炸单报备
    public function reportNullify();

    //充公报备
    public function reportConfiscate();

    //款项报备
    public function reportFund();

    //尾款报备
    public function reportRemainderFund();

    //特殊情况报备
    public function reportSpecial();


}
