<?php

namespace App\Repositories;

use App\Criteria\HasFieldCriteria;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ReportRepository;
use App\Models\Report;
use App\Validators\ReportValidator;

/**
 * Class ReportRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ReportRepositoryEloquent extends BaseRepository implements ReportRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Report::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return ReportValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    public function reportHouseTrusteeship()
    {
        // TODO: Implement reportHouseTrusteeship() method.
    }

    public function reportKeepTrusteeship()
    {
        // TODO: Implement reportKeepTrusteeship() method.
    }

    public function reportHouseRent()
    {
        // TODO: Implement reportHouseRent() method.
    }

    public function reportKeepRent()
    {
        // TODO: Implement reportKeepRent() method.
    }

    public function reportEmptyRent()
    {
        // TODO: Implement reportEmptyRent() method.
    }

    public function reportSubletRent()
    {
        // TODO: Implement reportSubletRent() method.
    }

    public function reportAdjustRent()
    {
        // TODO: Implement reportAdjustRent() method.
    }

    public function reportNullify()
    {
        // TODO: Implement reportNullify() method.
    }

    public function reportConfiscate()
    {
        // TODO: Implement reportConfiscate() method.
    }

    public function reportFund()
    {
        // TODO: Implement reportFund() method.
    }

    public function reportRemainderFund()
    {
        // TODO: Implement reportRemainderFund() method.
    }

    public function reportSpecial()
    {
        // TODO: Implement reportSpecial() method.
    }
}
