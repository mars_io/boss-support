<?php

namespace App\Repositories;

use App\Http\Requests\OrganizationAddRequest;
use App\Models\Organization;

use GuzzleHttp\Psr7\Request;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrganizationRepository
 * @package namespace App\Repositories;
 */
interface OrganizationRepository extends BaseRepositoryInterface
{


    /**
     * 获取指定组织架构id下面的子组织架构
     * @param $id
     * @return mixed
     */
    public function getOrganization($id, $is_recursion);


    /**
     * 添加组织架构信息
     * @param $request
     * @return mixed
     */
    public function addOrganization($request);

    /**
     * 修改组织架构信息（包括状态）
     * @param $request
     * @return mixed
     */
    public function setOrganization($request);

    /**
     * 删除组织架构
     * @param $id
     * @return mixed
     */
    public function deleteOrganization($request);


}
