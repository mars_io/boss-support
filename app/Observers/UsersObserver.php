<?php

namespace App\Observers;

use App\Models\Users;
use Illuminate\Support\Facades\Cache;

class UsersObserver
{
    public $userCacheKey = "Boss_User_";

    public function created(Users $user){
        Cache::forget($this->userCacheKey . $user->id, $user);
        Cache::tags(['USER_QUERY_LIST'])->flush();
    }

    public function updated(Users $user) {
        Cache::forget($this->userCacheKey . $user->id, $user);
        Cache::tags(['USER_QUERY_LIST'])->flush();
    }

    public function deleted(Users $user) {
        Cache::forget($this->userCacheKey . $user->id);
        Cache::tags(['USER_QUERY_LIST'])->flush();
    }

    public function retrieved(Users $user) {

    }

}
