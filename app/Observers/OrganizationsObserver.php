<?php

namespace App\Observers;

use App\Models\Organizations;
use Illuminate\Support\Facades\Cache;

class OrganizationsObserver
{
    public $orgCacheKey = "Boss_Org_";

    public $userOrgCacheKey = "boss_user_org_";

    public function created(Organizations $org)
    {
        Cache::forget($this->orgCacheKey . $org->id, $org);
        Cache::forget($this->userOrgCacheKey . $org->id, $org);
        Cache::tags(['ORG_QUERY_LIST'])->flush();

    }

    public function updated(Organizations $org) {

        Cache::forget($this->orgCacheKey . $org->id, $org);
        Cache::forget($this->userOrgCacheKey . $org->id, $org);
        Cache::tags(['ORG_QUERY_LIST'])->flush();
    }

    public function deleted(Organizations $org) {
        Cache::forget($this->orgCacheKey . $org->id);
        Cache::forget($this->userOrgCacheKey . $org->id, $org);
        Cache::tags(['ORG_QUERY_LIST'])->flush();
    }
}
