<?php

namespace App\Observers;

use App\Models\Role;
use Illuminate\Support\Facades\Cache;

class RolesObserver
{
    public $roleCacheKey = "Boss_Role_";

    public $userRoleCacheKey = "boss_user_role_";

    public function created(Role $role){
        Cache::forget($this->roleCacheKey . $role->id, $role);
        Cache::forget($this->userRoleCacheKey . $role->id, $role);
    }

    public function updated(Role $role) {
        Cache::forget($this->roleCacheKey . $role->id, $role);
        Cache::forget($this->userRoleCacheKey . $role->id, $role);
    }

    public function deleted(Role $role) {
        Cache::forget($this->roleCacheKey . $role->id);
        Cache::forget($this->userRoleCacheKey . $role->id, $role);
    }
}
