<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class CustomerUsefulNotification extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    public $skin;

    public $sign;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($skin, $sign, $template_var = [])
    {
        $this->template_var = explode('|', $template_var);
        $this->skin = $skin;
        $this->sign = $sign;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

    }

    public function toSms($notifiable) {
        $phone = $this->template_var[3];
        array_pop($this->template_var);
        $this->template_var = implode('|', $this->template_var);
        return (new SmsMessage())
            ->phone($phone)
            ->skin($this->skin)
            ->sign($this->sign)
            ->param($this->template_var);
    }

    public function toDing($notifiable) {

    }

    public function toDatabase($notifiable) {

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
