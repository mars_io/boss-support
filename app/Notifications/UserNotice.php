<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class UserNotice extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        //return ['mail'];
        //return [SmsChannel::class, DingChannel::class];
        return [DingChannel::class, 'database'];
        //return ['database'];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("勒勒这是个主题吧")
                    ->line($notifiable->name . ' :感谢您加入XX大家庭')
                    ->action('Notification Action', url('/'))
                    ->line('欢迎您使用Boss系统');
    }

    public function toSms($notifiable) {
        return (new SmsMessage())
            ->phone(' 1332402181')
            ->skin(1002)
            ->param("勒勒|5|12000");
            //->skin(5)
            //->code(2058);
    }

    public function toDing($notifiable) {

        $noticeUrl = urlencode(getenv("BOSS_MOBILE_HOST") . "/#/warning?id=" . $this->template_var[0]);
        $dingUrl = "dingtalk://dingtalkclient/page/link?url={$noticeUrl}&pc_slide=true";

        return (new DingMessage())
            ->msgtype('oa')
            ->msgcontent([
                'message_url' =>  $dingUrl,
                'pc_message_url' =>  $dingUrl,
                'head' => [
                    "bgcolor" => "FF5CB0FE",
                ],
                //'title' => '最新的排名已经生成',
                "body" => [
                    "title" => $this->template_var[1],
                    "content" => $this->template_var[2],
                    "file_count" => $this->template_var[3],
                    "author" => "南京House Company"
                ]
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {
        return [
            'title'    =>  $this->template_var[1],
            'content' => $this->template_var[2],
            'url'  => ""
        ];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
