<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\RobotChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\RobotMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class WelcomeToCop extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    public $robot_token;



    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * @param $user
     * 初始化token，西安发到自己的管理群中
     */
    public function _setToken() {

        $this->robot_token = getenv('DING_ROBOT_TOKEN');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [RobotChannel::class];
    }

    public function toRobot($notifiable) {

        $this->_setToken();

        $welcome = array_random([
            '心中有了方向，才不会一路跌跌撞撞',
            '胸怀有多宽广，未来就有多辽阔',
            '习惯千差万别，未来天壤之别',
            '看得远，就更懂得诚信的珍贵',
            '今天克制自己，将来才能成就自己',
            '把握内心，别让它改变了你的节奏',
            '你要去相信，没有到达不了的明天',
            '拼一把，让明天的你感谢今天的自己'
        ]);

        $markdown = "";

        $markdown = "![avatar](http://house-t.oss-cn-shanghai.aliyuncs.com/notification/ruzhi.gif) 
### {$this->template_var[0]}
 >  {$welcome}~ ";

        return (new RobotMessage())
            ->msgcontent([
                'msgtype'   =>  'actionCard',
                'actionCard'   =>  [
                    'title' =>  "恭喜新同事入职啦",
                    'text'  =>  $markdown,
                    "hideAvatar"    =>  1,
                ]

            ], $this->robot_token);
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
    }

    public function toSms($notifiable) {

    }

    public function toDing($notifiable) {

    }

    public function toDatabase($notifiable) {

    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
