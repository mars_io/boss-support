<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class UserGetCode extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return [SmsChannel::class];
        //return ['mail'];
        return [SmsChannel::class, DingChannel::class];
        //return [DingChannel::class];
        //return ['database'];
    }

    public function toSms($notifiable) {
        return (new SmsMessage())
            ->phone($notifiable->phone)
            ->skin(9010)
            ->sign(1082)
            ->code($this->template_var);
    }

    public function toDing($notifiable) {

        return (new DingMessage())
            ->msgtype('action_card')
            ->msgcontent([
                'markdown' =>  "欢迎使用Boss系统，\n \n # 验证码: {$this->template_var}\n 请在15分钟内按页面提示提交验证码，切勿将验证码泄露于他人",
                'single_title' => "我知道了",
                'title' => '您的手机验证码：' . $this->template_var,
                'single_url' => 'dingtalk://dingtalkclient/page/link?url=http://localhost:8086&pc_slide=true'
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }

    public function tags() {
        return ['UserGetCode'];
    }
}
