<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class MockGetCode extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DingChannel::class];
    }

    public function toSms($notifiable) {

    }

    public function toDing($notifiable) {

        return (new DingMessage())
            ->msgtype('action_card')
            ->msgcontent([
                'markdown' =>  "本次验证为模拟验证，\n\n # 姓名: {$this->template_var[0]}\n # 手机号: {$this->template_var[1]}\n # 验证码: {$this->template_var[2]}\n 请在15分钟内按页面提示提交验证码，切勿将验证码泄露于他人",
                'single_title' => "我知道了",
                'title' => '@模拟验证码：' . $this->template_var[2],
                'single_url' => 'dingtalk://dingtalkclient/page/link?url=http://localhost:8086&pc_slide=true'
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
