<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\RobotChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\RobotMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class ExamInvite extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DingChannel::class, SmsChannel::class];
    }

    public function toSms($notifiable) {
        return (new SmsMessage())
            ->phone($notifiable->phone)
            ->skin(33663)
            ->sign(38538)
            ->param(implode('|', [$this->template_var[1], $this->template_var[0]]));
    }

    public function toDing($notifiable) {

        return (new DingMessage())
            ->msgtype('markdown')
            ->msgcontent([
                'text' =>  "![avatar](http://house-t.oss-cn-shanghai.aliyuncs.com/notification/peixunkaoshi.png) \n #### House大学邀请您参加({$this->template_var[0]})考试 \n
场次名称：({$this->template_var[0]}) \n
开考时间：({$this->template_var[1]}) \n
试卷时长：({$this->template_var[2]}) \n
请提前做好准备！",
                'title' => "{$this->template_var[0]}考试邀请函.",
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {

    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
