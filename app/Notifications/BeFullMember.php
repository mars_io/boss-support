<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\RobotChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\RobotMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class BeFullMember extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    public $robot_token;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    public function _setToken() {

        $this->robot_token = getenv('DING_ROBOT_TOKEN');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [RobotChannel::class];
    }

    public function toRobot($notifiable) {

        $this->_setToken();

        $markdown = "";

        $markdown = "![avatar](http://house-t.oss-cn-shanghai.aliyuncs.com/notification/zhuanzheng.gif) 
### {$this->template_var[0]}
 >  我们唯一能把握的，就是变成更好的自己，将来的你，一定会感谢现在拼命的自己~ ";

        return (new RobotMessage())
            ->msgcontent([
                'msgtype'   =>  'actionCard',
                'actionCard'   =>  [
                    'title' =>  "恭喜新同事转正啦",
                    'text'  =>  $markdown,
                    "hideAvatar"    =>  1,
                ]

            ], $this->robot_token);
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
    }

    public function toSms($notifiable) {

    }

    public function toDing($notifiable) {

    }

    public function toDatabase($notifiable) {

    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
