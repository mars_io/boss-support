<?php

namespace App\Notifications;

use App\Models\Organizations;
use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\RobotChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\RobotMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class HouseGoodNews extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * @var 群机器人token，大群机器人，西安机器人
     */
    public $robot_token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * @param $user
     * 初始化token，西安发到自己的管理群中
     */
    public function _setToken($user) {

        $org = Organizations::find($user->orgs[0]->id);
        $xianOrg = Organizations::find(168);

        $chengdu = Organizations::find(169);

        $chongqing = Organizations::find(167);

        $hefei = Organizations::find(166);

        $nanjin = Organizations::find(142);

        $hangzhou = Organizations::find(93);

        $suzhou = Organizations::find(68);

        $yunwei = Organizations::find(88);

        if ($xianOrg && $org->isDescendantOf($xianOrg)) {

            $this->robot_token = getenv('DING_ROBOT_XIAN_TOKEN');
        } elseif ($chengdu && $org->isDescendantOf($chengdu)){

            $this->robot_token = getenv('DING_ROBOT_CHENGDU_TOKEN');
        }elseif ($chongqing && $org->isDescendantOf($chongqing)){

            $this->robot_token = getenv('DING_ROBOT_CHONGQING_TOKEN');
        }elseif ($hefei && $org->isDescendantOf($hefei)){

            $this->robot_token = getenv('DING_ROBOT_HEFEI_TOKEN');
        }elseif ($nanjin && $org->isDescendantOf($nanjin)){

            $this->robot_token = getenv('DING_ROBOT_NANJIN_TOKEN');
        }elseif ($hangzhou && $org->isDescendantOf($hangzhou)){

            $this->robot_token = getenv('DING_ROBOT_HANGZHOU_TOKEN');
        }elseif ($suzhou && $org->isDescendantOf($suzhou)){

            $this->robot_token = getenv('DING_ROBOT_SUZHOU_TOKEN');
        }elseif ($yunwei && $org->isDescendantOf($yunwei)){

            $this->robot_token = getenv('DING_ROBOT_TOKEN');
        }else {
            //$this->robot_token = getenv('DING_ROBOT_TOKEN');
        }

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [RobotChannel::class];
    }

    public function toRobot($notifiable) {

        $this->_setToken($notifiable);

        $markdown = "";
        //房屋地址，年限，价格，是否中介（收房）
        //开单人，部门
        //付款方式
        $comon = array_random([
            '他们试图把你埋了，你要记得你是种子。',
            '为了选择生活，而不是被生活选择。',
            '这一路走来，说不上多辛苦，庆幸自己很清楚。',
            '总是有人要赢的，那为什么不能是我呢？',
            '我和我骄傲的倔强，握紧双手绝对不放。',
            '将来的你，一定会感谢现在拼命的自己。',
            '比你漂亮比你聪明比你有钱的人都在努力，你有什么资格堕落。',
            '你走过的曲折，都会变成一条条彩虹。',
            '你只管盛开，清风自然会来。',
            '如果你觉得累，那说明你在走上坡路。',
            '为什么要努力？为的是以后夹菜时没人敢转桌子。',
            '所有辛苦的尽头，都是行云流水般的此世光阴。',
            '从哪里摔倒就从哪里站起来，这只是为了不让坚强瞧不起你!',
            '学会珍惜，才会拥有。',
            '生活就像一面镜子，你若对她笑，她就对你笑。',
            '你被拒绝的越多，你就成长得越快;你学的越多，就越能成功。',
            '大器不必晚成，趁着年轻，努力让自己的才能创造最大的价值。',
            '遇到困难时不要放弃，要记住，坚持到底就是胜利。',
            '展现自己的风采，用加倍的努力来赢得成功。',
            '给自己一片没有退路的悬崖，就是给自己一个向生命高地冲锋的机会。',
            '一份信心，一份努力，一份成功;十分信心，十分努力，十分成功。',
            '成长是一种需要学会坚持的事儿，不敢怠慢。',
            '勤奋，是步入成功之门的通行证。',
            '障碍与失败，是通往成功最稳靠的踏脚石，肯研究、利用它们，便能从失败中培养出成功。',
            '挫折其实就是迈向成功所应缴的学费。',
            '成功需要成本，时间也是一种成本，对时间的珍惜就是对成本的节约。',
            '成熟不是人的心变老，而是泪在眼眶里打转还能微笑。',
            '人生最大的光荣，不在于永不失败，而在于能屡仆屡起。',
            '莫等闲，白了少年头，空悲切。',
            '凡是自强不息者，最终都会成功。',
            '应以事业而不应以寿数来衡量人的一生。',
            '眼睛所能看到的地方,就是你会到达的地方。',
            '我们总喜欢去验证别人对我们许下的诺言，却很少去验证自己给自己许下的诺言。',
            '过去属于死神，未来属于你自己。',
            '岁月会让我们学会理解，年轮会让我们学会宽容！',
            '不要只是埋头拉车，还要抬头看好路。',
            '当你长大时，你会发现你有两只手，一只用来帮助自己，一只用来帮助别人。',
            '平凡简单，安于平凡，真不简单。',
            '千百万次的努力不如一次正确的抉择',
            '生活像一道彩虹，你不知道另一端通向哪里，但你会知道，它总是在那里。',
            '不忘初心，方得始终。',
            '只要不把自己束缚在心灵的牢笼里，谁也束缚不了你去展翅高飞。',
            '我们自己选择的路，即使跪着也要走完;因为一旦开始，便不能终止。这才叫做真正的坚持。',
            '为别人着想，为自己而活。为别人着想，才不失活得高尚;为自己而活，才不失活得洒脱。',
            '世上没有绝望的处境，只有对处境绝望的人。',
            '既然选择了远方，就只有风雨兼程。',
            '改变能改变的，接受不能接受的。',
            '做任何一件事，都要有始有终，坚持把它做完。',
            '只要我们愿意与时间为伴，时间会为我们沉淀曾经的苦与乐，四季会为我们过滤悲与喜。'
        ]);

        $markdown = "![avatar](http://house-t.oss-cn-shanghai.aliyuncs.com/notification/{$this->template_var[7]}) 
### {$this->template_var[0]}
#### 部分报备信息：
#### {$this->template_var[1]}
#### {$this->template_var[2]}
#### {$this->template_var[3]}
#### {$this->template_var[4]}
#### {$this->template_var[5]}
#### {$this->template_var[6]}  
> 「 {$comon} 」";

        return (new RobotMessage())
            ->msgcontent([
                'msgtype'   =>  'actionCard',
                'actionCard'   =>  [
                    'title' =>  "@快来点赞，最新喜报!!",
                    'text'  =>  $markdown,
                    "hideAvatar"    =>  1,
                ]

            ], $this->robot_token);
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
    }

    public function toSms($notifiable) {

    }

    public function toDing($notifiable) {

    }

    public function toDatabase($notifiable) {
        return [

            'title'    =>  $this->template_var[0],
            'content' => [
                $this->template_var[1],
                $this->template_var[2],
                $this->template_var[3],
                $this->template_var[4],
                $this->template_var[5],
                $this->template_var[6],
            ],
            'is_alert'  =>  1,
            //'raw'   =>  $this->template_var[2],
            'url'  => ""
        ];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }

    public function tags() {
        return ['HouseGoodNews'];
    }
}
