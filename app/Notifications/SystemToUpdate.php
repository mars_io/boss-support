<?php

namespace App\Notifications;

use App\Http\Resources\TaskResourceCollection;
use App\Models\Tasks;
use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\RobotChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\RobotMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class SystemToUpdate extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DingChannel::class];
    }

    public function toRobot($notifiable) {
        return (new RobotMessage())
            ->msgcontent(['msgtype'=>'text', 'text'=>['content'=>'aaaaa']]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

    }

    public function toSms($notifiable) {

    }

    public function toDing($notifiable) {

        return (new DingMessage())
            ->msgtype('action_card')
            ->msgcontent([
                'markdown' =>  "![avatar]({$this->template_var[0]}) 
{$this->template_var[1]}",
                'single_title' => "查看详情",
                'title' => "{$this->template_var[2]}",
                'single_url' => "{$this->template_var[3]}"
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {
        return [
            'title' => $notifiable->name,
            'content' => $notifiable->phone,
            'url'   => str_random(10)
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
