<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\RobotChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\RobotMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class ArticlePublish extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    public $robot_token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    public function _setToken() {

        $this->robot_token = getenv('DING_ROBOT_TOKEN');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [RobotChannel::class];
    }

    public function toRobot($notifiable) {
        $this->_setToken();
        $articleUrl = getenv("BOSS_MOBILE_HOST") . "/#/writings?id=" . $this->template_var[2];
        $markdown = "";

        if (isset($this->template_var[3]) && $this->template_var[3]){
            $markdown .= "![avatar]({$this->template_var[3]}) \n\n ";
        }
        $markdown .= "#### {$this->template_var[0]} \n\n ";
        $markdown .= $this->template_var[1];

        return (new RobotMessage())
            ->msgcontent([
                'msgtype'   =>  'actionCard',
                'actionCard'   =>  [
                    'title' =>  $this->template_var[0],
                    'text'  =>  $markdown,
                    'btns'  =>  [
                        [
                            'title' =>  "阅读全文",
                            'actionURL' =>  $articleUrl
                        ],
                        /*[
                            'title' =>  '不感兴趣',
                            'actionURL' =>  $articleUrl
                        ]*/

                    ],
                ]

            ], $this->robot_token);
    }

    public function toDatabase($notifiable) {
        return [
            'title'    =>  $this->template_var[1],
            'content' => $this->template_var[2],
            'url'  => ""
        ];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
