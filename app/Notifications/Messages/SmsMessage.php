<?php

namespace App\Notifications\Messages;

use Illuminate\Support\Facades\Log;

class SmsMessage
{

    public $phone;

    public $skin;

    public $sign;

    public $code;

    public $param;
    /**
     * Create a new message instance.
     *
     * @param  string  $content
     * @return void
     */
    public function __construct()
    {

    }

    public function phone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function skin($skin)
    {
        $this->skin = $skin;
        return $this;
    }

    /**
     * Set the message content.
     *
     * @param  string  $content
     * @return $this
     */
    public function code($code)
    {
        $this->code = $code;
        return $this;
    }

    public function sign($sign)
    {
        $this->sign = $sign;
        return $this;
    }

    public function param($param) {
        $this->param = $param;
        return $this;
    }

    /**
     * Set the message type.
     *
     * @return $this
     */
    public function unicode()
    {
        $this->type = 'unicode';

        return $this;
    }
}
