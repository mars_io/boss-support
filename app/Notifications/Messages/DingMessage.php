<?php

namespace App\Notifications\Messages;

class DingMessage
{

    public $userid_list;

    public $msgtype;

    public $msgcontent;

    /**
     * Create a new message instance.
     *
     * @param  string  $content
     * @return void
     */
    public function __construct()
    {
    }

    public function userid_list($userid_list){

        $this->userid_list = $userid_list;
        return $this;
    }

    public function msgtype($msgtype) {
        $this->msgtype = $msgtype;
        return $this;
    }

    public function msgcontent($msgcontent){
        $this->msgcontent = json_encode($msgcontent);
        return $this;
    }

}
