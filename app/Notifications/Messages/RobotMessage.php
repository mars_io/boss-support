<?php

namespace App\Notifications\Messages;

class RobotMessage
{

    public $msgcontent;

    public $robottoken;

    /**
     * Create a new message instance.
     *
     * @param  string  $content
     * @return void
     */
    public function __construct()
    {
    }

    public function msgcontent($msgcontent, $robottoken){
        $this->msgcontent = json_encode($msgcontent);
        $this->robottoken = $robottoken;
        return $this;
    }

}
