<?php

namespace App\Notifications;

use App\Http\Resources\TaskResourceCollection;
use App\Models\Tasks;
use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\RobotChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\RobotMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class QAGetAnswer extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DingChannel::class];
    }

    public function toRobot($notifiable) {

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

    }

    public function toSms($notifiable) {

    }

    public function toDing($notifiable) {

        return (new DingMessage())
            ->msgtype('action_card')
            ->msgcontent([
                'markdown' =>  "![avatar](http://house-t.oss-cn-shanghai.aliyuncs.com/notification/wendazhongxin.png) \n #### 您收到一条新的回答,请前往问答中心查看. \n > {$this->template_var[0]}",
                'single_title' => "查看详情",
                'title' => "@亲!有人回答了您的问题啦",
                'single_url' => "#"
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {
        return [
            'title' => $notifiable->name,
            'content' => $notifiable->phone,
            'url'   => str_random(10)
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
