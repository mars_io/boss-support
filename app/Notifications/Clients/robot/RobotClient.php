<?php
namespace App\Notifications\Clients\robot;

use GuzzleHttp\Client as HttpClient;

/**
* 
*/
class RobotClient
{
    public $options;

	function __construct()
	{
	    //TODO: 上线后，换成线上Token
        $this->options = ['headers' => ['Content-Type' => 'application/json']];

    }

	public function send($dingData = [], $robot_token) {
        $http = new HttpClient();
        $this->options['body'] = $dingData;

        return $http->request('POST',
            "https://oapi.dingtalk.com/robot/send?access_token=" . $robot_token,
            $this->options
        );
	}
}

?>