<?php
namespace App\Notifications\Clients\ding;

use EasyDingTalk\Application;
/**
* 
*/
class DingClient
{
    public $ding;

	function __construct()
	{
        $options = [
            'corp_id' => config('services.dingtalk.corp_id'),
            'corp_secret' => config('services.dingtalk.corp_secret'),
        ];
        $this->ding = new Application($options);

        //$res = $this->ding->user->simpleList(1);
        //dd($res);
	}

	public function send($dingData = []) {

        $dingData['agent_id'] = config("services.dingtalk.agent_id");
        $res = $this->ding->async_message->send($dingData);

	    return $res;
	}
}

?>