<?php
namespace App\Notifications\Clients\sms;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Log;


class SmsClient
{
	
	public $url;

	public $headers;

	function __construct()
	{
    	$this->url = "";
    	$this->headers = ['headers' => ['Authorization' => 'APPCODE ' . config('services.sms.code')]];
	}

	public function send($smsData = []) {

	    $url = (isset($smsData['skin']) && (int)$smsData['skin'] === 9010) ? config('services.sms.host_notice') . '?' : config('services.sms.host_large') . '?';
	    $this->url = $url . http_build_query($smsData);
	    $http = new HttpClient();
    	$res = $http->request(config('services.sms.method'), $this->url, $this->headers);
    	Log::info("SMSCLIENT:" . json_encode(['url' => urldecode($this->url), 'res' => $res->getBody()->getContents()]));

	}
}


?>