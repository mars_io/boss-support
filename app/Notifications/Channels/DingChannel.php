<?php
namespace App\Notifications\Channels;

use App\Notifications\Clients\ding\DingClient;
use Illuminate\Notifications\Notification;

class DingChannel {

	protected $dingchannel;

	public function __construct(DingClient $ding) {
        $this->dingchannel = $ding;
    }

	public function send($notifiable, Notification $notification) {
		
		$message = $notification->toDing($notifiable);

		return $this->dingchannel->send([
			'userid_list' => $message->userid_list,
			'msgtype' => $message->msgtype,
			'msgcontent' => $message->msgcontent
		]);
		
	}
}


?>