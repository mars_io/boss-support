<?php
namespace App\Notifications\Channels;

use App\Notifications\Clients\robot\RobotClient;
use Illuminate\Notifications\Notification;

class RobotChannel {

	protected $robot;

	protected $robotchannel;

	public function __construct(RobotClient $robot) {
        $this->robotchannel = $robot;
    }

	public function send($notifiable, Notification $notification) {
		$message = $notification->toRobot($notifiable);
		return $this->robotchannel->send($message->msgcontent, $message->robottoken);
	}
}


?>