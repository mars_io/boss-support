<?php
namespace App\Notifications\Channels;

use Illuminate\Notifications\Notification;
use App\Notifications\Clients\sms\SmsClient;
use Illuminate\Support\Facades\Log;

class SmsChannel {

	protected $smschannel;

	public function __construct(SmsClient $sms) {
        $this->smschannel = $sms;
    }

	public function send($notifiable, Notification $notification) {
		
		$message = $notification->toSms($notifiable);
		$smsData = [];
        $smsData['phone'] = $message->phone;
        $smsData['skin'] = $message->skin;
        $smsData['sign'] = $message->sign;
        if ($message->code) {
            $smsData['param'] = $message->code;
        }
        //param = "张三|18|2018-3-21
        if ($message->param) {
            $smsData['param'] = $message->param;
        }

		return $this->smschannel->send($smsData);
	}
}
?>