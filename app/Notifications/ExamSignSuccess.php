<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\RobotChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\RobotMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class ExamSignSuccess extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DingChannel::class, SmsChannel::class];
    }

    public function toSms($notifiable) {
        return (new SmsMessage())
            ->phone($notifiable->phone)
            ->skin(33663)
            ->sign(38538)
            ->param(implode('|', [$this->template_var[1], $this->template_var[0]]));
    }

    public function toDing($notifiable) {

        return (new DingMessage())
            ->msgtype('markdown')
            ->msgcontent([
                'text' =>  "![avatar](http://house-t.oss-cn-shanghai.aliyuncs.com/notification/peixunkaoshi.png) \n #### 您提交的 ({$this->template_var[0]}) 考试报名申请，主考官已同意，考试时间为({$this->template_var[1]})，请提前做好准备。",
                'title' => "您的考试申请已通过.",
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {

    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
