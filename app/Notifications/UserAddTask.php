<?php

namespace App\Notifications;

use App\Http\Resources\TaskResourceCollection;
use App\Models\Tasks;
use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\RobotChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\RobotMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class UserAddTask extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return [SmsChannel::class];
        //return ['mail'];
        return [DingChannel::class];
        //return [RobotChannel::class];
        //return ['database'];
    }

    public function toRobot($notifiable) {
        return (new RobotMessage())
            ->msgcontent(['msgtype'=>'text', 'text'=>['content'=>'aaaaa']]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $count = Tasks::where('user_id', $notifiable->id)
            ->where('is_cc', 0)
            ->whereNull('finish_at')
            ->count();

        $taskList = new TaskResourceCollection(Tasks::where('user_id', $notifiable->id)
            ->where('is_cc', 0)
            ->whereNull('finish_at')
            ->with('user')
            ->with('user.orgs')
            ->orderBy('id', 'desc')->get());

        return (new MailMessage)
            ->subject("您有{$count}条待办需要处理")
            ->markdown('mail.task', ['count' => $count, 'tasks' => $taskList]);
    }

    public function toSms($notifiable) {
        /*return (new SmsMessage())
            ->phone($notifiable->phone)
            ->skin(9010)
            ->sign(1082)
            ->code($this->template_var);*/
    }

    public function toDing($notifiable) {

        return (new DingMessage())
            ->msgtype('action_card')
            ->msgcontent([
                'markdown' =>  "![avatar](http://house-t.oss-cn-shanghai.aliyuncs.com/notification/{$this->template_var['avatar']}) 
{$this->template_var['markdown']}",
                'single_title' => "查看详情",
                'title' => "{$this->template_var['title']}",
                'single_url' => "{$this->template_var['url']}"
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {
        return [
            'title' => $notifiable->name,
            'content' => $notifiable->phone,
            'url'   => str_random(10)
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }

    public function tags() {
        return ['UserAddTask'];
    }
}
