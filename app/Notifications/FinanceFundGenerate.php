<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;


class FinanceFundGenerate extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DingChannel::class];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

    }

    public function toSms($notifiable) {
        return (new SmsMessage())
            ->phone($notifiable->phone)
            ->skin(9010)
            ->sign(1082)
            ->code($this->template_var);
    }

    public function toDing($notifiable) {

        return (new DingMessage())
            ->msgtype('markdown')
            ->msgcontent([
                'text' =>  "![avatar](http://house-t.oss-cn-shanghai.aliyuncs.com/notification/weishenhekuanxiang2.png)\n #### {$this->template_var[1]} \n#{$this->template_var[0]}#关于#{$this->template_var[1]}#的#{$this->template_var[2]}#已通过审核并生成款项草稿, 请即时进入Boss2查看并处理.",
                'title' => "一条最新的未审核款项已生成，请即时查看处理",
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
