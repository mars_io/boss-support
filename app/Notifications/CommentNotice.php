<?php

namespace App\Notifications;

use App\Notifications\Channels\DingChannel;
use App\Notifications\Messages\DingMessage;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;


class CommentNotice extends Notification implements ShouldQueue {

    use Queueable;

    public $template_var;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template_var = [])
    {
        $this->template_var = $template_var;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        //return ['mail'];
        //return [SmsChannel::class, DingChannel::class];
        return [DingChannel::class, 'database'];
        //return ['database'];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("勒勒这是个主题吧")
                    ->line($notifiable->name . ' :感谢您加入XX大家庭')
                    ->action('Notification Action', url('/'))
                    ->line('欢迎您使用Boss系统');
    }

    public function toSms($notifiable) {

    }

    public function toDing($notifiable) {
        return (new DingMessage())
            ->msgtype('action_card')
            ->msgcontent([
                'markdown' =>  "{$this->template_var['markdown']}",
                'single_title' => "查看评论",
                'title' => "@有人在报备中评论了你",
                'single_url' => "{$this->template_var['url']}"
            ])
            ->userid_list($notifiable->ding_user_id);
    }

    public function toDatabase($notifiable) {
        return [
            'id'    =>  $notifiable->id,
            'name' => $notifiable->name,
            'ding_user_id'  => $notifiable->ding_user_id
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }

    public function tags() {
        return ['CommentNotice'];
    }
}
