<?php

namespace App\Listeners;

use App\Events\CompatibleEvent;
use App\Events\HouseStatusEvent;
use App\Exceptions\AbortException;
use App\Models\ContractLord;
use Illuminate\Http\Request;

class ContractLordEventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function onStoreContractLord($event) {

        $process = $event->process;
        $house = $event->house[0] ? $event->house[0] : $event->house;
        $customer = $event->customer[0] ? $event->customer[0] : $event->customer;

        $lord = new ContractLord();

        $lord->house_id = $house->id;

        if (array_get($process, 'contract_number')) {
            $existContract = ContractLord::where('contract_number', array_get($process, 'contract_number'))->first();
            if ($existContract) {
                throw new AbortException("该收房合同编号已经存在: " . array_get($process, 'contract_number'));
            }
        }

        $lord->contract_number = array_get($process, 'contract_number');

        //是否合租
        $lord->is_joint = array_get($process, 'share.id');

        //是否中介
        $lord->is_agency = array_get($process, 'is_agency.id');

        $lord->agency_info = array_get($process, 'agency_info');

        //是否以公司名义签单
        $lord->is_corp = array_get($process, 'is_corp.id');

        //1为新收，2为续收
        $lord->type = array_get($process, 'type.id');

        $lord->first_pay_at = array_get($process, 'pay_first_date');
        $lord->second_pay_at = array_get($process, 'pay_second_date');

        $lord->sign_month = array_get($process, 'month');
        $lord->sign_remainder_day = array_get($process, 'day');

        $lord->sign_at = array_get($process, 'sign_date');
        $lord->start_at = array_get($process, 'begin_date');
        $lord->end_at = array_get($process, 'end_date');
        $lord->mortgage_price = array_get($process, 'deposit');
        $lord->penalty_price = array_get($process, 'penalty');

        $lord->month_price = array_get($process, 'price');
        $lord->pay_way = array_get($process, 'pay_way');
        $lord->purchase_way = array_get($process, 'purchase_way');
        $lord->pay_account_info = [
            'name' => array_get($process, 'name'),
            'phone' => array_get($process, 'phone'),
            'bank' => array_get($process, 'bank'),
            'subbranch' => array_get($process, 'subbranch'),
            'account_name' => array_get($process, 'account_name'),
            'account' => array_get($process, 'account'),
            'relationship' => array_get($process, 'relationship')
        ];

        $lord->album = array_get($process, 'album');

        //空置期天数
        $lord->ready_days = array_get($process, 'vacancy');

        //收房总天数
        $lord->duration_days = array_get($process, 'duration_days');

        $lord->property_payer = array_get($process, 'property_payer.id');

        $lord->property_price = array_get($process, 'property');

        $lord->guarantee_days = array_get($process, 'guarantee_days');

        $lord->vacancy_way = array_get($process, 'vacancy_way.id');
        $lord->vacancy_other = array_get($process, 'vacancy_other');
        $lord->vacancy_end_date = array_get($process, 'vacancy_end_date');

        $lord->remark = array_get($process, 'remark');

        $lord->city_name = array_get($process, 'city_name');

        //报备人id
        $lord->user_id = array_get($process, 'bulletin_staff_id');;

        //开单人id
        $lord->sign_user_id = array_get($process, 'staff_id');

        //开单部门id
        $lord->sign_org_id = array_get($process, 'department_id');

        $lord->org_id = array_get($process, 'department_id');


        if ($lord->save()) {

            $lord->customers()->sync($customer->id);
            $report = $lord->type === 1 ? 1 : 2;
            event(new HouseStatusEvent($report, $lord->house_id, $lord->id));
            event(new CompatibleEvent(false, $lord->id, $process, array_get($process, 'bulletin_type')));
            return $lord;
        }
        return false;
    }

    public function onUpdateContractLord($event) {

    }

    public function subscribe($event) {

        $event->listen(
            "App\Events\ContractLordStoreEvent",
            "App\Listeners\ContractLordEventSubscriber@onStoreContractLord"
        );

        $event->listen(
            "App\Events\ContractLordUpdateEvent",
            "App\Listeners\ContractLordEventSubscriber@onUpdateContractLord"
        );


    }
}
