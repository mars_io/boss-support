<?php

namespace App\Listeners;

use App\Events\CompatibleEvent;
use App\Events\Event;
use App\Exceptions\AbortException;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class CompatibleListener implements ShouldQueue
{
    use Queueable;
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(CompatibleEvent $event)
    {
        $option['form_params'] = [
            'content' => json_encode($event->process),
            'processable_type' => $event->process_type,
            'contract_id' => $event->contract_id
        ];
        $option['headers'] = [
            'token' => 'f_a_v_2',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $http = new HttpClient();

        $rent_url = "/financial/customer_rent";
        $lord_url = "/financial/customer_collect";
        $agency_url = "/financial/agency";
        if ($event->isRent === true) {
            $url = $rent_url;
        }

        if ($event->isRent === false) {
            $url = $lord_url;
        }

        if ($event->isRent === null) {
            $url = $agency_url;
        }

        //$url = $event->isRent ? $rent_url : $lord_url;

        $api_host = getenv("BOSS_CLIENT_API");
        $url = $api_host . $url;

        $res = $http->request("POST", $url, $option);
        Log::info("SyncFinance" . $res->getBody()->getContents());
    }

    public function tags() {
        return ["CompatibleListener"];
    }
}
