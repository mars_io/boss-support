<?php

namespace App\Listeners;

use App\Events\CommentEvent;
use App\Events\Event;
use App\Notifications\CommentNotice;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentListener implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(CommentEvent $event)
    {
        $display_name = $event->commentUser->name ."(" . $event->commentUser->orgs[0]->name . "-" . $event->commentUser->roles[0]->display_name . ")";
        $message = [];
        array_set($message, 'url', getenv('BOSS_MOBILE_HOST') . "/#/publishDetail?ids={$event->processId}");
        array_set($message, 'markdown', "#### {$display_name} 在报备中参与了评论 \n > {$event->comment->body}\n\n#### 需要您进行及时回复!");

        $event->processUser->notify(new CommentNotice($message));
        if ($event->processUser->orgs->isNotEmpty()) {
            $event->processUser->orgs[0]->leader->notify(new CommentNotice($message));
        }
    }
}
