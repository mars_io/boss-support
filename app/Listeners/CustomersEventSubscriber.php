<?php

namespace App\Listeners;

use App\Exceptions\AbortException;
use App\Models\ContractRenter;
use App\Models\Customers;
use Illuminate\Http\Request;

class CustomersEventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {


    }

    public function onStoreCustomer($event) {
        $process = $event->process;
        $customer = new Customers();
        $customer->name = array_get($process, 'name');
        $customer->phone = array_get($process, 'phone');
        $customer->is_corp = array_get($process, 'from.id', 0) ? 0 : 1;

        if (!$customer->name) {
            throw new AbortException("客户姓名不能为空");
        }

        if (!$customer->phone) {
            throw new AbortException("客户手机号不能为空");
        }

        /*if (Customers::where('phone', $customer->phone)->first()) {
            throw new AbortException("该客户手机号已经存在");
        }*/

        if (($cust = Customers::where([
            ['name', '=', array_get($process, 'name')],
            ['phone', '=', array_get($process, 'phone')]])->first())) {

            return $cust;
        }

        if (array_get($process, 'name')) {
            $py = pinyin_abbr(array_get($process, 'name'));
            $pinyin = str_replace(' ','', pinyin_sentence(array_get($process, 'name')));
            $customer->py = $py;
            $customer->pinyin = $pinyin;
        }
        //$customer->user_id = $process->user_id;

        return $customer->save() ? $customer : false;
    }


    public function onUpdateCustomer($event) {

    }



    public function subscribe($event) {

        $event->listen(
            "App\Events\CustomerUpdateEvent",
            "App\Listeners\CustomersEventSubscriber@onUpdateCustomer"
        );


        $event->listen(
            "App\Events\CustomerStoreEvent",
            "App\Listeners\CustomersEventSubscriber@onStoreCustomer"
        );


    }
}
