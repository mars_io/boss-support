<?php

namespace App\Listeners;

use App\Events\Boss2Event;
use App\Events\CompatibleEvent;
use App\Events\Event;
use App\Exceptions\AbortException;
use App\Http\Resources\ContractLordResource;
use App\Http\Resources\ContractRenterResource;
use App\Http\Resources\CustomersResource;
use App\Http\Resources\HousesResource;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class Boss2Listener implements ShouldQueue
{
    use Queueable;
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Boss2Event $event)
    {

        $content = NULL;
        switch ($event->ableType) {
            case 'contract_lord':
                $content = new ContractLordResource($event->model);
                break;

            case 'contract_renter':
                $content = new ContractRenterResource($event->model);
                break;

            case 'customer':
                $content = new CustomersResource($event->model);
                break;

            case 'house':
                $content = new HousesResource($event->model);
                break;
        }

        $option['form_params'] = [
            'content' => json_encode($content),
            'able_type' => $event->ableType,
        ];

        $option['headers'] = [
            'token' => 'f_a_v_2',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $http = new HttpClient();

        $boss2 = "/financial/change_sync";

        $api_host = getenv("BOSS_CLIENT_API");
        $url = $api_host . $boss2;

        $res = $http->request("POST", $url, $option);
        Log::info("SyncBoss2" . $res->getBody()->getContents());
    }

    public function tags() {
        return ["Boss2Listener"];
    }
}
