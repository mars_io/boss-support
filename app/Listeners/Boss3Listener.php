<?php

namespace App\Listeners;

use App\Events\Boss3Event;
use App\Events\Event;
use App\Exceptions\AbortException;
use App\Models\Process;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

//class Boss3Listener implements ShouldQueue
class Boss3Listener
{
    //use Queueable;
    //use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Boss3Event $event)
    {
        $report_create_time = Process::find($event->process_id)->created_at;

        $option['form_params'] = [
            'content' => json_encode($event->process),
            'processable_id' => $event->process_id,
            'processable_type' => $event->process_type,
            'created_at'    =>  $report_create_time
        ];
        $option['headers'] = [
            'token' => 'f_a_v_2',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $http = new HttpClient();
        //$url = '/bulletin/helper/notice';
        $url    = '/api/uploaddata';

        $api_host = getenv("BOSS_CLIENT_API");
        $url = $api_host . $url;

        $res = $http->request("POST", $url, $option);
        $data = $res->getBody()->getContents();
        Log::info("SyncBoss3" . $data);
        $d = json_decode($data, true);
        if ($d['code'] == 40099) {
            throw new AbortException($d['msg']);
        }
        return $data;
    }

    public function tags() {
        return ["Boss3Listener"];
    }
}
