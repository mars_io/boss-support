<?php

namespace App\Listeners;


use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\UserAddTask;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class Work
{

    public $place;

    public $nodeFrom;

    public $defaultLeader = 62;

    public $reporter;

    public $cc;

    public $process;

    public $processModel;

    public $title = "您有一条最新报备!";

    public $ending = "请您尽快处理!";

    public function nodeInit($event) {

        $this->process = $event->originEvent->getSubject()->content;
        $this->processModel = $event->originEvent->getSubject();
        $this->place = array_keys($event->originEvent->getMarking()->getPlaces())[0];
        $this->nodeFrom = $event->originEvent->getTransition()->getFroms()[0];
        $this->reporter = Users::find($this->processModel->user_id);
        $this->cc = $this->getWorkflowNode($event->originEvent, 'node.cc');

        $this->taskInit($event->taskRole);
    }

    public function taskInit($task_role) {

        if ($this->nodeFrom == "draft") {

            $org = Organizations::find($this->process['department_id']);

            if ($org) {
                if ($org->leader) {
                    $auditor = $org->leader->id;
                } else {
                    $parentOrg = $org->parent()->first();
                    $parentLeader = $parentOrg->leader;
                    if ($parentLeader) {
                        $auditor = $parentLeader->id;
                    }else {
                        $ppLeader = $parentOrg->parent()->first()->leader;
                        if ($ppLeader) {
                            $auditor = $ppLeader->id;
                        }
                    }
                }
            } else {
                $auditor = (array)$this->defaultLeader;
            }

            return $this->task(null, $auditor);

        }elseif ($this->place == 'published' || $this->place == 'cancelled' || str_contains($this->place, '_rejected')){
            return false;
        } else {

            foreach ($task_role as $role) {
                if (str_contains($role, 'market-')) {
                    if ($role == 'market-marketing-officer') {
                        return $this->task(null, $this->reporter->id);
                    }

                    if ($role == 'market-marketing-manager') {
                        $org = Organizations::find($this->process['department_id']);
                        return $this->task(null, $org->leader->id);
                    }
                } else {
                    return $this->task($task_role);
                }
            }
        }
    }


    public function message($user) {

        $i = 0;
        array_set($message, 'avatar', $this->avatar);
        array_set($message, 'title', $this->title);
        array_set($message, 'url', getenv('BOSS_MOBILE_HOST') . "/#/publishDetail?ids={$this->processModel->id}");

        $markdown = "";
        $markdown .= "### {$this->reporter->orgs[0]->name} - {$this->reporter->name} {$this->name} \n ";

        foreach ($this->process['show_content'] as $k => $v) {
            if (is_array($v)) continue;
            $markdown .= "#### {$k}: {$v} \n ";
            $i++;
            if ($i > 5) break;
        }
        $markdown .= "#### " . $this->ending;
        $markdown .= " \n #### [ " . date("m-d H:i", time()) . " ]";

        array_set($message, 'markdown', $markdown);
        $user->notify(new UserAddTask($message));
    }

    //TODO: task中同步house_id
    public function task($task_role, $options = null) {

        $name = Users::find($this->processModel->user_id)->name;
        if ($task_role === null) {
            $user_ids = (array)$options;
        }else {

            $user_ids = Users::whereHas('roles', function ($roleQuery) use ($task_role) {
                $roleQuery->whereIn('name', $task_role);
            })->pluck('id')->toArray();
            $user_ids = array_unique(array_flatten($user_ids));
        }

        if (!$user_ids) {
            Log::info("Work-lose:" . $this->processModel->id);
            return false;
        }

        foreach ($user_ids as $user_id) {
            $user = Users::find($user_id);
            $res = $this->processModel->tasks()->create([
                'flow_type' => $this->processModel->processable_type,
                'user_id'   =>  $user_id,
                'house_id'  => $this->processModel->house_id,
                'title'   =>  "{$name}的{$this->name}",
                'begin_at'   =>  Carbon::now(),
                'important_level'   =>  1,
                'is_cc' =>  $this->cc ? true : false
            ]);
            if ($res) {
                $this->message($user);
            }
        }
    }

    public function taskCC() {

        $this->title = "{$this->reporter->name}的{$this->name}已通过.";
        $this->ending = "抄送给了您，请您知晓!";

        //抄送组长区长
        $reporter = Users::find($this->reporter->id);
        if ($reporter->hasRole('market-marketing-officer')) {
            $marketManager = $reporter->orgs[0]->leader;
            if ($marketManager) {
                $this->task(null, $marketManager->id);
            }

            $marketRegion = $reporter->orgs[0]->parent()->first()->leader;
            if ($marketRegion) {
                $this->task(null, $marketRegion->id);
            }
        }

        $this->task($this->cc);
    }

    public function taskPublished() {

        $this->title = "您的{$this->name}已通过.";
        $this->ending = "您的{$this->name}已通过，感谢您的努力，再接再励.";
        $this->task(null, $this->reporter->id);
        $this->taskCC();
    }

    public function taskRejected() {
        $this->title = "您的{$this->name}已被拒绝.";
        $this->ending = "您的{$this->name}已被拒绝，请继续加油.";
        $this->task(null, $this->reporter->id);
    }

    public function getWorkflowNode($originEvent, $node) {
        return config("workflow." . $originEvent->getWorkflowName() . ".transitions." . $originEvent->getTransition()->getName() . "." . $node);
    }

}