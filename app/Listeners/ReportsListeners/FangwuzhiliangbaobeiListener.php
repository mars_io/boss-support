<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\FangwuzhiliangbaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\ZhuanzubaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Exceptions\AbortException;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\Houses;
use App\Models\Organizations;
use App\Models\Tasks;
use App\Models\Users;
use App\Notifications\HouseGoodNews;
use App\Notifications\UserAddTask;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;


class FangwuzhiliangbaobeiListener extends Work
{


    public $name = "房屋质量报备";

    public $avatar = "fangwuzhiliang.png";

    public $title = "您有一条最新的房屋质量报备!";

    public $ending = "为了更好的维护房屋，请您尽快处理！";

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(FangwuzhiliangbaobeiEvent $event)
    {
        $this->nodeInit($event);

        if ($this->place == 'published') {

            /*
            if (isset($this->process['quality_up']) && (int)$this->process['quality_up'] === 1) {
                //房屋质量报备后续
                $house = Houses::find($this->process['house_id']);
                $house->house_res = $this->process;
                $house->save();
            }else {
                //存储房屋
                $house = event(new HouseStoreEvent($this->process));
                if (isset($house[0])) {
                    $house = $house[0];
                }
            }

            if ($house) {

                //更新房屋id
                $this->processModel->house_id = $house->id;
                $this->processModel->save();
                Tasks::where('flow_id', $this->processModel->id)->update([
                    'house_id'  =>  $house->id
                ]);

                $this->process['house_id'] = $house->id;
                event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
                $this->taskPublished();
            }*/
            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();
        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }

    }
}
