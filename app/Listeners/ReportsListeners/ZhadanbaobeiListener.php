<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStatusEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\ZhadanbaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\UserAddTask;
use Carbon\Carbon;


class ZhadanbaobeiListener extends Work
{

    public $name = "炸单报备";

    public $avatar = "zhadan.png";

    public $title = "您有一条最新的炸单报备!";

    public $ending = "请您尽快处理";


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(ZhadanbaobeiEvent $event)
    {
        $this->nodeInit($event);

        if ($this->place == 'published') {
            //炸单
            /*if ((int)array_get($this->process, 'type.id') === 0) {
                //checkout_date
                //contract_id
                //收房
                if ((int)array_get($this->process, 'collect_or_rent.id') === 0) {
                    $lord = ContractLord::find($this->process['contract_id']);
                    $lord->end_real_at  =   Carbon::now();
                    $lord->end_type  =   1;
                    $lord->save();
                    if ($lord) {
                        event(new HouseStatusEvent(9, $lord->house_id, $lord->id));
                    }
                } else {
                    //租房
                    $renter = ContractRenter::find($this->process['contract_id']);
                    $renter->end_real_at  =   Carbon::now();
                    $renter->end_type  =   1;
                    $renter->save();
                    if ($renter) {
                        event(new HouseStatusEvent(11, $renter->rentable_id, $renter->id));
                    }
                }
            }

            //取消炸单
            if ((int)array_get($this->process, 'type.id') === 1) {

                //收房
                if ((int)array_get($this->process, 'collect_or_rent.id') === 0) {
                    $lord = ContractLord::find($this->process['contract_id']);
                    $lord->end_real_at  =   NULL;
                    $lord->end_type  =   NULL;
                    $lord->save();
                    if ($lord) {
                        event(new HouseStatusEvent(10, $lord->house_id, $lord->id));
                    }
                } else {
                    //租房
                    $renter = ContractRenter::find($this->process['contract_id']);
                    $renter->end_real_at  =   NULL;
                    $renter->end_type  =   NULL;
                    $renter->save();
                    if ($renter) {
                        event(new HouseStatusEvent(12, $renter->rentable_id, $renter->id));
                    }
                }
            }

            $this->taskPublished();*/

            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();
        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }

    }
}
