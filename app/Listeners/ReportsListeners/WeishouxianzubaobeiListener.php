<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\WeishouxianzubaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\HouseGoodNews;
use App\Notifications\UserAddTask;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;


class WeishouxianzubaobeiListener extends Work
{

    public $name = "未收先租报备";

    public $avatar = "weishouxianzu.png";

    public $title = "您有一条最新的未收先租报备!";

    public $ending = "请您尽快处理";
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(WeishouxianzubaobeiEvent $event)
    {
        $this->nodeInit($event);

        if ($this->place == 'published') {

            //存储客户
            /*$customer = event(new CustomerStoreEvent($this->process));

            //存储租房合同
            event(new ContractRenterStoreEvent($customer, $this->process));

            $this->taskPublished();

            if (array_get($this->process, 'show_robot')) {
                array_set($this->process, "show_robot.7", $this->avatar);
                Notification::send(Users::find($this->process['bulletin_staff_id']), new HouseGoodNews(array_get($this->process, 'show_robot')));
            }*/

            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();
            if (array_get($this->process, 'show_robot')) {
                array_set($this->process, "show_robot.7", $this->avatar);
                Notification::send(Users::find($this->process['bulletin_staff_id']), new HouseGoodNews(array_get($this->process, 'show_robot')));
            }
        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }

    }
}
