<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\CompatibleEvent;
use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\ContractRenterUpdateEvent;
use App\Events\CustomerStoreEvent;
use App\Events\CustomerUpdateEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\WeishouxianzubaobeiEvent;
use App\Events\ReportsEvents\WeishouxianzuquerenbaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\UserAddTask;
use Carbon\Carbon;


class WeishouxianzuquerenbaobeiListener extends Work
{
    public $name = "未收先租确认报备";

    public $avatar = "weishouxianzu.png";

    public $title = "您有一条最新的未收先租确认报备!";

    public $ending = "请您尽快处理";

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(WeishouxianzuquerenbaobeiEvent $event)
    {
        $this->nodeInit($event);
        if ($this->place == 'published') {
            //修改客户
            //$customer = event(new CustomerUpdateEvent($process_content));
            /*array_set($this->process, 'type.id', 4);

            //修改租房合同
            $res = event(new ContractRenterUpdateEvent($this->process));

            event(new CompatibleEvent(true, $res[0]->id, $this->process, array_get($this->process, 'bulletin_type')));

            $this->taskPublished();*/

            //TODO: 修改信息时，必须同步兼容Boss2
            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();
        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }

    }
}
