<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\QingtuibaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\ContractRenter;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\UserAddTask;
use Carbon\Carbon;


class QingtuibaobeiListener extends Work
{

    public $name = "清退报备";

    public $avatar = "qingtui.png";

    public $title = "您有一条最新的清退报备!";

    public $ending = "请您尽快处理！";

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(QingtuibaobeiEvent $event)
    {

        $this->nodeInit($event);

        if ($this->place == 'published') {
            //清退
            /*if ((int)array_get($this->process, 'type.id') === 0) {

                //checkout_date
                //contract_id
                $lord = ContractRenter::find($this->process['contract_id']);
                $lord->end_real_at  =   Carbon::now();
                $lord->end_type  =   4;
                $lord->save();
            }

            //取消清退
            if ((int)array_get($this->process, 'type.id') === 1) {

                $renter = ContractRenter::find($this->process['contract_id']);
                $renter->end_real_at    =   NULL;
                $renter->end_type       =   NULL;
                $renter->save();
            }*/

            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();
        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }
    }
}
