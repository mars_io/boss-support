<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\CompatibleEvent;
use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\WeikuanfangzubaobeiEvent;
use App\Events\ReportsEvents\ZhongjiefeibaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Exceptions\AbortException;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\Agency;
use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\UserAddTask;
use Carbon\Carbon;


class ZhongjiefeibaobeiListener extends Work
{

    public $name = "中介费报备";

    public $avatar = "zhongjiefei.png";

    public $title = "您有一条最新的中介费报备!";

    public $ending = "请您尽快处理";


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(ZhongjiefeibaobeiEvent $event)
    {
        $this->nodeInit($event);

        if ($this->place == 'published') {

            /*$collect_or_rent = (int)array_get($this->process, 'collect_or_rent.id');
            $contract_id = array_get($this->process, 'contract_id');

            $agency = new Agency();
            //房屋id
            $agency->house_id = array_get($this->process, 'house_id');

            //中介费
            $agency->agency_price = array_get($this->process, 'agency_price');

            //之前报备的中介费
            $agency->agency_before_price = array_get($this->process, 'agency_before_price');

            //中介名称
            $agency->agency_name = array_get($this->process, 'agency_name');

            //中介联系人
            $agency->agency_username = array_get($this->process, 'agency_username');

            //中介联系电话
            $agency->agency_phone = array_get($this->process, 'agency_phone');

            //中介付款账户信息
            $agency->agency_account_info = array_get($this->process, 'agency_account_info');

            //中介报备图册
            $agency->album = array_get($this->process, 'album');

            //报备备注
            $agency->remark = array_get($this->process, 'remark');

            //报备人
            $agency->user_id = array_get($this->process, 'user_id');

            //开单人
            $agency->sign_user_id = array_get($this->process, 'sign_user_id');


            //TODO: 客户端为获取到部门id，暂时改为获取开单人的当前部门
            $sign_user = Users::find($agency->sign_user_id);
            if ($sign_user->orgs->isNotEmpty()) {
                $agency->sign_org_id = $sign_user->orgs[0]->id;
            }

            //开单部门
            //$agency->sign_org_id = array_get($this->process, 'sign_org_id');


            if ($collect_or_rent === 1) {
                //租房
                $contract = ContractRenter::find($contract_id);
            }else {
                //收房
                $contract = ContractLord::find($contract_id);
            }

            if ($contract) {

                if ($contract->agency_contract) {
                    throw new AbortException("该房屋已经报备过中介费");
                }
                $contract->agency_contract()->create($agency->toArray());

                event(new CompatibleEvent(null, $contract->id, $this->process, array_get($this->process, 'bulletin_type')));

            } else {
                throw new AbortException("未查找到有效的合同");
            }

            $this->taskPublished();*/
            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();

        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }
    }
}
