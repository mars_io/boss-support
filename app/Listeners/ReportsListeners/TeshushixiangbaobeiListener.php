<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\TeshushixiangbaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\UserAddTask;
use Carbon\Carbon;


class TeshushixiangbaobeiListener extends Work
{

    public $name = "特殊事项报备";

    public $avatar = "teshuqingkuang.png";

    public $title = "您有一条最新的特殊事项报备!";

    public $ending = "为了更好的维护房屋，请您尽快处理！";

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(TeshushixiangbaobeiEvent $event)
    {
        $this->nodeInit($event);

        if ($this->place == 'published') {

            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();
        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }

    }

}
