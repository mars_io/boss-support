<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStatusEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\QingtuibaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\TuikuanbaobeiEvent;
use App\Events\ReportsEvents\TuizubaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\UserAddTask;
use Carbon\Carbon;


class TuizubaobeiListener extends Work
{

    public $name = "退租报备";

    public $avatar = "tuizu.png";

    public $title = "您有一条最新的退租报备!";

    public $ending = "请您尽快处理";

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(TuizubaobeiEvent $event)
    {

        $this->nodeInit($event);

        ////1炸单， 2正常退租，3违约退租  4协商退租， 5转租， 6调租, 7boss2中转为待处理项，但是未结算的状态
        if ($this->place == 'published') {

            //收房
            /*if ((int)array_get($this->process, 'collect_or_rent.id') === 0) {
                //checkout_date
                //contract_id
                $lord = ContractLord::find($this->process['contract_id']);
                $lord->end_real_at  =   $this->process['checkout_date'];
                $lord->end_type  =   $this->process['contract_checkout_type'];
                $lord->end_handover_id = $this->process['handover_staff'];

                if (array_get($this->process, 'handover_department', null)) {
                    $lord->org_id = $this->process['handover_department'];
                }
                $res = $lord->save();
                //TODO: 1、所属部门改成交接部门 2、通知Boss3系统

                event(new HouseStatusEvent(3, $lord->house_id, $lord->id));
            }

            //租房
            if ((int)array_get($this->process, 'collect_or_rent.id') === 1) {

                $renter = ContractRenter::find($this->process['contract_id']);
                $renter->end_real_at    =   $this->process['checkout_date'];
                $renter->end_type       =   $this->process['contract_checkout_type'];
                $renter->end_handover_id = $this->process['handover_staff'];

                if (array_get($this->process, 'handover_department', null)) {
                    $renter->org_id = $this->process['handover_department'];
                }

                $res = $renter->save();
                event(new HouseStatusEvent(8, $renter->rentable_id, $renter->id));
            }

            if ($res) {
                event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            }*/

            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();
        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }

    }

    public function tags() {
        return ["TuizubaobeiListener"];
    }
}
