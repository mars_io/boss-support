<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\CompatibleEvent;
use App\Events\ContractLordStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\RoomStoreEvent;
use App\Exceptions\AbortException;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\ContractLord;
use App\Models\Houses;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\HouseGoodNews;
use App\Notifications\UserAddTask;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;


class ShoufangbaobeiListener extends Work
{

    public $name = "收房报备";

    public $avatar = "shoufangxibao.png";

    public $happyAvatar = "shoufang-gif.gif";

    public $title = "您有一条最新的收房报备!";

    public $ending = "为了不影响公司的业绩，请您尽快处理！！";
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(ShoufangbaobeiEvent $event)
    {
        $this->nodeInit($event);

        //大群喜报

        if ($this->place == 'published') {

            //如果存在有效期的合同，不能重复收房
            /*$house_id = array_get($this->process, 'house.id');
            $valid_contract =   ContractLord::where('house_id', $house_id)->whereNull('end_real_at')->get();
            if ($valid_contract->isNotEmpty()) {
                throw new AbortException("该房屋已经报备过收房，如为续收，请通知业务员更正");
            }

            //存储房屋
            $house = Houses::find($house_id);

            //如果是合租，存储房间信息
            $room = event(new RoomStoreEvent($this->process, $house));

            //存储客户
            $customer = event(new CustomerStoreEvent($this->process));

            //存储收房合同
            event(new ContractLordStoreEvent($house, $customer, $this->process));

            $this->taskPublished();
            if (array_get($this->process, 'show_robot')){
                array_set($this->process, "show_robot.7", $this->happyAvatar);
                Notification::send(Users::find($this->process['bulletin_staff_id']), new HouseGoodNews(array_get($this->process, 'show_robot')));
            }*/

            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();
            if (array_get($this->process, 'show_robot')){
                array_set($this->process, "show_robot.7", $this->happyAvatar);
                Notification::send(Users::find($this->process['bulletin_staff_id']), new HouseGoodNews(array_get($this->process, 'show_robot')));
            }
        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }

    }
}
