<?php

namespace App\Listeners\ReportsListeners;

use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Http\Resources\UsersResource;
use App\Models\Users;
use App\Notifications\UserAddTask;
use Carbon\Carbon;


class ChonggongbaobeiListener
{

    public $process;

    public $processModel;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * 待办事项发起消息通知
     *
     * @param $user
     */
    public function message($user) {

        $i = 0;
        array_set($message, 'avatar', 'chonggong.png');
        array_set($message, 'title', '您有一条充公报备!' . str_random(5));
        array_set($message, 'url', getenv('BOSS_MOBILE_HOST') . "/#/publishDetail?ids={$this->processModel->id}");

        $staff = Users::find($this->processModel->user_id);
        $markdown = "";
        $markdown .= "### {$staff->orgs[0]->name} - {$staff->name} 的充公报备 \n ";



        foreach ($this->process['show_content'] as $k => $v) {
            if (is_array($v)) continue;
            $markdown .= "#### {$k}: {$v} \n ";
            $i++;
            if ($i > 5) break;
        }

        array_set($message, 'markdown', $markdown);


        $user->notify(new UserAddTask($message));
    }

    /**
     * 新增待办事项
     * @param $task_role
     * @param null $options
     */
    public function task($task_role, $options = null) {

        $user_ids = Users::getRoleUsers($task_role);
        $name = Users::find($this->processModel->user_id)->name;

        foreach ($user_ids as $user_id) {
            $user = Users::find($user_id);
            $res = $this->processModel->tasks()->create([
                'flow_type' => $this->processModel->processable_type,
                'user_id'   =>  $user_id,
                'title'   =>  $name . "的充公报备",
                'begin_at'   =>  Carbon::now(),
                'important_level'   =>  1,
                'is_cc' =>  false
            ]);

            if ($res) {
                $this->message($user);
            }
        }
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(ChonggongbaobeiEvent $event)
    {

        $originEvent = $event->originEvent;
        $process = $originEvent->getSubject();
        $task_role = $event->taskRole;

        $process_content = $process->content;
        $this->process = $process_content;
        $this->processModel = $process;

        if (array_keys($originEvent->getMarking()->getPlaces())[0] == 'published') {

        }

        $this->task($task_role);

    }
}
