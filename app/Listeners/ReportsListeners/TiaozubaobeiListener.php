<?php

namespace App\Listeners\ReportsListeners;

use App\Events\Boss3Event;
use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\TiaozubaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Exceptions\AbortException;
use App\Http\Resources\UsersResource;
use App\Listeners\Work;
use App\Models\ContractRenter;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\HouseGoodNews;
use App\Notifications\UserAddTask;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;


class TiaozubaobeiListener extends Work
{

    public $name = "调租报备";

    public $avatar = "tiaofangxibao.png";

    public $happyAvatar = "tiaofang-gif.gif";

    public $title = "您有一条最新的调租报备!";

    public $ending = "请您尽快处理";
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(TiaozubaobeiEvent $event)
    {

        $this->nodeInit($event);

        if ($this->place == 'published') {
            //存储客户

            /*$oldContractRenter = ContractRenter::find($this->process['contract_id_rent']);
            if ($oldContractRenter) {
                $customer = $oldContractRenter->customers()->first();
                array_set($this->process, "type.id", 5);

                //存储调租合同
                $renter = event(new ContractRenterStoreEvent($customer, $this->process));

                $customerIds = $oldContractRenter->customers()->pluck('customer_id')->toArray();

                if (count($customerIds)) {
                    $renter[0]->customers()->sync(array_unique($customerIds));
                }

                $this->taskPublished();
                if (array_get($this->process, 'show_robot')) {
                    array_set($this->process, "show_robot.7", $this->happyAvatar);
                    Notification::send(Users::find($this->process['bulletin_staff_id']), new HouseGoodNews(array_get($this->process, 'show_robot')));
                }

            } else {
                throw new AbortException("未找到老的租房合同");
            }*/

            event(new Boss3Event($this->process, $this->processModel->id, array_get($this->process, 'bulletin_type')));
            $this->taskPublished();
            if (array_get($this->process, 'show_robot')) {
                array_set($this->process, "show_robot.7", $this->happyAvatar);
                Notification::send(Users::find($this->process['bulletin_staff_id']), new HouseGoodNews(array_get($this->process, 'show_robot')));
            }
        }

        if (str_contains($this->place, '_rejected')) {
            $this->taskRejected();
        }

    }
}
