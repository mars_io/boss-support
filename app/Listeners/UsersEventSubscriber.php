<?php

namespace App\Listeners;

use App\Models\Organizations;
use EasyDingTalk\Application;
use App\Models\Users;


class UsersEventSubscriber
{
    protected $ding;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $options = [
            'corp_id' => config('services.dingtalk.corp_id'),
            'corp_secret' => config('services.dingtalk.corp_secret'),
        ];
        $this->ding = new Application($options);
    }

    public function onStoreUser($event) {

        $org_ids = $event->request->input('org_id');
        $ding_department_ids = Organizations::whereIn('id', $org_ids)->pluck('ding_department_id')->toArray();

        if (!$ding_department_ids) {
            return false;
        } else {
            $result = $this->ding->user->create([
                'name' => $event->request->input('name'),
                'mobile' => $event->request->input('phone'),
                'department' => $ding_department_ids,
                'isSenior'  => false
            ]);

            return $result['errmsg'] == 'ok' ? $result : false;
        }
    }

    public function onUpdateUser($event) {
        $user = Users::find($event->user_id);
        $ding_user_id = $user->ding_user_id;
        if ($user->is_on_job) {
            $result = $this->ding->user->delete((array)$ding_user_id);
            return $result['errmsg'] == 'ok' ? $result : false;
        }else {

        }

        if (!$user || !$ding_user_id) {
            return false;
        }
        $org_ids = $event->request->input('org_id');
        $ding_department_ids = Organizations::whereIn('id', $org_ids)->pluck('ding_department_id')->toArray();

        $result = $this->ding->user->update([
            'userid' => $ding_user_id,
            'name' => $event->request->input('name'),
            'mobile' => $event->request->input('phone'),
            'department' => $ding_department_ids
        ]);
        return $result['errmsg'] == 'ok' ? $result : false;
    }

    public function onDestroyUser($event) {

        $ding_user_id = $event->user->ding_user_id;
        $result = $this->ding->user->delete((array)$ding_user_id);
        return $result['errmsg'] == 'ok' ? $result : false;
    }

    public function subscribe($event) {

        $event->listen(
            "App\Events\UserStoreEvent",
            "App\Listeners\UsersEventSubscriber@onStoreUser"
            );

        $event->listen(
            "App\Events\UserUpdateEvent",
            "App\Listeners\UsersEventSubscriber@onUpdateUser"
        );

        $event->listen(
            "App\Events\UserDestroyEvent",
            "App\Listeners\UsersEventSubscriber@onDestroyUser"
        );


    }
}
