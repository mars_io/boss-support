<?php

namespace App\Listeners;

use App\Events\ContractLordStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\FangwuzhiliangbaobeiEvent;
use App\Events\ReportsEvents\QingtuibaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\TeshushixiangbaobeiEvent;
use App\Events\ReportsEvents\TiaozubaobeiEvent;
use App\Events\ReportsEvents\TuikuanbaobeiEvent;
use App\Events\ReportsEvents\TuizubaobeiEvent;
use App\Events\ReportsEvents\WeikuanfangzubaobeiEvent;
use App\Events\ReportsEvents\WeishouxianzubaobeiEvent;
use App\Events\ReportsEvents\WeishouxianzuquerenbaobeiEvent;
use App\Events\ReportsEvents\XushoubaobeiEvent;
use App\Events\ReportsEvents\XuzubaobeiEvent;
use App\Events\ReportsEvents\ZhadanbaobeiEvent;
use App\Events\ReportsEvents\ZhongjiefeibaobeiEvent;
use App\Events\ReportsEvents\ZhuanzubaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Exceptions\AbortException;
use App\Models\Process;
use App\Models\Tasks;
use App\Models\Users;
use App\Notifications\UserAddTask;
use App\Notifications\UserJoinCompany;
use Brexis\LaravelWorkflow\Events\CompletedEvent;
use Brexis\LaravelWorkflow\Events\EnteredEvent;
use Brexis\LaravelWorkflow\Events\EnterEvent;
use Brexis\LaravelWorkflow\Events\GuardEvent;
use Brexis\LaravelWorkflow\Events\LeaveEvent;
use Brexis\LaravelWorkflow\Events\TransitionEvent;
use Brexis\LaravelWorkflow\Facades\WorkflowFacade as Workflow;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Request;

class ProcessEventSubscriber
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function getWorkflowNode($originEvent, $node) {
        return config("workflow." . $originEvent->getWorkflowName() . ".transitions." . $originEvent->getTransition()->getName() . "." . $node);
    }

    public function getTransitions($workFlowName) {
        $workFlow = config("workflow." . $workFlowName . ".transitions");
        return array_keys($workFlow);
    }

    public function onGuard(GuardEvent $event) {

        $originEvent = $event->getOriginalEvent();
        $guard = $this->getWorkflowNode($originEvent, 'node.guard');

        $user = Auth::guard('api')->user();

        $userId = NULL;
        //TODO：上线前必须只通过当前会话用户
        if (!$user) {
            if (($userId = Request::input('user_id'))) {
                $user = Users::find($userId);
            }
        }
        $userId = $userId ? $userId : NULL;

        if (!$user) {
            throw new AbortException("{$userId} :Api应用认证失败，Token过期");
        }

        $hasRole = $user->hasRole($guard);

        if ($originEvent->getWorkflowName() == "shoufangbaobei") {

            if (!$hasRole) {
                //不具备角色，不能进行流程的操作
                //TODO: 临时打开走流程，上线前需要设置为True
                $originEvent->setBlocked(false);
            }
        }

        if ($originEvent->getWorkflowName() == "zufangbaobei") {
            if (!$hasRole) {
                //不具备角色，不能进行流程的操作
                //TODO: 临时打开走流程，上线前需要设置为True
                $originEvent->setBlocked(false);
            }
        }

        if ($originEvent->getWorkflowName() == "chonggongbaobei") {
            if (!$hasRole) {
                //不具备角色，不能进行流程的操作
                //TODO: 临时打开走流程，上线前需要设置为True
                $originEvent->setBlocked(false);
            }
        }

        if ($originEvent->getWorkflowName() == "weikuanfangzubaobei") {
            if (!$hasRole) {
                //不具备角色，不能进行流程的操作
                //TODO: 临时打开走流程，上线前需要设置为True
                $originEvent->setBlocked(false);
            }
        }

    }

    public function onLeave(LeaveEvent $event) {}

    public function onTransition(TransitionEvent $event){}

    public function onEnter(EnterEvent $event){}

    public function onEntered(EnteredEvent $event){}


    public function onCompleted(CompletedEvent $event){

        //合同结束状态：1炸单， 2正常退租，3违约退租  4协商退租， 5转租， 6调租
        //合同标记： 收房（1：新收，2：续收） 租房（1：新租，

        $originEvent = $event->getOriginalEvent();
        $task_role = $this->getWorkflowNode($originEvent, 'node.task');
        $result = false;

        if ($originEvent->getWorkflowName() == "contract_doc_verify") {
            return true;
        }

        if ($originEvent->getWorkflowName() == "visit_verify") {
            return true;
        }

        if ($originEvent->getWorkflowName() == "shoufangbaobei") {
            $result = event(new ShoufangbaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "zufangbaobei") {
            $result = event(new ZufangbaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "chonggongbaobei") {
            $result = event(new ChonggongbaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "weikuanfangzubaobei") {
            $result = event(new WeikuanfangzubaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "teshushixiangbaobei") {
            $result = event(new TeshushixiangbaobeiEvent($originEvent, $task_role));

        }

        if ($originEvent->getWorkflowName() == "zhongjiefeibaobei") {
            $result = event(new ZhongjiefeibaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "qingtuibaobei") {
            $result = event(new QingtuibaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "zhadanbaobei") {
            $result = event(new ZhadanbaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "tuikuanbaobei") {
            $result = event(new TuikuanbaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "tuizubaobei") {
            $result = event(new TuizubaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "xuzubaobei") {
            $result = event(new XuzubaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "weishouxianzubaobei") {
            $result = event(new WeishouxianzubaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "weishouxianzuqueren") {

            $result = event(new WeishouxianzuquerenbaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "xushoubaobei") {
            $result = event(new XushoubaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "tiaozubaobei") {
            $result = event(new TiaozubaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "zhuanzubaobei") {

            $result = event(new ZhuanzubaobeiEvent($originEvent, $task_role));
        }

        if ($originEvent->getWorkflowName() == "fangwuzhiliangbaobei") {

            $result = event(new FangwuzhiliangbaobeiEvent($originEvent, $task_role));
        }

        $process = $originEvent->getSubject();
        if ($process->getPlace()['status'] != 'review') {
            $process->finish_at = Carbon::now();
            $process->save();
        }

        return $result;
    }

    public function subscribe($event) {

        $event->listen(
            "Brexis\LaravelWorkflow\Events\GuardEvent",
            "App\Listeners\ProcessEventSubscriber@onGuard"
            );

        $event->listen(
            "Brexis\LaravelWorkflow\Events\LeaveEvent",
            "App\Listeners\ProcessEventSubscriber@onLeave"
        );

        $event->listen(
            "Brexis\LaravelWorkflow\Events\TransitionEvent",
            "App\Listeners\ProcessEventSubscriber@onTransition"
        );

        $event->listen(
            "Brexis\LaravelWorkflow\Events\EnterEvent",
            "App\Listeners\ProcessEventSubscriber@onEnter"
        );

        $event->listen(
            "Brexis\LaravelWorkflow\Events\EnteredEvent",
            "App\Listeners\ProcessEventSubscriber@onEntered"
        );

        $event->listen(
            "Brexis\LaravelWorkflow\Events\CompletedEvent",
            "App\Listeners\ProcessEventSubscriber@onCompleted"
        );


    }
}
