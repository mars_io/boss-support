<?php

namespace App\Listeners\Auth;

use App\Events\ContractLordStoreEvent;
use App\Events\ContractRenterStoreEvent;
use App\Events\CustomerStoreEvent;
use App\Events\HouseStoreEvent;
use App\Events\ReportsEvents\ChonggongbaobeiEvent;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\ReportsEvents\ZufangbaobeiEvent;
use App\Http\Resources\UsersResource;
use App\Models\Users;
use App\Notifications\UserAddTask;
use Carbon\Carbon;
use Laravel\Passport\Events\AccessTokenCreated;
use Laravel\Passport\Token;


class RevokeOldTokens
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(AccessTokenCreated $event)
    {
        Token::where('id', '!=', $event->tokenId)
            ->where('user_id', $event->userId)
            ->where('client_id', $event->clientId)
            ->where('expires_at', '<', Carbon::now())
            ->orWhere('revoked', true)
            ->delete();
    }
}
