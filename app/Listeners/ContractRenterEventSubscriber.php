<?php

namespace App\Listeners;

use App\Events\CompatibleEvent;
use App\Events\HouseStatusEvent;
use App\Exceptions\AbortException;
use App\Models\ContractRenter;
use App\Models\Houses;
use Illuminate\Http\Request;

class ContractRenterEventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function onStoreContractRenter($event) {

        $process = $event->process;
        $customer = $event->customer[0] ? $event->customer[0] : $event->customer;

        $renter = new ContractRenter();


        if (array_get($process, 'contract_number')){
            $existContract = ContractRenter::where('contract_number', array_get($process, 'contract_number'))->first();
            if ($existContract) {
                throw new AbortException("该租房合同编号已经存在: " . array_get($process, 'contract_number'));
            }
        }

        if (!array_get($process, 'house_id')){
            throw new AbortException("未获取到合法的房屋");
        }

        $renter->contract_number = array_get($process, 'contract_number');
        $renter->rentable_id = array_get($process, 'house_id');//TODO: 改成房屋id
        $renter->rentable_type = Houses::class;

        //1租房 2转租 3续租 4未收先租 5调租
        $renter->type = array_get($process, 'type.id');

        $renter->sign_month = array_get($process, 'month');

        $renter->sign_remainder_day = array_get($process, 'day');

        $renter->sign_at = array_get($process, 'sign_date');

        $renter->start_at = array_get($process, 'begin_date');

        $renter->end_at = array_get($process, 'end_date');

        $renter->duration_days = array_get($process, 'duration_days');

        $renter->month_price = array_get($process, 'price');

        $renter->mortgage_price = array_get($process, 'deposit');

        $renter->purchase_way = array_get($process, 'purchase_way');

        $renter->pay_way = array_get($process, 'pay_way');

        $renter->pay_bet = array_get($process, 'pay_way_bet');

        $renter->pay_account_info = array_get($process, 'pay_account_info');

        $renter->is_joint = array_get($process, 'share.id', 0);

        $renter->is_agency = array_get($process, 'is_agency.id', 0);

        //是否以公司名义签单
        $renter->is_corp = array_get($process, 'is_corp.id', 1);

        $renter->agency_info = array_get($process, 'agency_info');

        $renter->money_sum = array_get($process, 'money_sum');

        $renter->money_table = array_get($process, 'money_table');

        $renter->album = array_get($process, 'album');

        $renter->property_payer = array_get($process, 'property_payer.id');

        $renter->property_price = array_get($process, 'property');

        $renter->final_payment_at    =   array_get($process, 'retainage_date');

        $renter->receipt_number    =   array_get($process, 'receipt');

        $renter->city_name = array_get($process, 'city_name');

        $renter->remark = array_get($process, 'remark');

        $renter->user_id = array_get($process, 'bulletin_staff_id');

        $renter->sign_user_id = array_get($process, 'staff_id');

        $renter->sign_org_id = array_get($process, 'department_id');

        $renter->org_id = array_get($process, 'department_id');


        if ($renter->save()) {
            $renter->customers()->sync($customer->id);

            event(new CompatibleEvent(true, $renter->id, $process, array_get($process, 'bulletin_type')));

            //1租房 2转租 3续租 4未收先租 5调租
            if ($renter->type === 1){
                $report = 4;
            }
            if ($renter->type === 2){
                $report = 6;
            }
            if ($renter->type === 3){
                $report = 5;
            }
            if ($renter->type === 5){
                $report = 7;
            }
            //TODO: 需要兼容合租房
            if (in_array($renter->type, [1, 2, 3, 5])) {
                event(new HouseStatusEvent($report, $renter->rentable_id, $renter->id));
            }
            return $renter;
        }
        return false;
    }

    public function onUpdateContractRenter($event) {
        $process = $event->process;
        $renter = ContractRenter::find(array_get($process, 'contract_id_rent'));
        $renter->rentable_id = array_get($process, 'house_id');//TODO: 改成房屋id
        $renter->rentable_type = Houses::class;

        //1租房 2转租 3续租 4未收先租 5调租
        $renter->type = array_get($process, 'type.id');

        $renter->sign_at = array_get($process, 'sign_date');

        $renter->start_at = array_get($process, 'begin_date');

        $renter->end_at = array_get($process, 'end_date');

        $renter->duration_days = array_get($process, 'duration_days');

        $renter->month_price = array_get($process, 'price');

        $renter->mortgage_price = array_get($process, 'deposit');

        $renter->pay_way = array_get($process, 'pay_way');

        $renter->pay_bet = array_get($process, 'pay_way_bet');

        $renter->pay_account_info = array_get($process, 'pay_account_info');

        $renter->is_joint = array_get($process, 'share.id', 0);

        $renter->is_agency = array_get($process, 'is_agency.id', 0);

        //是否以公司名义签单
        $renter->is_corp = array_get($process, 'is_corp.id', 1);

        $renter->agency_info = array_get($process, 'agency_info');

        $renter->album = array_get($process, 'album');

        $renter->property_payer = array_get($process, 'property_payer.id');

        $renter->property_price = array_get($process, 'property');

        $renter->final_payment_at    =   array_get($process, 'retainage_date');

        $renter->receipt_number    =   array_get($process, 'receipt');

        $renter->city_name = array_get($process, 'city_name');

        $renter->remark = array_get($process, 'remark');

        $renter->user_id = array_get($process, 'bulletin_staff_id');

        $renter->sign_user_id = array_get($process, 'staff_id');

        $renter->sign_org_id = array_get($process, 'department_id');

        if ($renter->save()) {
            return $renter;
        }
        return false;
    }

    public function subscribe($event) {

        $event->listen(
            "App\Events\ContractRenterStoreEvent",
            "App\Listeners\ContractRenterEventSubscriber@onStoreContractRenter"
        );

        $event->listen(
            "App\Events\ContractRenterUpdateEvent",
            "App\Listeners\ContractRenterEventSubscriber@onUpdateContractRenter"
        );

    }
}
