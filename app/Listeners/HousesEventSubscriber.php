<?php

namespace App\Listeners;

use App\Exceptions\AbortException;
use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Houses;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class HousesEventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function onStoreHouse($event) {

        $process = $event->process;
        $houseName = [];
        $house = new Houses();

        if (array_get($process, 'community.village_name')) {
            $door_address_str = array_get($process, 'door_address_str');
            $py = pinyin_abbr(array_get($process, 'community.village_name')) . $door_address_str;
            $pinyin = str_replace(' ','', pinyin_sentence(array_get($process, 'community.village_name'))) . $door_address_str;
            $house->py = $py;
            $house->pinyin = $pinyin;
            $house->name = array_get($process, 'community.village_name') . $door_address_str;
        }

        $house->building = array_get($process, 'door_address.0');
        $house->unit = array_get($process, 'door_address.1');
        $house->house_number = array_get($process, 'door_address.2');

        $house->room = array_get($process, 'house_type.0');
        $house->hall = array_get($process, 'house_type.1');
        $house->toilet = array_get($process, 'house_type.2');

        $house->area = array_get($process, 'area');
        $house->floor = array_get($process, 'floor');
        $house->floors = array_get($process, 'floors');

        //TODO: 异步更新房屋照片
        $house->album   = array_get($process, 'photo');

        $house->village_id = array_get($process, 'community.id');
        $house->village_name = array_get($process, 'community.village_name');
        $house->city_name = array_get($process, 'community.city_name');

        $house->decoration = array_get($process, 'decorate.id');
        $house->house_identity = array_get($process, 'property_type.id');

        //$house->suggest_price = array_get($process, 'price');
        $house->user_id = array_get($process, 'staff_id');
        $house->org_id = array_get($process, 'department_id');

        $house->house_res = $process;

        if (Houses::where('name', $house->name)->first()) {
            throw new AbortException("房屋地址不能重复");
        }

        return $house->save() ? $house : false;
    }

    /**
     * 维护房屋状态数据
     *
     * @param $event
     */
    public function onStatusHouse($event) {

        //---- 合同标记 ----
        //收房合同，1新收 2续收
        //租房合同，1新租 2转租 3续租 4未收先租 5调租

        //---- 合同状态 ----
        //1:未签约， 2：已签约， 3：快到期（60天内）， 4：已结束， 5：已过期

        //---- 合同结束状态 ----
        //1炸单， 2正常退租，3违约退租  4协商退租， 5转租， 6调租

        //---- 房屋状态 ----
        //已出租，未出租
        //！！！！最新：未出租，待签约，待收房，已出租，已结束

        //---- 房屋状态触发场景 ----
        //1：新收， 2：续收， 3：收房退租 4：新租， 5：续租，
        // 6：转租， 7：调租， 8：租房退租，
        //9：收房炸单， 10：收房取消炸单
        //11: 租房炸单， 12：租房取消炸单
        $house = Houses::find($event->house_id);

        if (in_array($event->report, [1, 2, 3, 9, 10])){
            $lord = ContractLord::find($event->contract_id);
        }else {
            $renter = ContractRenter::find($event->contract_id);
        }
        //TODO: 后面需要兼容合租房
        switch ($event->report) {

            case 1://收房

                //该房屋最新的一条租房合同
                $renter = ContractRenter::where('rentable_id', $house->id)
                    ->whereNotNull('contract_number')
                    ->whereNotNull('end_real_at')
                    ->orderBy('id', 'desc')
                    ->orderBy('end_at', 'desc')
                    ->first();

                if ($renter) {

                    $rent_end_days = $renter->end_at;
                    $lord_end_days = $lord->end_at;
                    $diffTime = strtotime($rent_end_days) - strtotime($lord_end_days);
                    $house->rent_end_than_days = $diffTime/3600/24;
                }else {
                    $house->rent_end_than_days = NULL;
                }
                $house->lord_start_at = $lord->start_at;
                $house->ready_end_at = date("Y-m-d", strtotime("+{$lord->ready_days} days", strtotime($lord->start_at)));
                $house->lord_end_at = $lord->end_at;
                $house->warning_init_at = $lord->start_at;

                $house->save();
                break;

            case 2://续收
                $renter = ContractRenter::where('rentable_id', $house->id)
                    ->whereNotNull('contract_number')
                    ->whereNotNull('end_real_at')
                    ->orderBy('id', 'desc')
                    ->orderBy('end_at', 'desc')
                    ->first();
                if ($renter) {
                    $rent_end_days = $renter->end_at;
                    $lord_end_days = $lord->end_at;

                    $diffTime = strtotime($rent_end_days) - strtotime($lord_end_days);
                    $house->rent_end_than_days = $diffTime/3600/24;
                } else {
                    $house->rent_end_than_days = NULL;
                }
                $house->lord_start_at = $lord->start_at;
                $house->lord_end_at = $lord->end_at;
                $house->save();
                break;

            case 3://收房退房
                $house->rent_end_than_days = NULL;
                $house->lord_start_at = NULL;
                $house->ready_end_at = NULL;
                $house->lord_end_at = NULL;
                $house->warning_init_at = NULL;
                $house->lord_end_at = NULL;
                $house->lord_start_at = NULL;
                $house->again_rent_at = NULL;
                $house->rent_start_at = NULL;
                $house->save();
                break;

            case 4://新租

                $house->rent_end_than_days = $this->rent_than_lord_days($house, $renter);
                $house->ready_end_at = NULL;
                $house->rent_start_at = $renter->start_at;
                $house->warning_init_at = NULL;
                $house->save();
                break;

            case 5://续租
                $house->rent_end_than_days = $this->rent_than_lord_days($house, $renter);
                $house->ready_end_at = NULL;
                $house->rent_start_at = $renter->start_at;
                $house->warning_init_at = NULL;
                $house->save();
                break;

            case 6://转租
                $house->rent_end_than_days = $this->rent_than_lord_days($house, $renter);
                $house->ready_end_at = NULL;
                $house->rent_start_at = $renter->start_at;
                $house->warning_init_at = NULL;
                $house->save();
                break;

            case 7://调租
                $house->rent_end_than_days = $this->rent_than_lord_days($house, $renter);
                $house->ready_end_at = NULL;
                $house->rent_start_at = $renter->start_at;
                $house->warning_init_at = NULL;
                $house->save();
                break;

            case 8://租房退租

                $house->rent_end_than_days = NULL;
                $house->ready_end_at = date("Y-m-d", strtotime("+7 days"));

                $house->rent_start_at = NULL;
                $house->warning_init_at = date("Y-m-d", time());
                $house->save();
                break;

            case 9://收房炸单

                $house->rent_end_than_days = NULL;
                $house->lord_start_at = NULL;
                $house->ready_end_at = NULL;
                $house->lord_end_at = NULL;
                $house->warning_init_at = NULL;
                $house->lord_end_at = NULL;
                $house->lord_start_at = NULL;
                $house->again_rent_at = NULL;
                $house->rent_start_at = NULL;
                $house->save();

            case 10://收房取消炸单

                $renter = ContractRenter::where('rentable_id', $house->id)
                    ->whereNotNull('contract_number')
                    ->whereNotNull('end_real_at')
                    ->orderBy('id', 'desc')
                    ->orderBy('end_at', 'desc')
                    ->first();

                if ($renter) {

                    $rent_end_days = $renter->end_at;
                    $lord_end_days = $lord->end_at;
                    $diffTime = strtotime($rent_end_days) - strtotime($lord_end_days);
                    $house->rent_end_than_days = $diffTime/3600/24;
                }else {
                    $house->rent_end_than_days = NULL;
                }
                $house->lord_start_at = $lord->start_at;
                $house->ready_end_at = date("Y-m-d", strtotime("+{$lord->ready_days} days", strtotime($lord->start_at)));
                $house->lord_end_at = $lord->end_at;
                $house->warning_init_at = $lord->start_at;

                $house->save();
                break;

            case 11://租房炸单
                $house->rent_end_than_days = NULL;
                $house->ready_end_at = date("Y-m-d", strtotime("+7 days"));
                $house->rent_start_at = NULL;
                $house->warning_init_at = date("Y-m-d", time());
                $house->save();
                break;

            case 12://租房取消炸单
                $this->rent_than_lord_days($house, $renter);
                $house->ready_end_at = NULL;
                $house->rent_start_at = $renter->start_at;
                $house->warning_init_at = NULL;
                $house->save();
                break;
            default:
        }

    }

    public function rent_than_lord_days($house, $renter) {
        $lord = ContractLord::where('house_id', $house->id)
            ->whereNotNull('contract_number')
            ->whereNotNull('end_real_at')
            ->orderBy('id', 'desc')
            ->orderBy('end_at', 'desc')
            ->first();

        if ($lord) {
            $rent_end_days = $renter->end_at;
            $lord_end_days = $lord->end_at;
            $diffTime = strtotime($rent_end_days) - strtotime($lord_end_days);
            return $diffTime/3600/24;
        }else {
            return NULL;
        }
    }

    public function subscribe($event) {

        $event->listen(
            "App\Events\HouseStoreEvent",
            "App\Listeners\HousesEventSubscriber@onStoreHouse"
        );

        $event->listen(
            "App\Events\HouseStatusEvent",
            "App\Listeners\HousesEventSubscriber@onStatusHouse"
        );


    }
}
