<?php

namespace App\Listeners;

use App\Exceptions\AbortException;
use App\Models\Organizations;
use Illuminate\Http\Request;
use EasyDingTalk\Application;

class OrganizationsEventSubscriber
{
    protected $ding;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $options = [
            'corp_id' => config('services.dingtalk.corp_id'),
            'corp_secret' => config('services.dingtalk.corp_secret'),
        ];
        $this->ding = new Application($options);

    }

    public function onIndexOrganization($event) {

        if (!Organizations::first()) {
            $result = $this->ding->department->list();
            if ($result['errmsg'] == 'ok') {
                Organizations::create([
                    'name' => $result['department'][0]['name'],
                    'ding_department_id' => $result['department'][0]['id'],
                    'order' => 1,
                    'is_enable' => true
                ]);
            }
        }
        return true;
    }

    public function onStoreOrganization($event) {

        $ding_department_id = Organizations::where('id', $event->request->input('parent_id'))->first()->ding_department_id;
        $result = $this->ding->department->create([
            'name' => $event->request->input('name'),
            'parentid' => $ding_department_id,
            'order' => $event->request->input('order')
        ]);

        return $result['errmsg'] == 'ok' ? $result : false;
    }

    public function onUpdateOrganization($event) {

        $ding_department_id = Organizations::find($event->org_id)->ding_department_id;
        if ($parent_id = $event->request->input('parent_id')) {
            $ding_parent_id = Organizations::find($event->request->input('parent_id'))->ding_department_id;
        }
        $result = $this->ding->department->update([
            'id' => $ding_department_id,
            'name' => $event->request->input('name'),
            'order' => $event->request->input('order'),
            'parentid' => $ding_parent_id
        ]);

        return $result['errmsg'] == 'ok' ? $result : false;
    }

    public function onDestroyOrganization($event) {
        $org  = Organizations::find($event->org_id);
        if ($org->ding_department_id) {
            $result = $this->ding->department->delete($org->ding_department_id);
            return $result['errmsg'] == 'ok' ? $result : false;
        } else {
            return false;
        }
    }

    public function subscribe($event) {

        $event->listen(
            "App\Events\OrganizationIndexEvent",
            "App\Listeners\OrganizationsEventSubscriber@onIndexOrganization"
        );

        $event->listen(
            "App\Events\OrganizationStoreEvent",
            "App\Listeners\OrganizationsEventSubscriber@onStoreOrganization"
            );

        $event->listen(
            "App\Events\OrganizationUpdateEvent",
            "App\Listeners\OrganizationsEventSubscriber@onUpdateOrganization"
        );

        $event->listen(
            "App\Events\OrganizationDestroyEvent",
            "App\Listeners\OrganizationsEventSubscriber@onDestroyOrganization"
        );


    }
}
