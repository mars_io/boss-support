<?php

namespace App\Listeners;

use App\Exceptions\AbortException;
use App\Models\Houses;
use App\Models\Rooms;
use Illuminate\Http\Request;

class RoomsEventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function onStoreRoom($event) {

        $process = $event->process;
        $house = $event->house[0] ? $event->house[0] : $event->house;
        $rooms = array_get($process, 'rooms_sum');
        if ($rooms) {
            for ($i = 1; $i <= $rooms; $i++) {
                Rooms::create([
                    'house_id'  =>  $house->id,
                    'name'      =>  $i,
                ]);
            }
            return true;
        }
        return false;


    }

    public function subscribe($event) {

        $event->listen(
            "App\Events\RoomStoreEvent",
            "App\Listeners\RoomsEventSubscriber@onStoreRoom"
        );


    }
}
