<?php

namespace App\Listeners;


use App\Models\Tasks;
use App\Models\Users;
use App\Notifications\UserJoinCompany;
use Brexis\LaravelWorkflow\Events\EnteredEvent;
use Brexis\LaravelWorkflow\Events\EnterEvent;
use Brexis\LaravelWorkflow\Events\GuardEvent;
use Brexis\LaravelWorkflow\Events\LeaveEvent;
use Brexis\LaravelWorkflow\Events\TransitionEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class ReportsEventSubscriber
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function onGuard(GuardEvent $event) {

        $originEvent = $event->getOriginalEvent();
        if ($originEvent->getWorkflowName() == "report") {
            $userId = Auth::user()->id;
            $user = Users::find($userId);
            $hasRole = $user->hasRole('reception');




            //dd($originEvent->getSubject()->id);
            //$f = $user->roles()->attach(1);
            //$f = $user->attachRole('reception');
            $config = "workflow." . $originEvent->getWorkflowName() . ".transitions." . $originEvent->getTransition()->getName();

            $guard = config( $config . ".node.guard");

            if ($hasRole) {
                $report = $originEvent->getSubject();

                $guard_can = (function () use ($user, $guard) {
                    return $user->$guard;
                });

                $users = Users::whereRoleIs(config( $config . ".node.users"))->get();

                foreach ($users as $user) {
                    $res = Tasks::create([
                        'flow_id'   => $originEvent->getSubject()->id,
                        'flow_type' => $originEvent->getWorkflowName(),
                        'user_id'   =>  $user->id,
                        'title' =>  $user->name . "的审批",
                        'type'  => 1,
                        'important_level'   => 1,
                        'is_cc' => false
                    ]);
                    if ($res) {
                        Notification::send($user, new UserJoinCompany(['黄三', '22']));
                    }

                }


            } else {
                //$originEvent->setBlocked(true);
            }
        }

    }

    public function onLeave(LeaveEvent $event) {
        $originEvent = $event->getOriginalEvent();
        if ($originEvent->getWorkflowName() == "report") {
            dump('onLeaveReport');
        }
    }

    public function onTransition(TransitionEvent $event){
        $originEvent = $event->getOriginalEvent();
        if ($originEvent->getWorkflowName() == "report") {
            dump('onTransitionReport');
        }
    }

    public function onEnter(EnterEvent $event){
        $originEvent = $event->getOriginalEvent();
        if ($originEvent->getWorkflowName() == "report") {
            dump('onEnterReport');
        }
    }

    public function onEntered(EnteredEvent $event){
        $originEvent = $event->getOriginalEvent();
        if ($originEvent->getWorkflowName() == "report") {
            dump('onEnteredReport');
        }
    }

    public function subscribe($event) {
        $event->listen(
            "Brexis\LaravelWorkflow\Events\GuardEvent",
            "App\Listeners\ReportsEventSubscriber@onGuard"
            );

        $event->listen(
            "Brexis\LaravelWorkflow\Events\LeaveEvent",
            "App\Listeners\ReportsEventSubscriber@onLeave"
        );

        $event->listen(
            "Brexis\LaravelWorkflow\Events\TransitionEvent",
            "App\Listeners\ReportsEventSubscriber@onTransition"
        );

        $event->listen(
            "Brexis\LaravelWorkflow\Events\EnterEvent",
            "App\Listeners\ReportsEventSubscriber@onEnter"
        );

        $event->listen(
            "Brexis\LaravelWorkflow\Events\EnteredEvent",
            "App\Listeners\ReportsEventSubscriber@onEntered"
        );


    }
}
