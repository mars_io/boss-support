<?php

namespace App\Criteria;

use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class HasFieldCriteria
 * @package namespace App\Criteria;
 */
class RecursionFieldCriteria implements CriteriaInterface
{
    protected $sourceField;

    protected $sourceValue;

    protected $parentField;

    /**
     * HasFieldCriteria constructor.
     * @param $sourceField
     * @param $sourceValue
     * @param $parentField
     */
    public function __construct($sourceField, $sourceValue, $parentField)
    {
        $this->sourceField = $sourceField;
        $this->sourceValue = $sourceValue;
        $this->parentField = $parentField;
    }


    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
//        $parentItem = new Collection();
//        $this->getRecursion($this->sourceField, $this->sourceValue, $this->parentField);
//        return $parentItem;

        $model = $model->where($this->field, '=', $this->value);
        return $model;
    }

    public function getRecursion($sourceField, $sourceValue, $parentField) {


//        foreach ($sourceValue as $k => $v) {
//            $collect = $model->where($sourceField, $sourceValue)->get([$parentField]);
//
//            if ($collect->$parentField == $sourceValue) {
//                $parentItem[] = $v;
//                $this->getRecursion($sourceItem, $parentItem, $v->parent_id);
//            }
//        }
    }
}
