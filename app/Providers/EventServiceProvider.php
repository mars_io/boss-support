<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],

        'App\Events\CommentEvent' => [
            'App\Listeners\CommentListener'
        ],

        'App\Events\CompatibleEvent' => [
            'App\Listeners\CompatibleListener'
        ],

        'App\Events\Boss3Event' => [
            'App\Listeners\Boss3Listener'
        ],

        'App\Events\Boss2Event' => [
            'App\Listeners\Boss2Listener'
        ],

        /*'Laravel\Passport\Events\AccessTokenCreated' => [
            'App\Listeners\Auth\RevokeOldTokens',
        ],
        'Laravel\Passport\Events\RefreshTokenCreated' => [
            'App\Listeners\Auth\PruneOldTokens',
        ],*/

        'App\Events\ReportsEvents\ShoufangbaobeiEvent' => [
            'App\Listeners\ReportsListeners\ShoufangbaobeiListener'
        ],
        'App\Events\ReportsEvents\ZufangbaobeiEvent' => [
            'App\Listeners\ReportsListeners\ZufangbaobeiListener'
        ],
        'App\Events\ReportsEvents\ChonggongbaobeiEvent' => [
            'App\Listeners\ReportsListeners\ChonggongbaobeiListener'
        ],
        'App\Events\ReportsEvents\WeikuanfangzubaobeiEvent' => [
            'App\Listeners\ReportsListeners\WeikuanfangzubaobeiListener'
        ],
        'App\Events\ReportsEvents\TeshushixiangbaobeiEvent' => [
            'App\Listeners\ReportsListeners\TeshushixiangbaobeiListener'
        ],
        'App\Events\ReportsEvents\ZhongjiefeibaobeiEvent' => [
            'App\Listeners\ReportsListeners\ZhongjiefeibaobeiListener'
        ],
        'App\Events\ReportsEvents\QingtuibaobeiEvent' => [
            'App\Listeners\ReportsListeners\QingtuibaobeiListener'
        ],
        'App\Events\ReportsEvents\ZhadanbaobeiEvent' => [
            'App\Listeners\ReportsListeners\ZhadanbaobeiListener'
        ],
        'App\Events\ReportsEvents\TuikuanbaobeiEvent' => [
            'App\Listeners\ReportsListeners\TuikuanbaobeiListener'
        ],
        'App\Events\ReportsEvents\TuizubaobeiEvent' => [
            'App\Listeners\ReportsListeners\TuizubaobeiListener'
        ],
        'App\Events\ReportsEvents\XuzubaobeiEvent' => [
            'App\Listeners\ReportsListeners\XuzubaobeiListener'
        ],
        'App\Events\ReportsEvents\WeishouxianzubaobeiEvent' => [
            'App\Listeners\ReportsListeners\WeishouxianzubaobeiListener'
        ],
        'App\Events\ReportsEvents\WeishouxianzuquerenbaobeiEvent' => [
            'App\Listeners\ReportsListeners\WeishouxianzuquerenbaobeiListener'
        ],
        'App\Events\ReportsEvents\XushoubaobeiEvent' => [
            'App\Listeners\ReportsListeners\XushoubaobeiListener'
        ],
        'App\Events\ReportsEvents\TiaozubaobeiEvent' => [
            'App\Listeners\ReportsListeners\TiaozubaobeiListener'
        ],
        'App\Events\ReportsEvents\ZhuanzubaobeiEvent' => [
            'App\Listeners\ReportsListeners\ZhuanzubaobeiListener'
        ],
        'App\Events\ReportsEvents\FangwuzhiliangbaobeiEvent' => [
            'App\Listeners\ReportsListeners\FangwuzhiliangbaobeiListener'
        ],

    ];

    protected $subscribe = [
        'App\Listeners\UsersEventSubscriber',
        'App\Listeners\OrganizationsEventSubscriber',
        'App\Listeners\HousesEventSubscriber',
        'App\Listeners\RoomsEventSubscriber',
        'App\Listeners\CustomersEventSubscriber',
        'App\Listeners\ContractRenterEventSubscriber',
        'App\Listeners\ContractLordEventSubscriber',
        'App\Listeners\ReportsEventSubscriber',
        'App\Listeners\ProcessEventSubscriber',
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();


        //Event::listen('*', function($event){

            //dump('A post was created');
        //});

        //
    }
}
