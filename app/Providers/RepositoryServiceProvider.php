<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\HouseRepository::class, \App\Repositories\HouseRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ReportRepository::class, \App\Repositories\ReportRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrganizationRepository::class, \App\Repositories\OrganizationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PowerRepository::class, \App\Repositories\PowerRepositoryEloquent::class);
        //:end-bindings:
    }
}
