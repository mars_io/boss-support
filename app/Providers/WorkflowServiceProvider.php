<?php

namespace App\Providers;

use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use Illuminate\Support\ServiceProvider;
use Brexis\LaravelWorkflow\WorkflowRegistry;


/**
 * @author Boris Koumondji <brexis@yahoo.fr>
 */
class WorkflowServiceProvider extends ServiceProvider
{
    protected $commands = [
        \Brexis\LaravelWorkflow\Commands\WorkflowDumpCommand::class,
    ];

    /**
     * Bootstrap the application services...
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);

        $this->app->singleton(
            'workflow', function ($app) {
            return new WorkflowRegistry($app['config']['workflow']);
        }
        );




    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['workflow'];
    }
}
