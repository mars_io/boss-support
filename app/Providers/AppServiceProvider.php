<?php

namespace App\Providers;

use App\Events\Event;
use App\Events\ReportsEvents\ShoufangbaobeiEvent;
use App\Events\UserGetUserEvent;
use App\Models\Organizations;
use App\Models\Users;
use App\Observers\OrganizationsObserver;
use App\Observers\UsersObserver;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Laravel\Horizon\Horizon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Users::observe(UsersObserver::class);
        Organizations::observe(OrganizationsObserver::class);

//        \Illuminate\Support\Facades\Event::listen('eloquent.created: App\post', function(){
//            dump('A post was created');
//        });

//        \Illuminate\Support\Facades\Event::listen(new UserGetUserEvent(), function(){
//            dump('A post was created');
//        });

        //dd(Event::listen());
        //


       /* \DB::listen(function($sql) {
            dump($sql->sql);
            dump($sql->bindings);
        });*/

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        //
    }
}
