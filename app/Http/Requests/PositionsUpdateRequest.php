<?php

namespace App\Http\Requests;

use App\Models\Organizations;
use App\Models\Positions;
use App\Models\PositionTypes;
use Illuminate\Foundation\Http\FormRequest;

class PositionsUpdateRequest extends ApiRequest
{

    protected $rules_key = [
        //'name' => '岗位名称',
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'name' => 'bail|required|max:20|unique:'.(new Positions())->getTable().',name',
        ];
    }
}
