<?php

namespace App\Http\Requests;

use App\Models\Organizations;
use App\Models\Users;
use Illuminate\Foundation\Http\FormRequest;

class UsersCreateRequest extends ApiRequest
{

    protected $rules_key = [
        'org_id'  => '所属部门id',
        //'phone' => '手机号'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'org_id'    =>  'bail|required',
            //'phone'     =>  'bail|required|integer|unique:'.(new Users())->getTable().',phone'
        ];
    }
}
