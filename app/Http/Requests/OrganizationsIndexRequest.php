<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrganizationsIndexRequest extends ApiRequest
{
    protected $rules_key = [
        'parent_id' => '父部门索引',
        'per_page_number'   =>  '每页请求数据量'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_id' => 'bail|max:255',
            'per_page_number'   =>  'bail|integer|max:500'
        ];
    }
}
