<?php

namespace App\Http\Requests;

use App\Models\Organizations;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationsStoreRequest extends ApiRequest
{

    protected $rules_key = [
        'name' => '部门名称',
        'parent_id' => '父部门索引'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:20',
            'parent_id' => 'bail|required|integer|exists:'.(new Organizations())->getTable().',id'
        ];
    }
}
