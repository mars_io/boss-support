<?php

namespace App\Http\Requests;

use App\Models\Modules;
use App\Models\Organizations;
use App\Models\Permission;
use App\Models\Positions;
use App\Models\PositionTypes;
use App\Models\Role;
use App\Models\Systems;
use Illuminate\Foundation\Http\FormRequest;

class SystemsCreateRequest extends ApiRequest
{

    protected $rules_key = [
        'name' => '系统标示',
        'display_name'  => '系统名称',
        'description'   =>  '系统描述',
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:20|regex:/[a-zA-Z_-]/|unique:'.(new Systems())->getTable().',name',
            'display_name' => 'bail|required|max:50',
            'description' => 'bail|required|max:255',
        ];
    }
}
