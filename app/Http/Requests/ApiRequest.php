<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApiRequest extends FormRequest
{

    protected $rules_key = [
        'q' => '查询参数',
        'd' => '的测试',
    ];

    protected $rules_val = [
        'required' => '是必填项',
        'min' => '最小长度为:min个字符',
        'max' => '最大长度为:max个字符',
        'between' => '在:min和:max之间',
        'integer' => '必须为整数',
        'exists' => '不存在',
        'unique' => '已经存在',
        'regex' =>  '不符合规则',
        'in'    =>  '不在指定枚举范围',
        'json' =>  '必须是json格式',
        'mimes' =>  '不符合mime类型枚举'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {

        $rules = $this->rules();
        $message = [];
        foreach ($rules as $key => $item) {
            $rule = explode("|", $item);
            foreach ($rule as $k => $v) {

                if (isset($this->rules_val[$v])) {
                    $message[$key . "." . $v] = $this->rules_key[$key] . $this->rules_val[$v];
                } elseif(str_contains($v, ':') && $size = explode(":", $v)) {

                    $message[$key . "." . $size[0]] = $this->rules_key[$key] . $this->rules_val[$size[0]];
                }

            }

        }
        return $message;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        return [
//            'q' => 'bail|required|max:255',
//            'd' => 'bail|required|max:255',
//        ];
    }
}
