<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportHouseTrusteeshipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'community_id' => [],
            'building' => [],
            'unit' => [],
            'layout' => [],
            'trusteeship_month' => [],
            'guarantee_month' => [],
            'ready_days' => [],
            'pay_way' => [],
            'month_price' => [],
            'mortgage_price	' => [],
            'pay_first_date' => [],
            'estate_remark' => [],
            //付款信息
            'pay_channel' => [],
            'account_bank' => [],
            'account_bank_subbranch' => [],
            'account_payee_name' => [],
            'account_number' => [],

            'lord_name' => [],
            'lord_phone' => [],
            'lord_payee_relationship' => [],
            'customer_from' => [],
            'contract_lord_album' => [],
            'remark' => [],
            'user_id' => [],
            'user_organization' => []
        ];
    }
}
