<?php

namespace App\Http\Requests;

use App\Models\Organizations;
use App\Models\Positions;
use App\Models\PositionTypes;
use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class PositionsStoreRequest extends ApiRequest
{

    protected $rules_key = [
        'name' => '岗位名称',
        'type'  =>  '所属职位id',
        'display_name' => '岗位标示',
        'description'   => '岗位描述'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:20|unique:'.(new Positions())->getTable().',name',
            'type' => 'bail|required|integer|exists:'.(new PositionTypes())->getTable().',id',
            'display_name' => 'bail|required|max:50|regex:/[a-zA-Z_-]/|unique:'.(new Role())->getTable().',name',
            'description' => 'bail|required|max:255'
        ];
    }
}
