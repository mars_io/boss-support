<?php

namespace App\Http\Requests;

use App\Models\Organizations;
use App\Models\Positions;
use App\Models\PositionTypes;
use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class RoleCreateRequest extends ApiRequest
{

    protected $rules_key = [
        'name' => '角色标示',
        'display_name'  => '角色名称',
        'description'   =>  '角色描述'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:20|regex:/[a-zA-Z_-]/|unique:'.(new Role())->getTable().',name',
            'display_name' => 'bail|required|max:50',
            'description' => 'bail|required|max:255',

        ];
    }
}
