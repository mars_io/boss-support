<?php

namespace App\Http\Requests;

use App\Models\Organizations;
use App\Models\Users;
use Illuminate\Foundation\Http\FormRequest;

class UsersUpdateRequest extends ApiRequest
{
    protected $rules_key = [
        //'phone' => '手机号'

    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'phone'     =>  'bail|integer|unique:'.(new Users())->getTable().',phone'
        ];
    }
}
