<?php

namespace App\Http\Requests;

use App\Models\Organizations;
use App\Models\Positions;
use App\Models\PositionTypes;
use Illuminate\Foundation\Http\FormRequest;

class RoleUpdateRequest extends ApiRequest
{

    protected $rules_key = [
        'display_name' => '角色名称',
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'bail|required|max:50',
        ];
    }
}
