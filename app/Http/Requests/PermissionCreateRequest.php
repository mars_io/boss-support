<?php

namespace App\Http\Requests;

use App\Models\Modules;
use App\Models\Organizations;
use App\Models\Permission;
use App\Models\Positions;
use App\Models\PositionTypes;
use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class PermissionCreateRequest extends ApiRequest
{

    protected $rules_key = [
        'name' => '权限标示',
        'display_name'  => '权限名称',
        'description'   =>  '权限描述',
        'mod_id'    =>  '所属模块id',
        'type'  => '权限分类'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return parent::messages();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:20|regex:/[a-zA-Z_-]/|unique:'.(new Permission())->getTable().',name',
            'display_name' => 'bail|required|max:50',
            'description' => 'bail|required|max:255',
            'mod_id'    => 'bail|required|integer|exists:'.(new Modules())->getTable().',id',
            'type'  =>  'bail|required|in:1,2'
        ];
    }
}
