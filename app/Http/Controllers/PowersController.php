<?php

namespace App\Http\Controllers;

use App\Exceptions\AbortException;
use App\Mod;
use App\Sys;
use App\Permission;
use App\Role;
use App\Models\Organization;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PowerCreateRequest;
use App\Http\Requests\PowerUpdateRequest;
use App\Repositories\PowerRepository;
use App\Validators\PowerValidator;
use App\User;


class PowersController extends Controller
{

    /**
     * @var PowerRepository
     */
    protected $repository;

    /**
     * @var PowerValidator
     */
    protected $validator;

    public function __construct(PowerRepository $repository, PowerValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * 添加子系统基础信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSys(Request $request) {

        $this->validate($request, [
            'name' => 'bail|required|unique:'.(new Sys())->getTable().'|max:255|regex:[^[a-zA-Z_-]+$]',
            'display_name' => 'bail|required|max:255',
            'description' => 'bail|required|max:255'
        ]);

        try {
            $sys = new Sys();
            $sys->name = $request->input('name');
            $sys->display_name = $request->input('display_name');
            $sys->description = $request->input('description');
            $sys->save();

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $sys ? 1 : 0,
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 修改系统信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setSys(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Sys())->getTable().',id',
            'name' => 'bail|required|unique:'.(new Sys())->getTable().'|max:255|regex:[^[a-zA-Z_-]+$]',
            'display_name' => 'bail|required|max:255',
            'description' => 'bail|required|max:255'
        ]);
        try {

            $sysId = $request->input('id');
            $sys = Sys::find($sysId);
            $sys->name = $request->input('name');
            $sys->display_name = $request->input('display_name');
            $sys->description = $request->input('description');
            $sys->save();

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $sys ? 1 : 0,
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 删除系统信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSys(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Sys())->getTable().',id',
        ]);

        try {
            $res = Sys::find($request->input('id'))->delete();

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res,
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 获取系统详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSys(Request $request) {
        //TODO: 获取相应系统关联的模块等信息（动态关联）
        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Sys())->getTable().',id',
        ]);

        try {
            $res = Sys::find($request->input('id'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }
    }

    /**
     * 查询系统
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchSys(Request $request) {
        $this->validate($request, [
            'keyword' => 'bail|required|max:255'
        ]);

        try {
            $res = Sys::search($request->input('keyword'))->paginate(10);

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }


    /**
     * 添加模块基础信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMod(Request $request) {

        $this->validate($request, [
            'sys_id' => 'bail|required|integer|exists:'.(new Sys())->getTable().',id',
            'name' => 'bail|required|unique:'.(new Mod())->getTable().'|max:255|regex:[^[a-zA-Z_-]+$]',
            'display_name' => 'bail|required|max:255',
            'description' => 'bail|required|max:255'
        ]);

        try {

            $res = Mod::create($request->all());

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res,
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 修改系统信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setMod(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Mod())->getTable().',id',
            'sys_id' => 'bail|integer|exists:'.(new Sys())->getTable().',id',
            'name' => 'bail|unique:'.(new Mod())->getTable().'|max:255|regex:[^[a-zA-Z_-]+$]',
            'display_name' => 'bail|max:255',
            'description' => 'bail|max:255'
        ]);

        try {
            $res = Mod::find($request->input('id'))->update($request->all());

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 删除系统信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMod(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Mod())->getTable().',id',
        ]);

        try {
            $res = Mod::find($request->input('id'))->delete();

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res ? 1 : 0,
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 获取系统详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMod(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Mod())->getTable().',id',
        ]);

        try {
            $res = Mod::find($request->input('id'));


            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', $exception->getMessage());
        }
    }

    /**
     * 查询系统
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchMod(Request $request) {
        $this->validate($request, [
            'keyword' => 'bail|required|max:255'
        ]);
        $res = Mod::search($request->input('keyword'))->paginate(10);
        try {

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 添加权限单元信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPermission(Request $request) {

        $this->validate($request, [
            'mod_id' => 'bail|required|integer|exists:'.(new Mod())->getTable().',id',
            'name' => 'bail|required|unique:'.(new Permission())->getTable().'|max:255|regex:[^[a-zA-Z_-]+$]',
            'display_name' => 'bail|required|max:255',
            'description' => 'bail|required|max:255',
            'type' => 'bail|integer',

        ]);

        try {

            $res = Permission::create($request->all());

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res,
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 修改系统信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setPermission(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Permission())->getTable().',id',
            'mod_id' => 'bail|integer|exists:'.(new Mod())->getTable().',id',
            'name' => 'bail|unique:'.(new Permission())->getTable().'|max:255|regex:[^[a-zA-Z_-]+$]',
            'display_name' => 'bail|max:255',
            'description' => 'bail|max:255',
            'type' => 'bail|integer'
        ]);

        try {
            $res = Permission::find($request->input('id'))->update($request->all());

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 删除系统信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePermission(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Permission())->getTable().',id',
        ]);

        try {
            $res = Permission::find($request->input('id'))->delete();

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res,
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 获取系统详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPermission(Request $request) {
        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Permission())->getTable().',id',
        ]);

        try {
            $res = Permission::find($request->input('id'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }
    }

    /**
     * 查询系统
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchPermission(Request $request) {
        //TODO: 动态关联其他模型，进行灵活搜索
        $this->validate($request, [
            'keyword' => 'bail|required|max:255'
        ]);
        $res = Permission::search($request->input('keyword'))->paginate(10);
        try {

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }


    /**
     * 添加角色信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addRole(Request $request) {

        $this->validate($request, [
            'name' => 'bail|required|unique:'.(new Role())->getTable().'|max:255|regex:[^[a-zA-Z_-]+$]',
            'display_name' => 'bail|required|max:255',
            'description' => 'bail|required|max:255',
            'type' => 'bail|required|integer',
            'organization_id' => 'bail|required|integer|exists:'.(new Organization())->getTable().',id',

        ]);

        try {
            $res = Role::create($request->all());

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res,
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 修改角色信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setRole(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Role())->getTable().',id',
            'organization_id' => 'bail|integer|exists:'.(new Organization())->getTable().',id',
            'name' => 'bail|unique:'.(new Role())->getTable().'|max:255|regex:[^[a-zA-Z_-]+$]',
            'display_name' => 'bail|max:255',
            'description' => 'bail|max:255',
            'type' => 'bail|integer'
        ]);

        try {
            $res = Role::find($request->input('id'))->update($request->all());

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 删除角色信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteRole(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Role())->getTable().',id',
        ]);

        try {
            $res = Role::find($request->input('id'))->delete();

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res,
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 获取角色详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRole(Request $request) {
        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Role())->getTable().',id',
        ]);

        try {
            $res = Role::find($request->input('id'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }
    }

    /**
     * 查询角色
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchRole(Request $request)
    {
        //TODO: 动态关联其他模型，进行灵活搜索
        $this->validate($request, [
            'keyword' => 'bail|required|max:255'
        ]);

        try {
            $res = Role::search($request->input('keyword'))->paginate(10);
            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        } catch (\Exception $exception) {
            throw new AbortException('401', 'Abort Error');
        }

    }

    /**
     * 用户附加角色
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachRole(Request $request) {

        $this->validate($request, [
            'user_id' => 'bail|required|integer|exists:'.(new User())->getTable().',id',
            'role_id' => 'bail|required|integer|exists:'.(new Role())->getTable().',id',
        ]);


        try{
            $res = User::find($request->input('user_id'))->attachRole($request->input('role_id'));
            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch(\Exception $exception) {
            throw new AbortException(401, 'Abort Error');
        }

    }

    /**
     * 用户分离角色
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachRole(Request $request) {
        $this->validate($request, [
            'user_id' => 'bail|required|integer|exists:'.(new User())->getTable().',id',
            'role_id' => 'bail|required|integer|exists:'.(new Role())->getTable().',id',
        ]);
        try{
            $res = User::find($request->input('user_id'))->detachRole($request->input('role_id'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch(\Exception $exception) {
            throw new AbortException(401, 'Abort Error');
        }

    }

    /**
     * 用户附加权限
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachPermission(Request $request) {
        $this->validate($request, [
            'user_id' => 'bail|required|integer|exists:'.(new User())->getTable().',id',
            'permission_id' => 'bail|required|integer|exists:'.(new Permission())->getTable().',id',
        ]);

        try{
            $res = User::find($request->input('user_id'))->attachPermission($request->input('permission_id'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch(\Exception $exception) {
            throw new AbortException(401, 'Abort Error');
        }

    }

    /**
     * 用户分离权限
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachPermission(Request $request) {
        $this->validate($request, [
            'user_id' => 'bail|required|integer|exists:'.(new User())->getTable().',id',
            'permission_id' => 'bail|required|integer|exists:'.(new Permission())->getTable().',id',
        ]);

        try {

            $res = User::find($request->input('user_id'))->detachPermission($request->input('permission_id'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch(\Exception $exception) {
            throw new AbortException(401, 'Abort Error');
        }

    }


    /**
     * 角色附加权限
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachPermissionOnRole(Request $request) {
        $this->validate($request, [
            'role_id' => 'bail|required|integer|exists:'.(new Role())->getTable().',id',
            'permission_id' => 'bail|required|integer|exists:'.(new Permission())->getTable().',id',
        ]);

        try {
            $res = Role::find($request->input('role_id'))->attachPermission($request->input('permission_id'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch(\Exception $exception) {
            throw new AbortException(401, 'Abort Error');
        }

    }

    /**
     * 角色分离权限
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachPermissionOnRole(Request $request) {
        $this->validate($request, [
            'role_id' => 'bail|required|integer|exists:'.(new Role())->getTable().',id',
            'permission_id' => 'bail|required|integer|exists:'.(new Permission())->getTable().',id',
        ]);

        try {
            $res = Role::find($request->input('role_id'))->detachPermission($request->input('permission_id'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch(\Exception $exception) {
            throw new AbortException(401, 'Abort Error');
        }

    }

    /**
     * 检查用户是否有权限进行操作
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(Request $request) {
        $this->validate($request, [
            'user_id' => 'bail|required|integer|exists:'.(new User())->getTable().',id',
            'permission_name' => 'bail|required|exists:'.(new Permission())->getTable().',name',
            'strict' => 'bail|nullable|boolean'
        ]);

        try {
            $user = User::find($request->input('user_id'));
            $res = $user->can($request->input('permission_name'), (boolean)$request->input('strict'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch(\Exception $exception) {
            throw new AbortException(401, 'Abort Error');
        }
    }

    /**
     * 获取指定用户的所有权限
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllPermissions(Request $request) {

        $this->validate($request, [
            'user_id' => 'bail|required|integer|exists:'.(new User())->getTable().',id'
        ]);

        try{
            $res = User::find($request->input('user_id'))->allPermissions();
            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch(\Exception $exception) {
            throw new AbortException(401, 'Abort Error');
        }

    }

    /**
     * 获取指定角色的所有用户
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersByRole(Request $request) {

        $this->validate($request, [
            'role_id' => 'bail|required|exists:'.(new Role())->getTable().',id'
        ]);

        try{
            $role_name = Role::find($request->input('role_id'))->name;
            $res = User::whereRoleIs($role_name)->get();

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }
        }catch(\Exception $exception) {
            throw new AbortException(401, 'Abort Error');
        }

    }
}
