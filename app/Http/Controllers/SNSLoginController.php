<?php

namespace App\Http\Controllers;

use App\Models\Users;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use EasyDingTalk\Application;
use Illuminate\Support\Facades\Redirect;

//TODO: [加功能] 钉钉用户修改手机号，回调Boss Support 系统，更新Boss系统用户资料
class SNSLoginController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        //https://oapi.dingtalk.com/connect/qrconnect?appid=dingoabeclxxwagukzyegm&response_type=code&scope=snsapi_login&state=STATE&redirect_uri=http://192.168.8.10/sns_login
        $tmp_auth_code = $request->get('code');
        $client = new Client();

        if (!Cache::has('app_token')) {

            //get $tmp_auth_code
            $response = $client->request(
                'GET',
                'https://oapi.dingtalk.com/sns/gettoken',
                ['query' => ['appid' => config('services.dingtalk.app_id'), 'appsecret' => config('services.dingtalk.app_secret')]]
            );
            $response_body = json_decode($response->getBody()->getContents(), true);
            if ((int)$response_body['errcode'] === 0) {
                $access_token = $response_body['access_token'];
                Cache::put('app_token', $access_token, 100);
            }

        } else {
            $access_token = Cache::get('app_token');
        }

        //get $openid, $unionid, $persistent_code
        $response = $client->request(
            'POST',
            'https://oapi.dingtalk.com/sns/get_persistent_code',
            [
                'query' => ['access_token' => $access_token],
                'headers' => ['Content-Type' => 'application/json'],
                'body' => json_encode([
                    'tmp_auth_code' => $tmp_auth_code
                ], JSON_UNESCAPED_UNICODE)
            ]

        );
        $response_body = json_decode($response->getBody()->getContents(), true);

        $unionid = $response_body['unionid'];

        if ($response_body['errmsg'] == 'ok') {

            $app = new Application(['corp_id' => config('services.dingtalk.corp_id'), 'corp_secret' => config('services.dingtalk.corp_secret')]);

            $info = $app->user->toUserId($unionid);

            $dingUserInfo = $app->user->get($info['userid']);
            if ((int)$dingUserInfo['errcode'] === 0) {
                $user = Users::where('phone', $dingUserInfo['mobile'])->withTrashed()->first();

                if ($user) {
                    if ($user->is_enable) {
                        return response('您已被本公司人力资源禁用，暂无法登陆系统', 200)
                            ->header('Content-Type', 'text/html');
                    }

                    if ($user->is_on_job) {
                        return response('您已被本公司人力资源离职，无法登陆系统', 200)
                            ->header('Content-Type', 'text/html');
                    }
                }

                if ($user && !$user->ding_user_id) {
                    $user->restore();
                    $user->ding_user_id = $info['userid'];
                    $user->is_on_job = NULL;
                    $user->deleted_at = NULL;
                    $user->save();
                }
                if (!$user) {

                    $user = Users::where('ding_user_id', $info['userid'])
                        ->whereNull('is_enable')
                        ->whereNull('is_on_job')
                        ->first();

                    if ($user) {
                        $user->phone = $dingUserInfo['mobile'];
                        $user->save();
                    } else {
                        $user = Users::create([
                            'name' => $dingUserInfo['name'],
                            'phone' => $dingUserInfo['mobile'],
                            'ding_user_id'  => $info['userid'],
                            'py'    =>  pinyin_abbr($dingUserInfo['name']),
                            'pinyin'    =>  implode('', pinyin($dingUserInfo['name'])),
                        ]);
                    }
                }
            }
            $user = $user->toArray();
            if ($user) {

                $code = str_random(32);
                $expiresAt = Carbon::now()->addMinutes(15);

                $res = Users::find($user['id'])->update([
                    'password' => bcrypt($code),
                    'phone_check_code_ttl' => $expiresAt,
                    'avatar' => $dingUserInfo['avatar'],
                    //'email' => $dingUserInfo['email']
                ]);
                $boss_client_url = getenv("BOSS_CLIENT_HOST");
                return response("<script>window.location.href='{$boss_client_url}/#/login?phone={$user['phone']}&code={$code}';</script>", 200);
            }

        }

        //TODO: 查询数据表中是否有绑定过的用户信息，如果有，从钉钉同步一次基础信息，如果没有绑定信息，跳转到非法页面

    }
}
