<?php

namespace App\Http\Controllers\Api;

use App\Events\UserDestroyEvent;
use App\Events\UserStoreEvent;
use App\Events\UserUpdateEvent;
use App\Exceptions\AbortException;
use App\Http\Requests\UsersCreateRequest;
use App\Http\Requests\UsersIndexRequest;
use App\Http\Requests\UsersUpdateRequest;
use App\Http\Resources\UsersResource;
use App\Http\Resources\UsersResourceCollection;
use App\Models\Organizations;
use App\Models\Positions;
use App\Models\Process;
use App\Models\Role;
use App\Models\Users;
use App\Notifications\UserJoinCompany;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UsersController extends ApiController
{

    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersIndexRequest $request)
    {
        try {
            $userCollect = Users::search($request->input('q'))
                ->where(function ($query) use ($request){

                    //按部门搜索
                    if ($request->input('org_id')){
                        if (!($org = Organizations::find($request->input('org_id')))) {
                            return $this->responseNotFound();
                        }

                        if ($request->input('is_recursion')) {

                            $org_ids = $org->getDescendantsAndSelf()
                                ->pluck('id')
                                ->toArray();

                            $ids = Organizations::whereIn('organizations.id', $org_ids)
                                ->join('org_user', 'organizations.id', '=', 'org_user.org_id')
                                ->get()
                                ->map(function ($user) {
                                    return $user->user_id;
                                })->unique()
                                ->toArray();

                        }else {
                            $ids = $org->users()
                                ->get()
                                ->map(function ($user) {
                                    return $user->id;
                                })
                                ->toArray();
                        }
                        $query->whereIn('id', $ids);
                    }

                    //按角色搜索用户
                    if (($role = $request->input('role'))) {
                        $roles = explode(',', $role);
                        $query->whereHas('roles', function ($roleQuery) use ($roles) {
                             $roleQuery->whereIn('name', $roles);
                        });
                    }

                    //搜索被禁用用户
                    if ($request->input('is_disable') !== null) {
                        if ($request->input('is_disable')) {
                            $query->whereNotNull('is_disable');
                        }else {
                            $query->whereNull('is_disable');
                        }
                    }

                    //搜索离职用户
                    if ($request->input('is_dimission') !== null) {
                        if ($request->input('is_dimission')) {
                            $query->whereNotNull('is_on_job');
                        } else {
                            $query->whereNull('is_on_job');
                        }
                    }

                    //入职日期
                    if ($request->input('create_start_time')) {
                        $query->whereBetween('created_at',
                            [
                                $request->input('create_start_time'),
                                $request->input('create_end_time', Carbon::now())
                            ]);
                    }
                })
                ->with('orgs')
                ->with('roles')
                ->orderBy('is_on_job', 'asc')
                ->orderBy('is_enable', 'asc')
                ->orderBy('id', 'asc')
                ->paginate($request->input('per_page_number'));

            return new UsersResourceCollection($userCollect);
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersCreateRequest $request)
    {
        try {
            Log::info("Request" . json_encode($request->all()));

            $role_ids = $org_ids = [];
            $request->offsetSet('org_id', (array)$request->input('org_id'));
            $org_ids = $request->input('org_id');

            foreach ($org_ids as $org_id) {
                if (!($org = Organizations::find($org_id))) {
                    return $this->setMessage("存在不合法的部门id: ". $org_id)->responseTransaction();
                }
            }

            $request->offsetSet('position_id', (array)$request->input('position_id'));
            $position_ids = $request->input('position_id');

            foreach ($position_ids as $pos_id) {
                if (!($pos = Positions::find($pos_id))) {
                    return $this->setMessage("职位id：" . $pos_id . "不存在")->responseTransaction();
                }
            }
            $user = Users::where('phone', $request->input('phone'))->withTrashed()->first();

            //如果该用户之前在系统中被删除过，恢复其正常状态，并且初始化他的入职时间
            if ($user && $user->deleted_at) {
                $user->restore();
                $user->is_on_job = NULL;
                $user->is_enable = NULL;
                $user->created_at = Carbon::now();
                $user->save();
            }

            if (!$user) {

                $user = Users::create([
                    'name' => $request->input('name'),
                    'phone' => $request->input('phone'),
                    'ding_user_id'  => $request->input('ding_user_id', NULL),
//                    'is_enable' =>  $request->input('is_enable'),
//                    'is_on_job' =>  $request->input('is_on_job'),
                    'py'    =>  pinyin_abbr($request->input('name')),
                    'pinyin'    =>  implode('', pinyin($request->input('name'))),
                ]);
            }

            //withTrashed
            if ($user && $position_ids) {
                $role_ids = Role::whereIn('position_id', $position_ids)->pluck('id')->toArray();
                $posResult = $user->roles()->sync($role_ids);
            }
            if ($user && $org_ids) {
                $orgResult = $user->orgs()->sync($org_ids);
            }

            $result = event(new UserStoreEvent($request));
            //$res = Users::find(1)->notify(new UserJoinCompany());
            $result = isset($result[0]) ? $result[0] : $result;

            if ($result) {
                $user->ding_user_id = $result['userid'];
                $user->save();
            }
            if ($user) {
                return $this->response($user);
            } else {
                return $this->setMessage("本地用户创建失败")->responseTransaction();
            }

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $user = Users::where('id', $id)
                ->orWhere('ding_user_id', $id)
                ->whereNotNull('ding_user_id')
                ->with('orgs')
                ->with('roles')
                ->first();

            if (!$user) {
                return $this->responseNotFound();
            }
            return $this->response(new UsersResource($user));
        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(UsersUpdateRequest $request, $id)
    {
        try {
            $user = Users::find($id);
            if (!$user) {
                return $this->responseNotFound();
            }
            $role_ids = $org_ids = [];
            $request->offsetSet('org_id',(array)$request->input('org_id'));
            $org_ids = $request->input('org_id');

            if (!$org_ids) {
                $org_ids = $user->orgs()->pluck('org_id')->toArray();
            }

            if ($org_ids) {

                foreach ($org_ids as $org_id) {

                    if (!($org = Organizations::find($org_id))) {
                        return $this->setMessage("存在不合法的部门id: ". $org_id)->responseTransaction();
                    }
                }
            }

            $request->offsetSet('position_id', (array)$request->input('position_id'));

            $position_ids = $request->input('position_id');
            if ($position_ids) {
                foreach ($position_ids as $pos_id) {
                    if (!($pos = Positions::find($pos_id))) {
                        return $this->setMessage("职位id：" . $pos_id . "不存在")->responseTransaction();
                    }
                }
            } else {
                $position_ids = $user->roles()->pluck('position_id')->toArray();
            }
            $role_ids = Role::whereIn('position_id', $position_ids)->pluck('id')->toArray();

            $user->roles()->sync((array)$role_ids);
            $user->orgs()->sync((array)$org_ids);

            if ($request->input('name')) {
                $request->offsetSet('py', pinyin_abbr($request->input('name')));
                $request->offsetSet('pinyin', implode('', pinyin($request->input('name'))));
            }

            if ($request->input('is_enable') !== null) {
                if ($request->input('is_enable')) {
                    $request->offsetSet('is_enable', NULL);
                }else {
                    $request->offsetSet('is_enable', Carbon::now());
                    DB::table('oauth_access_tokens')->where("user_id", $user->id)->delete();
                }
            }

            if ($request->input('is_on_job') !== null) {

                if ($request->input('is_on_job')) {
                    $request->offsetSet('is_on_job', NULL);

                    $user->is_on_job = NULL;
                    $user->save();

                    $user->update($request->all());

                    $orgIds = $user->orgs()->pluck('org_id')->toArray();

                    $request->offsetSet('name', $user->name);
                    $request->offsetSet('phone', $user->phone);
                    $request->offsetSet('org_id', $orgIds);

                    $result = event(new UserStoreEvent($request));
                    $result = isset($result[0]) ? $result[0] : $result;

                    if ($result) {
                        $user->ding_user_id = $result['userid'];
                        $user->save();
                    }
                    return $this->response(new UsersResource($user));

                }else {
                    $request->offsetSet('is_on_job', Carbon::now());

                    if (!$user->is_on_job) {
                        $result = event(new UserDestroyEvent($user));
                    }
                    $user->is_on_job = Carbon::now();
                    $user->save();
                    $user->update($request->all());
                    DB::table('oauth_access_tokens')->where("user_id", $user->id)->delete();
                    return $this->response(new UsersResource($user));
                }
            }

            $result = $user->update($request->all());

            if ($user->ding_user_id && !$user->is_on_job) {

                event(new UserUpdateEvent($request, $id));
            }

            //TODO: （启用|禁用）（离职|复职）员工前，必须解除所有关联的资源
            if ($result) {
                return $this->response($user);
            }else {
                return $this->setMessage("本地用户修改失败")->responseTransaction();
            }

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $user = Users::find($id);
            if (!$user) {
                return $this->responseNotFound();
            }
            //TODO: 删除员工前，必须解除所有关联的资源

            $user->orgs()->detach();
            $user->roles()->detach();
            $data = $user->delete();

            if ($data) {
                $result = event(new UserDestroyEvent($user));
                $result = isset($result[0]) ? $result[0] : $result;
            }

            if ($result) {
                return $this->response($data);
            } else {
                return $this->setMessage("远程同步钉钉失败")->responseTransaction();
            }

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function batch(Request $request) {
        try{

            putenv('x-batch=1');
            $data = [];

            if (!($batch = $request->batch) || !($batch = json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }
        }

        putenv('x-batch=0');
        return $this->response($data);
    }
}
