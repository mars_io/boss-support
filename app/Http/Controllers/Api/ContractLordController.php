<?php

namespace App\Http\Controllers\Api;

use App\Events\Boss2Event;
use App\Exceptions\AbortException;
use App\Http\Resources\ContractLordResourceCollection;
use App\Http\Resources\ContractLordResource;
use App\Http\Resources\HousesResource;
use App\Http\Resources\HousesResourceCollection;
use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Houses;
use App\Models\Organizations;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Brexis\LaravelWorkflow\Facades\WorkflowFacade as Workflow;
use Illuminate\Support\Facades\Auth;


class ContractLordController extends ApiController
{
    public function index(Request $request) {

        try{
            $ContractLordCollect = ContractLord::search($request->input('q'))
                ->where(function ($query) use ($request){

                    if (($doc_status = $request->input('doc_status'))) {
                        $query->where("doc_status->$doc_status", 1);

                        if ($doc_status == "draft") {
                            $query->orWhereNull("doc_status");
                        }

                    }

                    if (($visit_status = $request->input('visit_status'))) {
                        $query->where("visit_status->$visit_status", 1);
                        if ($visit_status == "draft") {
                            $query->orWhereNull("visit_status");
                        }
                    }

                    /*******精确*******/

                    // 回访情况,

                    // 上传日期, 签约日期，开始日期, 结束日期，
                    // 部门,
                    // 按时间搜索到期房源,
                    // 未上缴合同,

                    // 未上传合同(合同编号+合同照片必须同时存在才算合同已上传),
                    if ($request->input('un_upload')) {
                        $query->whereNull('contract_number');
                        $query->whereNull('lord_album->lord');
                    }

                    //上传日期
                    if ($request->input('publish_start_time')) {
                        $query->whereBetween('created_at',
                            [
                                $request->input('publish_start_time'),
                                $request->input('publish_end_time', Carbon::now())
                            ]);
                    }

                    //签约日期
                    if ($request->input('sign_start_time')) {
                        $query->whereBetween('sign_at',
                            [
                                $request->input('sign_start_time'),
                                $request->input('sign_end_time', Carbon::now())
                            ]);
                    }

                    //开始日期
                    if ($request->input('lord_start_start_time')) {
                        $query->whereBetween('start_at',
                            [
                                $request->input('lord_start_start_time'),
                                $request->input('lord_start_end_time', Carbon::now())
                            ]);
                    }

                    //结束日期
                    if ($request->input('lord_end_start_time')) {
                        $query->whereBetween('end_at',
                            [
                                $request->input('lord_end_start_time'),
                                $request->input('lord_end_end_time', Carbon::now())
                            ]);
                    }

                    if ($request->input('org_id') && (int)$request->input('org_id') !== 1) {
                        $org = Organizations::find($request->input('org_id'));
                        if ($org){
                            $org_ids = array_unique($org->getDescendantsAndSelf()->pluck('id')->toArray());
                            if ($request->input('or_user_id')) {
                                $query->whereIn('sign_org_id', $org_ids)->orWhere('user_id', $request->input('or_user_id'));
                            }else {
                                $query->whereIn('sign_org_id', $org_ids);
                            }
                        }
                    }

                    if ($request->input('user_id')) {
                        $query->where('user_id', $request->input('user_id'));
                    }

                    if (($house_id = $request->input('house_id'))) {
                        $house_id = array_unique(explode(',', $house_id));
                        $query->whereIn('house_id', $house_id);
                    }

                    // 合同状态,
                    if (($status = $request->input('status'))) {

                        if ((int)$status === 1) {

                            //正在运营
                            $query->whereNotNull('contract_number');
                            $query->whereNull('end_real_at');
                        }
                        if ((int)$status === 2) {
                            //快结束
                            $query->whereNotNull('contract_number');
                            //$query->whereBetween();
                            $query->whereNull('end_real_at');

                        }
                        if ((int)$status === 3) {
                            //已结束(到期未处理房源)
                            $query->whereNotNull('contract_number');
                            $query->whereNotNull('end_real_at');
                        }

                        if ((int)$status === 4) {
                            //签约中
                            $query->whereNull('contract_number');
                        }
                    }
                })
                ->with('house')
                ->with('house.rooms')
                ->with('agency_contract')
                ->with('customers')
                ->with('user')
                ->with('sign_user')
                ->with('sign_org')
                ->distinct()
                ->orderBy('id', 'desc')
                ->when(($report = $request->input('report')), function ($query) use ($report, $request) {

                    //续收
                    if ($report == "bulletin_collect_continued") {
                        $query->whereNotNull('contract_number');
                        $query->whereNull('end_real_at');
                        //return $query->take(1)->get();
                        return $query->paginate($request->input('per_page_number'));
                    }


                    //炸单
                    if ($report == "bulletin_lose") {

                        //取消炸单
                        if ((int)$request->input('able_type') === 1) {
                            $query->where('end_type', 1);
                        }

                        $query->whereNull('end_real_at');
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //退租
                    if ($report == "bulletin_checkout") {
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //租房
                    if ($report == "bulletin_rent_basic") {
                        $query->whereNotNull('contract_number');
                        $query->whereNull('end_real_at');
                        return $query->paginate($request->input('per_page_number'));

                        //return $query->take(1)->get();
                    }

                    //调房
                    if ($report == "bulletin_change") {

                        $query->whereNotNull('contract_number');
                        $query->whereNull('end_real_at');
                        //return $query->take(1)->get();
                        return $query->paginate($request->input('per_page_number'));

                    }

                    //充公
                    if ($report == "bulletin_confiscate") {
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //中介费
                    if ($report == "bulletin_agency") {
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //特殊事项
                    if ($report == "bulletin_special") {
                        return $query->paginate($request->input('per_page_number'));
                    }

                })
                ->when(!$request->input('report'), function ($query) use ($request) {
                    return $query->paginate($request->input('per_page_number'));
                });
            return new ContractLordResourceCollection($ContractLordCollect);
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(Request $request) {
        try {

            $user = Auth::guard('api')->user();
            if ($user) {
                $request->offsetSet('user_id', $user->id);
                if ($user->orgs->isNotEmpty()) {
                    $request->offsetSet('org_id', $user->orgs[0]->id);
                }
            }

            $lord = ContractLord::create($request->all());

            if ($request->input('customer_id')) {
                $customerIds = explode(',', $request->input('customer_id'));
                $lord->customers()->sync($customerIds);
            }


            return $this->response($lord);
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

        $lordInfo = ContractLord::where('id', $id)
            ->orWhere('contract_number', $id)
            ->with('customers')
            ->with('house')
            ->with('agency_contract')
            ->with('user')
            ->with('sign_user')
            ->with('sign_org')
            ->first();

        if (!$lordInfo) {
            return $this->responseNotFound();
        }

        $workflow = Workflow::get($lordInfo, 'contract_doc_verify');
        $transitions = $workflow->getEnabledTransitions($lordInfo);
        $operation = [];
        foreach ($transitions as $transition) {
            $operation[] = $transition->getName();
        }
        $lordInfo->contract_operation = $operation;

        $workflow = Workflow::get($lordInfo, 'visit_verify');
        $transitions = $workflow->getEnabledTransitions($lordInfo);
        $operation = [];
        foreach ($transitions as $transition) {
            $operation[] = $transition->getName();
        }
        $lordInfo->visit_operation = $operation;


        return $this->response(new ContractLordResource($lordInfo));
    }

    public function destroy($id) {
        try{

            $lord = ContractLord::find($id);
            if (!$lord) {
                return $this->responseNotFound();
            }

            $data = $lord->delete();
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function op_doc(Request $request, $id) {

        $lord = ContractLord::find($id);
        if (!$lord) {
            return $this->responseNotFound();
        }

        $workflow = Workflow::get($lord, 'contract_doc_verify');
        if ($workflow->can($lord, $request->input('operation'))) {
            $res = $workflow->apply($lord, $request->input('operation'));
            $lord->save();
            return $this->response($res);
        } else {
            return $this->setMessage("无法跨流程节点操作")->responseTransaction();
        }
    }

    public function op_visit(Request $request, $id) {

        $lord = ContractLord::find($id);
        if (!$lord) {
            return $this->responseNotFound();
        }
        $workflow = Workflow::get($lord, 'visit_verify');

        if ($workflow->can($lord, $request->input('operation'))) {
            $res = $workflow->apply($lord, $request->input('operation'));
            $lord->save();
            return $this->response($res);
        } else {
            return $this->setMessage("无法跨流程节点操作")->responseTransaction();
        }
    }

    public function update(Request $request, $id) {

        try {

            $lord = ContractLord::find($id);
            if (!$lord) {
                return $this->responseNotFound();
            }

            //1炸单， 2正常退租，3违约退租  4协商退租， 5转租， 6调租
            if ($request->input('finish_at')) {
                $end_real_at = $request->input('finish_at', Carbon::now());
                $end_type = $request->input('finish_type');
                $data = $lord->update([
                    'end_real_at'   =>  $end_real_at,
                    'end_type'  => $end_type,
                ]);

                return $this->response($lord);
            }

            $data = $lord->update($request->all());

            if ($data) {
                event(new Boss2Event('contract_lord', $lord));
            }

            if ($request->input('customer_id')) {
                $customerIds = explode(',', $request->input('customer_id'));
                $lord->customers()->sync($customerIds);
            }

            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function batch(Request $request) {
        try{
            putenv('x-batch=1');
            $data = [];

            if (!($batch = $request->batch) || !($batch = json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }
        }

        putenv('x-batch=0');
        return $this->response($data);
    }

}
