<?php
namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Resources\FilesResource;
use App\Http\Resources\FilesResourceCollection;
use App\Http\Resources\HousesResource;
use App\Jobs\SyncBoss2Images;
use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Customers;
use App\Models\Files;
use App\Models\Houses;
use Guzzle\Http\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\File;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class SyncController extends ApiController {


    //同步房屋数据
    public function syncHouse(Request $request) {

        $boss2House = $request->all();
        logger("house_sync", $boss2House);

        $house = new Houses();

        $house->name = array_get($boss2House, 'name', null);
        $house->building = array_get($boss2House, 'building', null);
        $house->unit = array_get($boss2House, 'unit', null);
        $house->house_number = array_get($boss2House, 'house_number', null);

        $house->room = array_get($boss2House, 'room', null);
        $house->hall = array_get($boss2House, 'hall', null);
        $house->toilet = array_get($boss2House, 'toilet', null);

        $house->area = array_get($boss2House, 'area', null);
        $house->floor = array_get($boss2House, 'floor', null);
        $house->floors = array_get($boss2House, 'floors', null);

        $house->village_name = array_get($boss2House, 'village_name', null);
        $house->village_id = array_get($boss2House, 'village_id', null);
        $house->city_name = array_get($boss2House, 'city_name', null);

        $house->decoration = array_get($boss2House, 'decoration', null);
        $house->house_identity = array_get($boss2House, 'house_identity', null);

        $house->user_id = array_get($boss2House, 'user_id', null);
        $house->org_id = array_get($boss2House, 'org_id', null);

        $house->house_res = array_get($boss2House, 'house_res', null);

        $house->py  =   array_get($boss2House, 'py', null);

        $house->pinyin  =   array_get($boss2House, 'pinyin', null);

        $house->boss2_house_id  =   array_get($boss2House, 'boss2_house_id', null);

        if (!$house->name) {
            logger("house_sync_failed_name", $boss2House);
            return $this->setMessage("name不能为空")
                ->setStatus('error')
                ->setStatusCode(50000)
                ->response();
        }

        if (!$house->user_id) {
            logger("house_sync_failed_userid", $boss2House);
            return $this->setMessage("{$house->name}:user_id不能为空")
                ->setStatus('error')
                ->setStatusCode(50001)
                ->response();
        }

        if (!$house->org_id) {
            logger("house_sync_failed_orgid", $boss2House);
            return $this->setMessage("{$house->name}:org_id")
                ->setStatus('error')
                ->setStatusCode(50002)
                ->response();
        }

        if (!$house->house_number) {
            logger("house_sync_failed_number", $boss2House);
            return $this->setMessage("{$house->name}:house_number不能为空")
                ->setStatus('error')
                ->setStatusCode(50003)
                ->response();
        }

        if (!$house->boss2_house_id) {
            logger("house_sync_failed_boss2id", $boss2House);
            return $this->setMessage("{$house->name}:boss2_house_id不能为空")
                ->setStatus('error')
                ->setStatusCode(50004)
                ->response();
        }

        if (Houses::where('name', $house->name)->first()) {
            logger("house_sync_failed_repeat", $boss2House);
            return $this->setMessage("{$house->name}:name重复")
                ->setStatus('error')
                ->setStatusCode(50005)
                ->response();
        }

        $res = $house->save();
        if ($res) {
            $house_album = array_get($boss2House, 'album', null);
            if ($house_album) {
                SyncBoss2Images::dispatch($house, $house_album)->delay(Carbon::tomorrow());
            }
            return $this->response([
                $house->boss2_house_id    =>  $house->id,
                'name'  =>  $house->name
            ]);
        }
    }


    //同步合同数据
    public function syncContract(Request $request) {

        //TODO: 待处理项兼容，end_real_at,end_type
        ////1炸单， 2正常退租，3违约退租  4协商退租， 5转租， 6调租, 7boss2中转为待处理项，但是未结算的状态

        $boss2Contract = $request->all();
        logger("contract_sync", $boss2Contract);
        $house_id = array_get($boss2Contract, 'house_id', null);
        if (!$house_id) {
            logger("lord_sync_failed_houseid", $boss2Contract);
            return $this->setMessage("house_id不能为空")
                ->setStatus('error')
                ->setStatusCode(60000)
                ->response();

        }
        $lords = array_get($boss2Contract, 'lords', null);
        $collectLord = [];
        if ($lords) {

            foreach ($lords as $lord) {
                $cl = new ContractLord();

                $cl->house_id = $house_id;

                $cl->contract_number = array_get($lord, 'contract_number', null);
                $cl->is_joint = array_get($lord, 'is_joint', null);
                $cl->is_agency = array_get($lord, 'is_agency', null);
                $cl->agency_info = array_get($lord, 'agency_info', null);
                $cl->is_corp = array_get($lord, 'is_corp', null);

                //1:新收， 2:续收
                $cl->type = array_get($lord, 'type', null);

                $cl->first_pay_at = array_get($lord, 'first_pay_at', null);
                $cl->second_pay_at = array_get($lord, 'second_pay_at', null);

                $cl->sign_month = array_get($lord, 'sign_month', null);
                $cl->sign_remainder_day = array_get($lord, 'sign_remainder_day', null);

                $cl->sign_at = array_get($lord, 'sign_at', null);
                $cl->start_at = array_get($lord, 'start_at', null);
                $cl->end_at = array_get($lord, 'end_at', null);

                $cl->mortgage_price = array_get($lord, 'mortgage_price', null);
                $cl->penalty_price = array_get($lord, 'penalty_price', null);

                $cl->month_price = array_get($lord, 'month_price', null);
                $cl->pay_way = array_get($lord, 'pay_way', null);

                $cl->purchase_way = array_get($lord, 'purchase_way', null);

                //此处为收款账户信息
                $cl->pay_account_info = array_get($lord, 'pay_account_info', null);

                $cl->ready_days = array_get($lord, 'ready_days', null);
                $cl->duration_days = array_get($lord, 'duration_days', null);
                $cl->property_payer = array_get($lord, 'property_payer', null);
                $cl->property_price = array_get($lord, 'property_price', null);
                $cl->guarantee_days = array_get($lord, 'guarantee_days', null);
                $cl->vacancy_way = array_get($lord, 'vacancy_way', null);
                $cl->vacancy_other = array_get($lord, 'vacancy_other', null);
                $cl->vacancy_end_date = array_get($lord, 'vacancy_end_date', null);

                $cl->remark = array_get($lord, 'remark', null);
                $cl->city_name = array_get($lord, 'city_name', null);

                $cl->user_id = array_get($lord, 'user_id', null);
                $cl->sign_user_id = array_get($lord, 'sign_user_id', null);

                $cl->org_id = array_get($lord, 'org_id', null);
                $cl->sign_org_id = array_get($lord, 'sign_org_id', null);

                $cl->end_real_at = array_get($lord, 'pending_at', null);
                $cl->end_type = array_get($lord, 'pending_type', null);

                $cl->doc_status = array_get($lord, 'doc_status', null);
                $cl->visit_status = array_get($lord, 'visit_status', null);

                $cl->created_at = array_get($lord, 'sign_at', null);
                $cl->updated_at = array_get($lord, 'sign_at', null);

                $customers = array_get($lord, 'customers', null);
                $cl->boss2_contract_id = array_get($lord, 'boss2_contract_id', null);

                if ($cl->type === null) {
                    logger("lord_sync_failed_type", $boss2Contract);
                    return $this->setMessage("type不能为空")
                        ->setStatus('error')
                        ->setStatusCode(60001)
                        ->response();
                }
                if (!$cl->first_pay_at) {

                    logger("lord_sync_failed_firstpayat", $boss2Contract);
                    return $this->setMessage("first_pay_at不能为空")
                        ->setStatus('error')
                        ->setStatusCode(60002)
                        ->response();
                }
                if (!$cl->second_pay_at) {
                    logger("lord_sync_failed_secondpayat", $boss2Contract);
                    return $this->setMessage("second_pay_at不能为空")
                        ->setStatus('error')
                        ->setStatusCode(60003)
                        ->response();
                }
                if (!$cl->sign_at) {
                    logger("lord_sync_failed_signat", $boss2Contract);
                    return $this->setMessage("sign_at不能为空")
                        ->setStatus('error')
                        ->setStatusCode(60004)
                        ->response();
                }
                if (!$cl->start_at) {
                    logger("lord_sync_failed_startat", $boss2Contract);
                    return $this->setMessage("start_at不能为空")
                        ->setStatus('error')
                        ->setStatusCode(60005)
                        ->response();
                }
                if (!$cl->end_at) {
                    logger("lord_sync_failed_endat", $boss2Contract);
                    return $this->setMessage("end_at不能为空")
                        ->setStatus('error')
                        ->setStatusCode(60006)
                        ->response();
                }
                if (!$customers) {
                    logger("lord_sync_failed_customer", $boss2Contract);
                    return $this->setMessage("customers不能为空")
                        ->setStatus('error')
                        ->setStatusCode(60007)
                        ->response();
                }

                if (!$cl->boss2_contract_id) {
                    logger("lord_sync_failed_boss2id", $boss2Contract);
                    return $this->setMessage("boss2合同id不能为空")
                        ->setStatus('error')
                        ->setStatusCode(60008)
                        ->response();
                }

                if ($cl->save()) {
                    //TODO: 存储客户信息
                    $customer_ids = [];
                    foreach ($customers as $customer) {
                        $customer_ids[] = $this->setCustomer($customer);
                    }
                    $cl->customers()->sync($customer_ids);
                    $collectLord[$cl->boss2_contract_id] = $cl->id;

                    $lord_album = array_get($lord, 'album', null);
                    if ($lord_album) {
                        SyncBoss2Images::dispatch($cl, $lord_album)->delay(Carbon::tomorrow());
                    }
                }
            }
        }

        $collectRent = [];
        $renters = array_get($boss2Contract, 'renters', null);
        if ($renters) {

            foreach ($renters as $renter) {
                $cr = new ContractRenter();

                $cr->contract_number = array_get($renter, 'contract_number', null);
                $cr->rentable_id = array_get($renter, 'house_id', null);
                $cr->rentable_type = Houses::class;

                //1租房 2转租 3续租 4未收先租 5调租
                $cr->type = array_get($renter, 'type', null);

                $cr->sign_month = array_get($renter, 'sign_month', null);
                $cr->sign_remainder_day = array_get($renter, 'sign_remainder_day', null);
                $cr->sign_at = array_get($renter, 'sign_at', null);
                $cr->start_at = array_get($renter, 'start_at', null);
                $cr->end_at = array_get($renter, 'end_at', null);
                $cr->duration_days = array_get($renter, 'duration_days', null);
                $cr->month_price = array_get($renter, 'month_price', null);
                $cr->mortgage_price = array_get($renter, 'mortgage_price', null);
                $cr->purchase_way = array_get($renter, 'purchase_way', null);
                $cr->pay_way = array_get($renter, 'pay_way', null);
                $cr->pay_bet = array_get($renter, 'pay_bet', null);
                $cr->pay_account_info = array_get($renter, 'pay_account_info', null);

                $cr->is_joint = array_get($renter, 'is_joint', null);
                $cr->is_agency = array_get($renter, 'is_agency', null);
                $cr->agency_info = array_get($renter, 'agency_info', null);

                $cr->money_sum = array_get($renter, 'money_sum', null);
                $cr->money_table = array_get($renter, 'money_table', null);

                $cr->property_payer = array_get($renter, 'property_payer', null);
                $cr->property_price = array_get($renter, 'property_price', null);
                $cr->final_payment_at = array_get($renter, 'final_payment_at', null);
                $cr->receipt_number = array_get($renter, 'receipt_number', null);
                $cr->city_name = array_get($renter, 'city_name', null);
                $cr->remark = array_get($renter, 'remark', null);

                $cr->user_id = array_get($renter, 'user_id', null);
                $cr->sign_user_id = array_get($renter, 'sign_user_id', null);
                $cr->sign_org_id = array_get($renter, 'sign_org_id', null);
                $cr->org_id = array_get($renter, 'org_id', null);

                $cr->end_real_at = array_get($renter, 'pending_at', null);
                $cr->end_type = array_get($renter, 'pending_type', null);

                $cr->boss2_contract_id = array_get($renter, 'boss2_contract_id', null);

                $cr->created_at = array_get($renter, 'sign_at', null);
                $cr->updated_at = array_get($renter, 'sign_at', null);

                $cr->doc_status = array_get($renter, 'doc_status', null);
                $cr->visit_status = array_get($renter, 'visit_status', null);

                $customers = array_get($renter, 'customers', null);

                if ($cr->save()) {
                    //TODO: 存储客户信息
                    $customer_ids = [];
                    foreach ($customers as $customer) {
                        $customer_ids[] = $this->setCustomer($customer);
                    }
                    $cr->customers()->sync($customer_ids);
                    $collectRent[$cr->boss2_contract_id] = $cr->id;
                    $rent_album = array_get($renter, 'album', null);
                    if ($rent_album) {
                        SyncBoss2Images::dispatch($cr, $rent_album)->delay(Carbon::tomorrow());
                    }
                }
            }
        }

        return $this->response(['lord' => $collectLord, 'rent' => $collectRent]);
    }

    public function setCustomer($cust) {
        $customer = new Customers();
        $customer->name = array_get($cust, 'name', null);
        $customer->phone = array_get($cust, 'phone', null);
        $customer->is_corp = array_get($cust, 'is_corp', null);
        $customer->sex = array_get($cust, 'sex', null);
        $customer->idtype = array_get($cust, 'idtype', null);
        $customer->idcard = array_get($cust, 'idcard', null);
        $customer->user_id = array_get($cust, 'user_id', null);
        $customer->org_id = array_get($cust, 'org_id', null);
        $customer->py = array_get($cust, 'py', null);
        $customer->pinyin = array_get($cust, 'pinyin', null);

        if (($c = Customers::where([
            ['name', '=', array_get($customer, 'name')],
            ['phone', '=', array_get($customer, 'phone')]])->first())) {

            return $c->id;
        }

        if ($customer->save()) {
            return $customer->id;
        }

    }

}