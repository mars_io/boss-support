<?php

namespace App\Http\Controllers\Api;

use App\Events\Boss2Event;
use App\Exceptions\AbortException;
use App\Http\Requests\SystemsCreateRequest;
use App\Http\Requests\SystemsUpdateRequest;
use App\Http\Resources\HousesResource;
use App\Http\Resources\HousesResourceCollection;
use App\Http\Resources\SystemsResource;
use App\Http\Resources\SystemsResourceCollection;
use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Customers;
use App\Models\Houses;
use App\Models\Organizations;
use App\Models\Systems;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HousesController extends ApiController
{

    public function index(Request $request) {
        try{

            $houseCollect = Houses::search($request->input('q'))
                ->where(function ($query) use ($request){
                    if (($status = $request->input('status')) !== null) {
                        if ((int)$status === 0) {
                            //未出租
                            $query->whereNull('rent_start_at');
                            $query->whereNotNull('lord_start_at');
                        } else {
                            //已出租
                            $query->whereNotNull('rent_start_at');
                        }
                    }

                    if (($is_nrcy = $request->input('is_nrcy')) !== null) {
                        if ((int)$is_nrcy === 0) {
                            $query->where('is_nrcy', 0);
                        } else {
                            $query->where('is_nrcy', 1);
                        }
                    }

                    if ($request->path() == 'api/s1/houses' && !str_contains($request->server->get('HTTP_REFERER'), 'mobile')) {
                        $user = $request->user('api');

                        if ($user->hasRole('market-marketing-officer')) {
                            $query->where('user_id', $user->id);
                        }

                        if ($user->hasRole(['market-reserve-manager', 'market-marketing-manager'])) {
                            $org_ids = $user->orgs->pluck('id')->toArray();
                            $query->whereIn('org_id', $org_ids)->orWhere('user_id', $user->id);
                        }

                        if ($user->hasRole(['market-reserve-regional', 'market-marketing-regional', 'market-marketing-city'])) {
                            $org_ids = $user->orgs->pluck('id')->toArray();
                            $org_all_ids = [];
                            foreach ($org_ids as $org_id) {
                                $org = Organizations::find($org_id);
                                if ($org){
                                    $org_all_ids[] = $org->getDescendantsAndSelf()->pluck('id')->toArray();
                                }
                            }
                            $org_all_ids = array_unique(array_flatten($org_all_ids));

                            $query->whereIn('org_id', $org_all_ids)->orWhere('user_id', $user->id);
                        }

                    }

                    if ($request->input('org_id') && (int)$request->input('org_id') !== 1) {
                        $org = Organizations::find($request->input('org_id'));
                        if ($org){
                            $org_ids = array_unique($org->getDescendantsAndSelf()->pluck('id')->toArray());
                            $query->whereIn('org_id', $org_ids);
                        }
                    }
                })
                ->with('rooms')
                ->with(['contract_renters'=>function ($query) {
                    $query->orderBy('id', 'desc');
                }])
                ->with(['contract_lords'=>function ($query) {
                    $query->orderBy('id', 'desc');
                }])
                ->with('contract_lords.user')
                ->with('contract_lords.sign_user')
                ->with('contract_lords.sign_org')
                ->with('contract_lords.customers')
                ->with('contract_renters.user')
                ->with('contract_renters.sign_user')
                ->with('contract_renters.sign_org')
                ->with('contract_renters.customers')
                ->with('user')
                ->with('user.orgs')
                ->with('org')
                ->orderBy('id', 'desc')
                ->distinct()
                ->paginate($request->input('per_page_number'));

            return new HousesResourceCollection($houseCollect);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(Request $request) {

        try {
            $data = Houses::create($request->all());
            return $this->response($data);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

        if (is_numeric($id)){
            $house = Houses::where('id', $id)->first();
        }else{
            $house = Houses::where('name', $id)->first();
        }

        //$house = Houses::where('name', $id)->orWhere('id', $id)->first();
        if (!$house) {
            return $this->responseNotFound();
        }

        $houseInfo = Houses::where('id', $id)
            ->orWhere('name', $id)
            ->with('rooms')
            ->with(['contract_renters' => function($query){
                $query->orderBy('id', 'desc');
            }])
            ->with(['contract_lords' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->with('contract_lords.user')
            ->with('contract_lords.sign_user')
            ->with('contract_lords.sign_org')
            ->with('contract_lords.customers')
            ->with('contract_renters.user')
            ->with('contract_renters.sign_user')
            ->with('contract_renters.sign_org')
            ->with('contract_renters.customers')
            ->with('user')
            ->with('user.orgs')
            ->with('org')
            ->first();

        return $this->response(new HousesResource($houseInfo));
    }

    public function destroy($id) {
        try{
            return $this->responseNotCaptured();
            $house = Houses::find($id);
            if (!$house) {
                return $this->responseNotFound();
            }
            $house = $house->delete();
            return $this->response($house);
        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function update(Request $request, $id) {

        try {
            $house = Houses::find($id);
            if (!$house) {
                return $this->responseNotFound();
            }

            $request->offsetSet('warning_init_at', $request->input('warning_status'));

            $data = $house->update($request->all());

            if ($data) {

                if ($request->input('org_id')) {
                    //房管中心分配了房屋，同时分配合同
                    ContractRenter::where('rentable_id', $house->id)->update(['org_id' => $request->input('org_id')]);
                    ContractLord::where('house_id', $house->id)->update(['org_id' => $request->input('org_id')]);
                }

                if ($request->input('user_id')) {

                    //房管中心分配了房屋，同时分配合同
                    ContractRenter::where('rentable_id', $house->id)->update(['user_id' => $request->input('user_id')]);
                    ContractLord::where('house_id', $house->id)->update(['user_id' => $request->input('user_id')]);
                }

                //同步到Boss2的财务模块
                event(new Boss2Event('house', $house));
            }

            return $this->response($data);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function batch(Request $request) {

        try{
            putenv('x-batch=1');
            $data = [];
            if (!($batch = $request->batch) || !($batch = json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
        if (isset($batch['store']) && count($batch['store'])) {

        }

        if (isset($batch['update']) && count($batch['update'])) {
            foreach ($batch['update'] as $key=>$val) {
                if (isset($val['org_id'])) {
                    $request->offsetSet('org_id', $val['org_id']);
                }
                if (isset($val['user_id'])) {
                    $request->offsetSet('user_id', $val['user_id']);
                }
                $data[] = $this->update($request, $key);
            }
        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }
        }

        if (isset($batch['destroy']) && count($batch['destroy'])) {
            foreach ($batch['destroy'] as $val) {
                $data[] = $this->destroy($val);
            }

        }

        putenv('x-batch=0');
        return $this->response($data);

    }

}
