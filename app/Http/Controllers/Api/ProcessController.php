<?php

namespace App\Http\Controllers\Api;

use App\Events\CommentEvent;
use App\Exceptions\AbortException;
use App\Http\Resources\CommentsResource;
use App\Http\Resources\CommentsResourceCollection;
use App\Http\Resources\ProcessResource;
use App\Http\Resources\ProcessResourceCollection;
use App\Http\Resources\TaskResourceCollection;
use App\Models\Organizations;
use App\Models\Process;
use App\Models\Reports;
use App\Models\Role;
use App\Models\Tasks;
use App\Models\Users;
use App\Notifications\CommentNotice;
use App\Notifications\HouseGoodNews;
use App\Notifications\UserNotice;
use Brexis\LaravelWorkflow\Facades\WorkflowFacade as Workflow;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Spatie\Activitylog\Models\Activity;


class ProcessController extends ApiController
{

    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //获取所有的报备信息
        if ($request->input('all')) {
            $session_user = Auth::guard('api')->user();
            if ($session_user && !$session_user->can('flow-all-select')) {
                return $this->setMessage("您没有权限执行此操作")->responseTransaction();
            }
            $processList = Process::orderBy('id', 'desc')
                ->paginate($request->input('per_page_number'));
            return new ProcessResourceCollection($processList);
        }

        //上传日期
        if ($request->input('search')) {

            if ($request->input('processable_type') == false) {
                //return $this->setMessage("processable_type不能为空")->responseTransaction();
            }
            $session_user = Auth::guard('api')->user();

            if ($session_user && !$session_user->can('flow-all-select')) {
                return $this->setMessage("您没有权限执行此操作")->responseTransaction();
            }

            $processes = Process::search($request->input('q'))
                ->where(function ($query) use ($request){

                if ($request->input('start_time')) {
                    $query->whereBetween('created_at',
                        [
                            $request->input('start_time'),
                            $request->input('end_time', Carbon::now())
                        ]);
                }

                if (($place = $request->input('place'))) {
                    $places = explode(',', $place);
                    if (count($places) > 1) {
                        foreach ($places as $p) {
                            $query->orWhere("place->$p", 1);
                        }
                    }else {

                        $query->where("place->$places[0]", 1);
                    }
                }

                if ($request->input('processable_type')) {

                    $query->where('processable_type', $request->input('processable_type'));
                }

                if ($request->input('org_id')) {
                    $org = Organizations::find($request->input('org_id'));
                    if ($org) {
                        $org_ids = $org->getDescendantsAndSelf()
                            ->pluck('id')
                            ->toArray();
                        $query->whereIn('org_id', $org_ids);
                    }
                }
            })->orderBy('id', 'desc');
            if ($request->input('is_page')) {
                return new ProcessResourceCollection($processes->paginate($request->input('per_page_number')));
            } else {
                return new ProcessResourceCollection($processes->get());
            }
        }


        $house_id = $request->input('house_id');
        if ($house_id) {
            $processList = Process::where('house_id', $house_id)
                ->with('house')
                ->with('user')
                ->with('user.orgs')
                ->with('user.roles')
                ->orderBy('id', 'desc')
                ->paginate($request->input('per_page_number'));
            return new ProcessResourceCollection($processList);
        }

        //待办, 已办, 我发起的, 抄送我的
        //我审批的
        $type = (int)$request->input('type', 1);
        $currentUserId = Auth::guard('api')->id();
        if ($type === 1) {
            $taskList = Tasks::search($request->input('q'))->where('user_id', $currentUserId)
                ->whereNotNull('finish_at')
                ->where('is_cc', 0)
                ->with('user')
                ->with('user.orgs')
                ->with('user.roles')
                ->with('process.user')
                ->with('process.user.orgs')
                ->with('process.user.roles')
                ->orderBy('id', 'desc')
                ->paginate($request->input('per_page_number'));
        }

        //我的审批（待办|已完成）
        if ($type === 2) {
            if ($request->input('count')) {
                $count = Tasks::where('user_id', $currentUserId)
                    ->where('is_cc', 0)
                    ->whereNull('finish_at')
                    ->count();
                    return $this->response($count);
            }
            $taskList = Tasks::search($request->input('q'))->where('user_id', $currentUserId)
                ->where('is_cc', 0)
                ->where(function ($query) use ($request){
                    if ($request->input('finish_at')) {
                        //已完成
                        $query->whereNotNull('finish_at');
                    } else {
                        //待办
                        $query->whereNull('finish_at');
                    }
                })
                ->with('user')
                ->with('user.orgs')
                ->with('user.roles')
                ->with('process.user')
                ->with('process.user.orgs')
                ->with('process.user.roles')
                ->orderBy('id', 'desc')
                ->paginate($request->input('per_page_number'));
        }

        //我发起的（流程完成|流程未完成）
        if ($type === 3) {
            $processList = Process::search($request->input('q'))->where('user_id', $currentUserId)
                ->where(function ($query) use ($request){
                    if ($request->input('published')) {
                        $query->whereNotNull('finish_at');
                    } else {
                        $query->whereNull('finish_at');
                    }
                })
                ->with('user')
                ->with('user.orgs')
                ->with('user.roles')
                ->orderBy('id', 'desc')
                ->paginate($request->input('per_page_number'));

            return new ProcessResourceCollection($processList);
        }

        //抄送我的（已读|未读）
        if ($type === 4) {
            $taskList = Tasks::search($request->input('q'))->where([
                ['user_id', '=', $currentUserId],
                ['is_cc', '=', 1]
            ])
                ->where(function ($query) use ($request){
                    if ($request->input('read_at')) {
                        $query->whereNotNull('read_at');
                    } else {
                        $query->whereNull('read_at');
                    }
                })
                ->with('user')
                ->with('user.orgs')
                ->with('user.roles')
                ->with('process.user')
                ->with('process.user.orgs')
                ->with('process.user.roles')
                ->orderBy('id', 'desc')
                ->orderBy('read_at', 'asc')
                ->paginate($request->input('per_page_number'));
        }
        return new TaskResourceCollection($taskList);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info("ProcessRequest: " . json_encode($request->all()));
        try {
            $content = json_decode($request->input('content'), true);
            if (!is_array($content)) {
                return $this->setMessage("报备内容序列化失败")->responseTransaction();
            }
            $user_id = $request->input('user_id');

            if (!$user_id) {
                return $this->setMessage("报备人id不能为空")->responseTransaction();
            }

            $user = Users::find($user_id);
            if ($user && $user->orgs->isNotEmpty()) {
                $org_id = Users::find($user_id)->orgs[0]->id;
            } else {
                $org_id = NULL;
            }

            $process = Process::create([
                'user_id' => $user_id,
                'org_id'    => $org_id,
                'content' => $content,
                'processable_id'    => $request->input('processable_id'),
                'processable_type'    => $request->input('processable_type'),
                'house_id'  =>  $request->input('house_id')
            ]);

            $workflow = Workflow::get($process, $this->process_map($process->processable_type));
            $workflow->apply($process, $workflow->getEnabledTransitions($process)[0]->getName());

            $process->save();

            Log::info("ProcessResponse: " . json_encode($process));

            return $this->response($process);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $process = Process::where('id', $id)
                ->with('user')
                ->with('user.orgs')
                ->with('user.roles')
                ->first();

            if (!$process) {
                return $this->responseNotFound();
            }
            $currentId = Auth::guard('api')->id();

            $workflow = Workflow::get($process, $this->process_map($process->processable_type));
            $transitions = $workflow->getEnabledTransitions($process);

            $operation = [];

            foreach ($transitions as $transition) {
                $name = $transition->getName();
                $operation_arr = explode('_', $name);

                $hasRole = false;
                if (count($operation_arr) === 3) {
                    $hasRole = Users::find($currentId)->hasRole($operation_arr[1]);
                    if (!$hasRole && ($operation_arr[1] == 'market-marketing-manager')) {
                        $hasRole = Users::find($currentId)->hasRole('market-reserve-manager');
                    }
                }

                if (str_contains($name,'approved')) {
                    if ($hasRole) {
                        $operation[$name] = '同意';
                    }
                }
                if (str_contains($name, 'rejected')) {
                    if ($hasRole) {
                        $operation[$name] = '拒绝';
                    }
                }
                if (str_contains($name, 'cancelled')) {
                    if ($process->user_id == $currentId) {
                        $operation[$name] = '撤销';
                    }
                }
            }

            //如果该流程存在我的待办任务，我在浏览该页面时，更新我的已读时间
            $task = Tasks::where([['flow_id', '=', $process->id],['user_id', '=', $currentId]])->first();

            if ($task) {
                $task->update(['read_at' => Carbon::now()]);
            }
            $operation["to_comment"] = '评论';

            if ($process->finish_at) {
                $t = (strtotime($process->finish_at) - strtotime($process->created_at)) / 60;
                $t = ceil($t);
                $deal = "共用{$t}分钟";
            } else {
                $t = (time() - strtotime($process->created_at)) / 60;
                $t = ceil($t);
                $deal = "耗时{$t}分钟";
            }

            return $this->response(['operation' => $operation, 'deal' => $deal,  'process'=>new ProcessResource($process)]);
        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }


    /**
     * 获取抄送人
     *
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function cc(Request $request) {
        $flow = $this->getWorkflow('xibao.transitions');
        $cc_user = [];
        foreach ($flow as $k => $v) {
            if ($v['to'] == 'published') {
                $cc_user = $v['node']['task'];
            }
        }

        $users = Users::whereIn('id', $cc_user)->get();
        return $this->response($users);
    }

    //get => show, post => operation

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function op(Request $request) {
        //我发起的： 催办，撤销，评论
        //待我审批： 同意，拒绝，转交，评论
        //我发起（审批人里包含自己）： 同意，拒绝，撤销，转交，评论

        // process.user_id = Guard()->id(), process.place != published
        // process.id = task.flow_id, task.user_id = Guard()->id(), finish_at = NULL, process.place != task.flow_place
        // process.user_id = Guard()->id(), process.place = task.flow_place, process.place != published

        $process = Process::find($request->input('id'));
        $workflow = Workflow::get($process, $this->process_map($process->processable_type));
        if ($workflow->can($process, $request->input('operation'))) {
            $res = $workflow->apply($process, $request->input('operation'));
            $process->save();
        }else {
            return $this->setMessage("无法进行跨流程节点操作")->responseTransaction();
        }
        return $this->response($res);
    }

    /**
     * 获取当前表单填写页所属的工作流
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function flow(Request $request) {

       $flow = $this->getWorkflow('integral_ask');
       $places = $roles = [];

       foreach ($flow['places'] as $place) {
           if (str_contains($place, 'review')) {
               $role = explode('_', $place)[0];
               $user = Users::whereRoleIs($role)->get();

               $places[$place]['users'] = $user;
               $places[$place]['role'] = Role::where('name', $role)->first();
           }
       }
        return $this->response($places);

    }

    public function getWorkflow($flow_name) {
        return config("workflow." . $flow_name);
    }


    public function compact_house(Request $request) {

        $data = Process::where([
            [
                'house_id', '=', $request->input('from_house_id')
            ]
        ])->update([
            'house_id' => $request->input('to_house_id'),
        ]);
        return $this->response($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $process = Process::find($id);
        $currentUserId = Auth::guard('api')->id();
        $processUser = Users::find($process->user_id);
        $user = Users::find($currentUserId);

        if ($request->input('operation') == "to_comment") {
            if ($request->input('comment') || $request->input('album')) {
                if (!$request->input('comment') && !$request->input('album')) {
                    return $this->setMessage("评论内容不能为空")->responseTransaction();
                }

                $comment = $process->comment([
                    'body' => $request->input('comment'),
                ], $user);

                if ($request->input('album')) {
                    $comment->album = json_encode($request->input('album'));
                    $comment->save();
                }
                if ($comment) {
                    event(new CommentEvent($user, $processUser, $comment, $process->id));
                }
                return $this->response($comment);
            } else {
                return $this->response($process);
            }
        }

        if ($request->input('operation')) {

            $workflow = Workflow::get($process, $this->process_map($process->processable_type));
            if ($workflow->can($process, $request->input('operation'))) {
                $res = $workflow->apply($process, $request->input('operation'));
                $process->save();

                //更新我的任务
                $task = Tasks::where([
                    ['flow_id', '=', $process->id],
                    ['user_id', '=', $currentUserId]
                ])->first();
                if ($task) {
                    $task->update([
                        'finish_at' => Carbon::now(),
                        'finish_type'   =>  $request->input('operation')
                    ]);
                }

                if (str_contains($process->getPlace()['status'], ['published', 'cancelled', 'rejected'])) {
                    Tasks::where('flow_id', '=', $process->id)
                        ->whereNull('finish_at')
                        ->update([
                            'finish_at' => Carbon::now(),
                            'finish_type'   =>  $process->getPlace()['status']
                        ]);
                }

                if ($request->input('comment') || $request->input('album')) {

                    $comment = $process->comment([
                        'body' =>$request->input('comment'),
                    ], $user);

                    if ($request->input('album')) {
                        $comment->album = json_encode($request->input('album'));
                        $comment->save();
                    }

                    if ($comment) {
                        event(new CommentEvent($user, $processUser, $comment, $process->id));
                    }
                }
                return $this->response($res);
            }else {
                return $this->setMessage("无法进行跨流程节点操作")->responseTransaction();
            }
        }

        $data = $process->update($request->all());
        return $this->response($data);
    }


    public function process_map($processable_type) {

        $map = [
            'bulletin_collect_basic' => 'shoufangbaobei',//普通收房
            'bulletin_collect_continued' => 'xushoubaobei',//续收
            'bulletin_rent_basic' => 'zufangbaobei',//普通租房
            'bulletin_rent_trans' => 'zhuanzubaobei',//转租
            'bulletin_rent_continued' => 'xuzubaobei',//续租
            'bulletin_rent_RWC' => 'weishouxianzubaobei',//未收先租
            'bulletin_RWC_confirm' => 'weishouxianzuqueren',//未收先租确认
            'bulletin_agency' => 'zhongjiefeibaobei',//中介费
            'bulletin_banish' => 'qingtuibaobei',//清退
            'bulletin_change' => 'tiaozubaobei',//调房
            'bulletin_confiscate' => 'chonggongbaobei',//充公
            'bulletin_lose' => 'zhadanbaobei',//炸单
            'bulletin_refund' => 'tuikuanbaobei',//退款
            'bulletin_checkout' => 'tuizubaobei',//退租
            'bulletin_retainage' => 'weikuanfangzubaobei',//尾款房租
            'bulletin_special' => 'teshushixiangbaobei',//特殊
            'bulletin_quality'  =>  'fangwuzhiliangbaobei'
        ];
        return $map[$processable_type];
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{


        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function batch() {

    }
}
