<?php
namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Resources\FilesResource;
use App\Http\Resources\FilesResourceCollection;
use App\Models\Files;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class FilesController extends ApiController {

    public function index(Request $request) {

        try{
            $disk = Storage::disk('qiniu');
            $token = $disk->getUploadToken();
            return $this->response($token);

            //return new FilesResourceCollection(Files::paginate($request->input('per_page_number')));
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {
        $file = Files::find($id);
        if (!$file) {
            return $this->responseNotFound();
        }
        return $this->response(new FilesResource($file));
    }

    public function store(Request $request) {

        if (($file = $request->file('file'))) {
            $disk = Storage::disk('qiniu');
            $fileName = $disk->putFile('', $request->file('file'));
            $fileUrl = getenv('QINIU_DOMAIN') . $fileName;
            $rawName = $file->getClientOriginalName();
            $type = "";
            $size = $file->getClientSize();

        } else {
            $fileName = $request->input('name');
            if (getenv('APP_ENV') == 'production') {
                $fileUrl = str_replace("static.", "s.", $request->input('url'));
            }else {
                $fileUrl = $request->input('url');
            }

            $rawName = $request->input('raw_name', '');
            $type = $request->input('type', '');
            $size = $request->input('size', '');

            if (!$fileName || !$fileUrl) {
                return $this->setMessage("图片元信息必须上传")->responseTransaction();
            }
        }

        $info = [
            'bucket' => config('filesystems.disks.qiniu.bucket'),
            'host' => config('filesystems.disks.qiniu.domain'),
            'ext' => $type,
            'mime' => $type,
            'size' => $size
        ];

        $data = Files::create([
            'name' => $fileName,
            'raw_name' => $rawName,
            'display_name' => $rawName,
            'info' => $info,
            'user_id' => Auth::guard('api')->id(),//TODO: 修改为当前会话用户
            'uri' => $fileUrl
        ]);
        return $this->response($data);
    }

    public function update(Request $request, $id) {
        $file = Files::find($id);
        if (!$file) {
            return $this->responseNotFound();
        }
        $res = $file->update(['display_name' => $request->input('display_name')]);
        return $this->response($res);
    }

    public function destroy($id) {
        try{
            $file = Files::find($id);
            if (!$file) {
                return $this->responseNotFound();
            }
            $data = $file->delete();
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function batch(Request $request) {
        try{
            putenv('x-batch=1');
            $data = [];

            if (!($batch = $request->batch) || !($batch = json_decode($batch, true))) {

                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

        if (isset($batch['store']) && count($batch['store'])) {
            $req = new Request();

            foreach ($batch['store'] as $val) {
                foreach ($val as $k => $v) {
                    $req->offsetSet($k, $v);
                }
                $data[] = $this->store($req);
            }
        }

//        $b = ['update' => ['204' => ['display_name' => 'aaaa'], '205' => ['display_name' => 'bbbb'], '206' => ['display_name' => 'cccc']]];
//        dd(json_encode($b));
        //dd($batch['update']);
        if (isset($batch['update']) && count($batch['update'])) {
            foreach ($batch['update'] as $key=>$val) {
                $request->offsetSet('display_name', $val['display_name']);
                $data[] = $this->update($request, $key);
            }
        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {

                $data[] = $this->show($val);
            }
        }

        putenv('x-batch=0');
        return $this->response($data);
    }

}