<?php
namespace App\Http\Controllers\Api;

use App\Http\Resources\UsersResourceCollection;
use App\Models\Users;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class ApiController extends Controller {

    protected $statusCode = 200;

    protected $status = 'success';

    protected $message = '操作成功';

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatusCode($statusCode) {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function getStatusCode() {
        return $this->statusCode;
    }

    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    public function getMessage() {
        return $this->message;
    }

    public function responseNotFound() {
        return $this->setMessage('没有找到相关记录')->setStatusCode(404)->responseError();
    }

    public function responseForbidden() {
        return $this->setMessage('您没有权限执行该操作')->setStatusCode(403)->responseError();
    }

    public function responseUnauthorized() {
        return $this->setMessage('您尚未通过用户认证')->setStatusCode(401)->responseError();
    }

    public function responseTransaction() {
        return $this->setStatusCode(451)->responseError();
    }

    public function responseNotCaptured() {
        return $this->setMessage('网关上游出现异常')->setStatusCode(504)->responseError();
    }

    public function responseError() {
        return $this->setStatus('fail')->response();
    }

    public function response($data = []) {
        $resp = [
            'status' => $this->getStatus(),
            'status_code' => $this->getStatusCode(),
            'message' => $this->getMessage(),
            'data' => $data
        ];

        $this->setMessage('操作成功')->setStatusCode(200)->setStatus('success');
        return getenv('x-batch') ? $resp : Response::json($resp);
    }
}