<?php

namespace App\Http\Controllers\Api;

use App\Events\OrganizationDestroyEvent;
use App\Events\OrganizationIndexEvent;
use App\Events\OrganizationStoreEvent;
use App\Events\OrganizationUpdateEvent;
use App\Exceptions\AbortException;
use App\Http\Requests\OrganizationsShowRequest;
use App\Http\Requests\OrganizationsStoreRequest;
use App\Http\Requests\OrganizationsIndexRequest;
use App\Http\Requests\OrganizationsUpdateRequest;
use App\Http\Resources\OrganizationsResource;
use App\Http\Resources\OrganizationsResourceCollection;
use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Houses;
use App\Models\Organizations;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;


class OrganizationsController extends ApiController
{

    public function __construct()
    {

    }

    public function index(OrganizationsIndexRequest $request) {

        try{
            event(new OrganizationIndexEvent());

            if ($request->input('is_recur')) {
                if ($request->input('parent_id')) {
                    $org = Organizations::find($request->input('parent_id'));
                    if ($org){
                        return new OrganizationsResourceCollection($org->getDescendantsAndSelf());
                    }else {
                        return $this->responseNotFound();
                    }
                }
            }
            $orgCollect = Organizations::search($request->input('q'))
                ->where(function ($query) use ($request){
                    if ($request->input('parent_id')) {
                        $query->where('parent_id', $request->input('parent_id'));
                    }


                })
                ->with('leader')
                ->orderBy('order', 'desc')
                ->paginate($request->input('per_page_number'));

            return new OrganizationsResourceCollection($orgCollect);
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function isChild(Request $request) {

        $pid = $request->input('pid');
        $cid = $request->input('cid');

        $parentOrg = Organizations::find($pid);
        $childOrg = Organizations::find($cid);
        if (!$parentOrg || !$childOrg) {
            return $this->responseNotFound();
        }
        $res = $childOrg->isDescendantOf($parentOrg);
        return $this->response($res);
    }

    public function store(OrganizationsStoreRequest $request) {

        try {
            $result = event(new OrganizationStoreEvent($request));
            $result = isset($result[0]) ? $result[0] :$result;
            if ($result) {
                $parentNode = Organizations::find($request->input('parent_id'));
                $org = Organizations::create(
                    [
                        'name' => $request->input('name'),
                        'ding_department_id' => $result['id'],
                        'order' => $request->input('order'),
                        'py'    =>  pinyin_abbr($request->input('name')),
                        'pinyin'    =>  implode('', pinyin($request->input('name')))
                    ]
                );
                $data = $org->makeChildOf($parentNode);
                return $this->response($data);
            } else {
                return $this->setMessage("同步钉钉失败")->responseTransaction();
            }

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    //TODO: 解决批量接口无showMarket导致的异常
    public function show($id) {

        $org = Organizations::where('id', $id)
            ->with('leader')
            ->first();
        if (!$org) {
            return $this->responseNotFound();
        }
        $org->showMarket = true;


        return $this->response(new OrganizationsResource($org));
    }

    public function destroy($id) {

        try{
            //TODO: 删除部门之前 必须检查部门是否存在关联资源
            if (!($org = Organizations::find($id))) {
                return $this->responseNotFound();
            }

            if (!$org->getDescendants()->isEmpty()) {
                return $this->setMessage("{$org->name}下存在子部门，请先将其移出{$org->name}")->responseTransaction();
            }
            if (!$org->users()->get()->isEmpty()) {
                return $this->setMessage($org->name . "下存在员工，请先将其移出本部门")->responseTransaction();
            }

            $lordCount = ContractLord::where('org_id', $id)->count();
            if ($lordCount) {
                return $this->setMessage($org->name . "下存在收房合同，请先分配其合同")->responseTransaction();
            }

            $rentCount = ContractRenter::where('org_id', $id)->count();
            if ($rentCount) {
                return $this->setMessage($org->name . "下存在租房合同，请先分配其合同")->responseTransaction();
            }

            $houseCount = Houses::where('org_id', $id)->count();
            if ($houseCount) {
                return $this->setMessage($org->name . "下存在房屋，请先分配其房屋")->responseTransaction();
            }

            $result = event(new OrganizationDestroyEvent($id));

            if ($result) {
                $data = $org->delete();
                return $this->response($data);
            }else {
                return $this->setMessage("远程同步钉钉失败")->responseTransaction();
            }

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function update(OrganizationsUpdateRequest $request, $id) {

        try {
            $org = Organizations::find($id);

            if (!$org) {
                return $this->responseNotFound();
            }

            if ($request->input('name')) {
                $request->offsetSet('py', pinyin_abbr($request->input('name')));
                $request->offsetSet('pinyin', implode('', pinyin($request->input('name'))));
            }

            if ($request->input('parent_id')) {
                $result = event(new OrganizationUpdateEvent($request, $id));
                $result = isset($result[0]) ? $result[0] :$result;
                if ($result['errmsg'] != 'ok') {

                    return $this->responseNotCaptured();
                }
            }

            $data = Organizations::find($id)->update($request->all());
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     *
     */
    public function batch(Request $request) {
        try{
            putenv('x-batch=1');
            $data = [];
            if (!($batch = $request->batch) || !($batch = \GuzzleHttp\json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

        if (isset($batch['store']) && count($batch['store'])) {
            $req = new Request();
            foreach ($batch['store'] as $val) {
                foreach ($val as $k => $v) {
                    $req->offsetSet($k, $v);
                }
                $data[] = $this->store($req);
            }
        }

        if (isset($batch['update']) && count($batch['update'])) {

        }

        if (isset($batch['show']) && count($batch['show'])) {
            $batch['show'] = array_unique($batch['show']);
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }
        }

        if (isset($batch['destroy']) && count($batch['destroy'])) {
            foreach ($batch['destroy'] as $val) {
                $data[] = $this->destroy($val);
            }

        }

        putenv('x-batch=0');
        return $this->response($data);


        /*
        batch: {
            {
                "store": [{
                    "name": "Marvin",
                    "age": 18
                }, {
                    "name": "Alice",
                    "age": 22
                }],
                "update": {
                    "8": {
                        "name": "David"
                    },
                    "11": {
                        "name": "Dannis",
                        "age": 30
                    }
                },
                "show": [1, 3, 5, 7],
                "destroy": [2, 4, 6]
            }
        }
        */
//        $batch = [
//            'store' => [['name' => 'Marvin', 'age' => 18], ['name' => 'Alice', 'age' => 22]],
//            'update' => ['8' => ['name' => 'David'], '11' => ['name' => 'Dannis', 'age' => 30]],
//            'show' => [1, 3, 5, 7],
//            'destroy' => [2, 4, 6]
//        ];


    }

}
