<?php
namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Resources\MessageResourceCollection;
use App\Models\Customers;
use App\Models\Organizations;
use App\Models\Users;
use App\Notifications\AchieveRank;
use App\Notifications\ArticlePublish;
use App\Notifications\BeFullMember;
use App\Notifications\CommonTextNotice;
use App\Notifications\CompleteDocument;
use App\Notifications\ContractCheckOut;
use App\Notifications\CustomerUsefulNotification;
use App\Notifications\ExamGradePublish;
use App\Notifications\ExamInvite;
use App\Notifications\ExamSignApply;
use App\Notifications\ExamSignFailed;
use App\Notifications\ExamSignSuccess;
use App\Notifications\ExamStartRemind;
use App\Notifications\FinanceFundGenerate;
use App\Notifications\FinanceReimbursement;
use App\Notifications\LoseJob;
use App\Notifications\PaymentNotice;
use App\Notifications\QAGetAnswer;
use App\Notifications\QAGetQuestion;
use App\Notifications\QuestionnaireApply;
use App\Notifications\SystemToUpdate;
use App\Notifications\UserGetCode;
use App\Notifications\UserJoinCompany;
use App\Notifications\UserNotice;
use App\Notifications\WelcomeToCop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class MessagesController extends ApiController {

    public $ding;

    public function __construct()
    {

    }


    public function store(Request $request) {

        Log::info("Message-Request: " . json_encode($request->all()));
        try {
            //注册渠道
            $register_channel = $request->input('register_channel');
            //张三|9|12000
            $template_var    =   explode('|', $request->input('template_var'));

            //消息发送时间
            $send_at  = $request->input('send_at');

            $message_type = strtoupper($request->input('message_type'));

            $result = [];


            switch ($message_type) {
                case "CUSTOMER":
                    $customers_info = explode(',', $request->input('template_var'));
                    if (!$customers_info) {
                        return $this->setMessage("无效的参数")->responseTransaction();
                    }
                    break;

                case "PERSONAL":
                    $personal_ids = $request->input('personal_ids', false);
                    if ($personal_ids) {
                        if (is_numeric($personal_ids)) {

                            $users = [];
                            if (($u = Users::find($personal_ids))) {
                                $users = array_wrap($u);
                            }

                        }else {
                            $users = [];
                            $personal_ids = explode(',', $personal_ids);
                            foreach ($personal_ids as $p_id) {
                                if (($u = Users::find($p_id))) {
                                    $users[] = $u;
                                }
                            }
                        }

                    } else {
                        return $this->setMessage("无效的参数")->responseTransaction();
                    }

                    break;
                case "ORG":
                    $org_ids = $request->input('org_ids', false);
                    $org_ids = explode(',', $org_ids);

                    if ($org_ids) {
                        $user_ids = [];
                        foreach ($org_ids as $org_id) {
                            $orgs = Organizations::find($org_id);
                            if (!$orgs) {
                                return $this->responseNotFound();
                            }
                            $childAndSelf = $orgs->getDescendantsAndSelf();

                            foreach ($childAndSelf as $item) {

                                $org = Organizations::find($item->id);
                                $users = $org->users()
                                    ->whereNull('is_on_job')
                                    ->whereNull('is_enable')
                                    ->get();

                                foreach ($users as $user) {
                                    $user_ids[] = $user->pivot->user_id;
                                }
                            }
                        }
                        $user_ids = array_unique($user_ids);
                        $users = Users::find($user_ids);

                    } else {
                        return $this->setMessage("无效的参数")->responseTransaction();
                    }
                    break;
                case "ALL":
                    $users = Users::whereNull('is_on_job')
                        ->whereNull('is_enable')
                        ->get();
                    break;

                case "ROBOT":

                    break;
                default:
                    return $this->setMessage("未指定有效渠道")->responseTransaction();

            }

            //TODO: 指定是否为变量消息
            switch (strtolower($register_channel)) {
                case 'user_join_campany':

                    if (!$template_var || $template_var[0] == false) {

                    }

                    foreach ($users as $user) {
                        $result = Notification::send($user, new UserJoinCompany($template_var));
                    }
                    break;
                case 'user_get_code':
                    if (!$template_var || $template_var[0] == false) {
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new UserGetCode($template_var[0]));
                    }
                    break;
                case 'finance_fund_generate':
                    if (!$template_var || count($template_var) < 3) {
                        return $this->setMessage("款项消息参数有误")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new FinanceFundGenerate($template_var));
                    }
                    break;
                //TODO: 注册新的渠道
                case 'user_notice':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("未按公告渠道传递注册参数")->responseTransaction();
                    }

                    foreach ($users as $user) {
                        $result = Notification::send($user, new UserNotice($template_var));
                    }

                    break;

                case 'customer_useful_notification':
                    if (!$template_var || $template_var[0] == false) {

                    }

                    $skin = $request->input('skin', false);
                    $sign = $request->input('sign', false);

                    if (!$skin || !$sign) {
                        return $this->setMessage("无效的skin或者sign")->responseTransaction();
                    }

                    foreach ($customers_info as $customer_info) {
                        $result = Notification::send(Users::find(64), new CustomerUsefulNotification($skin, $sign, $customer_info));
                    }
                    break;

                case 'article_publish':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("文章内容不能为空")->responseTransaction();
                    }
                    $result = Notification::send(Users::find(64), new ArticlePublish($template_var));
                    break;

                case 'lose_job':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("离职通知内容不能为空")->responseTransaction();
                    }
                    $result = Notification::send(Users::find(64), new LoseJob($template_var));
                    break;

                case 'member_be_full':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("转正内容不能为空")->responseTransaction();
                    }
                    $result = Notification::send(Users::find(64), new BeFullMember($template_var));
                    break;

                case 'achieve_rank':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("业绩图片不能为空")->responseTransaction();
                    }
                    $result = Notification::send(Users::find(64), new AchieveRank($template_var));
                    break;

                case 'welcome_to_cop':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("入职描述不能为空")->responseTransaction();
                    }
                    $result = Notification::send(Users::find(64), new WelcomeToCop($template_var));
                    break;

                case 'complete_document':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("资料补齐内容不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new CompleteDocument($template_var));
                    }
                    break;

                case 'system_to_update':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("系统更新内容不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new SystemToUpdate($template_var));
                    }
                    break;

                case 'exam_sign_apply':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("考试申请内容不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new ExamSignApply($template_var));
                    }
                    break;

                case 'exam_sign_success':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("考试申请通过内容不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new ExamSignSuccess($template_var));
                    }
                    break;

                case 'exam_sign_failed':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("考试申请失败内容不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new ExamSignFailed($template_var));
                    }
                    break;

                case 'exam_invite':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("考试邀请内容不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new ExamInvite($template_var));
                    }
                    break;

                case 'exam_grade_publish':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("考试成绩发布内容不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new ExamGradePublish($template_var));
                    }
                    break;

                case 'exam_start_remind':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("考试开始提醒内容不能为空")->responseTransaction();
                    }

                    if ($send_at) {
                        $t = Carbon::createFromTimestamp($send_at);
                    }

                    foreach ($users as $user) {
                        if ($send_at) {
                            $result = $user->notify((new ExamStartRemind($template_var))->delay($t));
                        }else {
                            $result = Notification::send($user, new ExamStartRemind($template_var));
                        }
                    }
                    break;

                case 'questionnaire_apply':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("问卷调查的参数内容不能为空")->responseTransaction();
                    }
                    if ($send_at) {
                        $t = Carbon::createFromTimestamp($send_at);
                    }

                    foreach ($users as $user) {
                        if ($send_at) {

                            $result = $user->notify((new QuestionnaireApply($template_var))->delay($t));
                        } else {

                            $result = Notification::send($user, new QuestionnaireApply($template_var));
                        }
                    }
                    break;

                case 'qa_get_question':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("收到问题参数不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new QAGetQuestion($template_var));
                    }
                    break;

                case 'qa_get_answer':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("收到答案参数不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new QAGetAnswer($template_var));
                    }
                    break;

                case 'contract_check_out':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("退租通知参数不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new ContractCheckOut($template_var));
                    }
                    break;

                case 'finance_reimbursement':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("报销通知参数不能为空")->responseTransaction();
                    }
                    foreach ($users as $user) {
                        $result = Notification::send($user, new FinanceReimbursement($template_var));
                    }
                    break;

                case 'payment_notice':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("尾款内容不能为空")->responseTransaction();
                    }

                    foreach ($users as $user) {
                        $result = Notification::send($user, new PaymentNotice($template_var));
                    }
                    break;

                case 'common_text_notice':
                    if (!$template_var || $template_var[0] == false) {
                        return $this->setMessage("通知信息不能为空")->responseTransaction();
                    }

                    foreach ($users as $user) {
                        $result = Notification::send($user, new CommonTextNotice($template_var));
                    }
                    break;

                default:
                    return $this->setMessage('未注册的消息渠道')->responseTransaction();
            }
            Log::info("Message-Response: " . json_encode($result));
            return $this->response($result);

            //消息类型 message_type 个人: PERSONAL, 部门: ORG, 全公司: ALL
            //personal_ids, org_ids, all

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function index(Request $request) {
        $user = Auth::guard('api')->user();
        if (!$user) {
            return $this->setMessage("未获取到到当前用户")->responseTransaction();
        }
        if ($request->input('unread')) {
            $messageCollect = $user->unreadNotifications()->paginate($request->input('per_page_number'));
        }else {
            $messageCollect = $user->notifications()->paginate($request->input('per_page_number'));

        }
        return new MessageResourceCollection($messageCollect);
    }

    public function update(Request $request, $id) {
        try{
            $user = Auth::guard('api')->user();
            $message = $user->unreadNotifications()->find($id);
            if ($message) {
                return $this->response($message->markAsRead());
            } else {
                return $this->responseNotFound();
            }
            //$user->unreadNotifications()->update(['read_at' => Carbon::now()]);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

    }

}