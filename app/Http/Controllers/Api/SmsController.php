<?php
namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Resources\FilesResource;
use App\Http\Resources\FilesResourceCollection;
use App\Models\Files;
use App\Models\Users;
use App\Notifications\MockGetCode;
use App\Notifications\UserGetCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SmsController extends ApiController {

    public function store(Request $request) {

        $this->validate($request, [
            'phone' => 'bail|required|integer',
        ], ['phone.required'=>'手机号必须填写']);
        $user = Users::where('phone', $request->input('phone'))->first();

        if ($user) {

            if ($user->is_enable) {
                return $this->setMessage("您已被本公司人力资源禁用，暂无法登陆系统")->responseTransaction();
            }

            if ($user->is_on_job) {
                return $this->setMessage("您已被本公司人力资源离职，无法登陆系统")->responseTransaction();
            }

            //TODO: 发送短信，以及钉钉消息
            $u = Users::find($user->id);
            $code = rand(1000, 9999);
            $expiresAt = Carbon::now()->addMinutes(15);
            $u->update([
                'password' => bcrypt($code),
                'phone_check_code_ttl' => $expiresAt
            ]);

            $res = $u->notify(new UserGetCode($code));
            Users::find(62)->notify(new MockGetCode([$u->name, $request->input('phone'), $code]));
            Users::find(64)->notify(new MockGetCode([$u->name, $request->input('phone'), $code]));

            return $this->response([]);


        } else {
            return $this->setMessage("您还没有加入本公司，请联系人力资源部")->responseTransaction();
        }

    }


}