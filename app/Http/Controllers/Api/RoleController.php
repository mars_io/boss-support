<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Requests\RoleCreateRequest;
use App\Http\Requests\RoleUpdateRequest;
use App\Http\Resources\RoleResource;
use App\Http\Resources\RoleResourceCollection;
use App\Models\Organizations;
use App\Models\Positions;
use App\Models\Role;
use App\Models\Users;
use Illuminate\Http\Request;

class RoleController extends ApiController
{

    public function __construct()
    {
    }

    public function index(Request $request) {

        try{
            return new RoleResourceCollection(Role::paginate($request->input('per_page_number')));
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(RoleCreateRequest $request) {

        try {
            $data = Role::create($request->all());
            return $this->response($data);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

        $role = Role::find($id);
        if (!$role) {
            return $this->responseNotFound();
        }
        return $this->response(new RoleResource($role));

    }

    public function destroy($id) {
        try{

            $role = Role::find($id);
            if (!$role) {
                return $this->responseNotFound();
            }
            if (Users::whereRoleIs($role->name)->count()) {
                return $this->setMessage("该角色下存在关联用户，请先移出关联用户")->responseTransaction();
            }
            if ($role->position_id) {
                if(Positions::find($role->position_id)) {
                    return $this->setMessage("该角色下存在关联岗位，请先移出关联岗位")->responseTransaction();
                }
            }

            $data = $role->delete();
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function update(RoleUpdateRequest $request, $id) {

        try {
            $role = Role::find($id);
            if (!$role) {
                return $this->responseNotFound();
            }

            $data = $role->update(['display_name'=>$request->input('display_name')]);
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     *
     */
    public function batch(Request $request) {

        try{
            putenv('x-batch=1');
            $data = [];
            if (!($batch = $request->batch) || !($batch = \GuzzleHttp\json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
        if (isset($batch['store']) && count($batch['store'])) {
            $req = new OrganizationsStoreRequest;

            foreach ($batch['store'] as $val) {
                foreach ($val as $k => $v) {
                    $req->offsetSet($k, $v);
                }
                $data[] = $this->store($req);
            }
        }

        if (isset($batch['update']) && count($batch['update'])) {

        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }

        }

        if (isset($batch['destroy']) && count($batch['destroy'])) {
            foreach ($batch['destroy'] as $val) {
                $data[] = $this->destroy($val);
            }

        }

        putenv('x-batch=0');
        return $this->response($data);


        /*
        batch: {
            {
                "store": [{
                    "name": "Marvin",
                    "age": 18
                }, {
                    "name": "Alice",
                    "age": 22
                }],
                "update": {
                    "8": {
                        "name": "David"
                    },
                    "11": {
                        "name": "Dannis",
                        "age": 30
                    }
                },
                "show": [1, 3, 5, 7],
                "destroy": [2, 4, 6]
            }
        }
        */
//        $batch = [
//            'store' => [['name' => 'Marvin', 'age' => 18], ['name' => 'Alice', 'age' => 22]],
//            'update' => ['8' => ['name' => 'David'], '11' => ['name' => 'Dannis', 'age' => 30]],
//            'show' => [1, 3, 5, 7],
//            'destroy' => [2, 4, 6]
//        ];


    }

}
