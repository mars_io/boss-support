<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Requests\PermissionCreateRequest;
use App\Http\Requests\PermissionUpdateRequest;
use App\Http\Resources\PermissionResource;
use App\Http\Resources\PermissionResourceCollection;
use App\Models\Modules;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PowersController extends ApiController
{

    public function index(Request $request) {

        $user_id = $request->input('user_id');

        if (!$user_id) {
            $user = Auth::guard('api')->user();
        }else {
            if (!($user = Users::find($request->input('user_id')))) {
                return $this->setMessage("未找到匹配用户")->responseTransaction();
            }
        }

        if (!($action = $request->input('action')) || !in_array($action, ['can', 'role'])) {
            return $this->setMessage("权限行为不能为空")->responseTransaction();
        }

        if (!($has = $request->input('has'))) {
            return $this->setMessage("角色或权限单元不能为空")->responseTransaction();
        }

        $is_strict = (boolean)$request->input('is_strict', false);

        $has = explode(',', $has);

        if ($action == "can") {
            $result = $user->can($has, $is_strict);
        }
        if ($action == "role") {
            $result = $user->hasRole($has, $is_strict);
        }

        return $this->response($result);
    }

    public function show($id) {

        if (!($user = Users::find($id))) {
            return $this->setMessage("用户不能为空")->responseTransaction();
        }
        return $this->response($user->allPermissions());

    }

    public function withRole(Request $request, $id) {

        if (!($with = $request->input('with'))) {
            return $this->setMessage("角色关联不能为空")->responseTransaction();
        }
        if (!($role = $request->input('role'))) {
            return $this->setMessage("角色单元不能为空")->responseTransaction();
        }
        $user = Users::find($id);

        if ($with == 'attach') {
            $roles = explode(",", $role);
            foreach ($roles as $role){
                if (!$user->hasRole($role)) {
                    $user->attachRole($role);
                }
            }
        }

        if ($with == 'detach') {
            $roles = explode(",", $role);
            foreach ($roles as $role){
                $user->detachRole($role);
            }
        }
        return $this->response($user->roles()->get());
    }

    public function getRole($id) {
        $user = Users::find($id);
        $roles = $user->roles()->get();
        return $this->response($roles);
    }

    public function update(Request $request, $id) {

        try {

            if (!($permissions = $request->input('permissions'))) {
                return $this->setMessage("权限集合不能为空")->responseTransaction();
            }
            $permissions = explode(',', $request->input('permissions'));
            $on = $request->input('on');

            if ($on == 'user') {
                if (!($user = Users::find($id))) {
                    return $this->setMessage("用户不能为空")->responseTransaction();
                }
                $res = $user->syncPermissions($permissions);
            }

            if ($on == 'role') {
                if (!($role = Role::find($id))) {
                    return $this->setMessage("角色不能为空")->responseTransaction();
                }
                $res = $role->syncPermissions($permissions);
            }

            return $this->response($res);
        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }


}
