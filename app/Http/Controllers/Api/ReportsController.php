<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Resources\ReportsResource;
use App\Http\Resources\ReportsResourceCollection;
use App\Models\Reports;
use App\Models\Users;
use Brexis\LaravelWorkflow\Facades\WorkflowFacade as Workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laratrust\Traits\LaratrustUserTrait;


class ReportsController extends ApiController
{

    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return new ReportsResourceCollection(Reports::paginate());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            dd('ffff');

            $report = Reports::create([
                'house_id' => $request->input('house_id'),
                'user_id' => $request->input('user_id'),
                'content' => $request->input('content'),
            ]);


            return $this->response($report);
        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $report = Reports::find($id);
            $workflow = Workflow::get($report);
            $res = $workflow->can($report, 'to_director_review');

            $transitions = $workflow->getEnabledTransitions($report);
             dd($res);


            $apl = $workflow->apply($report, 'to_director_review');
            $sa = $report->save();

            if (!$report) {
                return $this->responseNotFound();
            }
            return $this->response(\GuzzleHttp\json_decode($report));
        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{


        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function batch() {

    }
}
