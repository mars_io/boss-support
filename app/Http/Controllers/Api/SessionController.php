<?php

namespace App\Http\Controllers\Api;

use App\Events\UserDestroyEvent;
use App\Events\UserStoreEvent;
use App\Events\UserUpdateEvent;
use App\Exceptions\AbortException;
use App\Http\Requests\UsersCreateRequest;
use App\Http\Requests\UsersIndexRequest;
use App\Http\Requests\UsersUpdateRequest;
use App\Http\Resources\SessionResource;
use App\Http\Resources\UsersResource;
use App\Http\Resources\UsersResourceCollection;
use App\Models\Organizations;
use App\Models\Process;
use App\Models\Users;
use App\Notifications\UserJoinCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SessionController extends ApiController
{

    public function __construct()
    {

    }

    public function index(Request $request) {
        if (($user = Auth::guard('api')->user())) {
            return $this->response(new SessionResource($user));
        } else {
            return $this->responseUnauthorized();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function batch() {

    }
}
