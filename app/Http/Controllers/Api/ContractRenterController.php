<?php

namespace App\Http\Controllers\Api;

use App\Events\Boss2Event;
use App\Exceptions\AbortException;
use App\Http\Requests\SystemsCreateRequest;
use App\Http\Requests\SystemsUpdateRequest;
use App\Http\Resources\ContractLordResourceCollection;
use App\Http\Resources\ContractLordResource;
use App\Http\Resources\ContractRenterResourceCollection;
use App\Http\Resources\ContractRenterResource;
use App\Http\Resources\HousesResource;
use App\Http\Resources\HousesResourceCollection;
use App\Http\Resources\SystemsResource;
use App\Http\Resources\SystemsResourceCollection;
use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Houses;
use App\Models\Organizations;
use App\Models\Systems;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Brexis\LaravelWorkflow\Facades\WorkflowFacade as Workflow;


class ContractRenterController extends ApiController
{

    public function index(Request $request) {
        try{
            $ContractRenterCollect = ContractRenter::search($request->input('q'))
                ->where(function ($query) use ($request){

                    if (($doc_status = $request->input('doc_status'))) {
                        $query->where("doc_status->$doc_status", 1);
                        if ($doc_status == "draft") {
                            $query->orWhereNull("doc_status");
                        }
                    }

                    if (($visit_status = $request->input('visit_status'))) {
                        $query->where("visit_status->$visit_status", 1);
                        if ($visit_status == "draft") {
                            $query->orWhereNull("visit_status");
                        }
                    }

                    if ($request->input('org_id') && (int)$request->input('org_id') !== 1) {
                        $org = Organizations::find($request->input('org_id'));
                        if ($org){
                            $org_ids = array_unique($org->getDescendantsAndSelf()->pluck('id')->toArray());
                            if ($request->input('or_user_id')) {
                                $query->whereIn('sign_org_id', $org_ids)->orWhere('user_id', $request->input('or_user_id'));
                            }else {
                                $query->whereIn('sign_org_id', $org_ids);
                            }
                        }
                    }

                    if ($request->input('user_id')) {
                        $query->where('user_id', $request->input('user_id'));
                    }

                    if (($house_id = $request->input('house_id'))) {
                        $house_id = array_unique(explode(',', $house_id));
                        $query->whereIn('rentable_id', $house_id);
                        $query->where('rentable_type', Houses::class);
                    }

                    // 未上传合同(合同编号+合同照片必须同时存在才算合同已上传),
                    if ($request->input('un_upload')) {
                        $query->whereNull('contract_number');
                        $query->whereNull('renter_album->renter');
                    }

                    //上传日期
                    if ($request->input('publish_start_time')) {
                        $query->whereBetween('created_at',
                            [
                                $request->input('publish_start_time'),
                                $request->input('publish_end_time', Carbon::now())
                            ]);
                    }

                    //开始日期
                    if ($request->input('renter_start_start_time')) {
                        $query->whereBetween('start_at',
                            [
                                $request->input('renter_start_start_time'),
                                $request->input('renter_start_end_time', Carbon::now())
                            ]);
                    }

                    //结束日期
                    if ($request->input('renter_end_start_time')) {
                        $query->whereBetween('end_at',
                            [
                                $request->input('renter_end_start_time'),
                                $request->input('renter_end_end_time', Carbon::now())
                            ]);
                    }

                    //房屋状态
                    if (($status = $request->input('status'))) {

                        if ((int)$status === 1) {
                            //正在出租
                            $query->whereNotNull('contract_number');
                            $query->whereNull('end_real_at');
                        }
                        if ((int)$status === 2) {
                            //快结束
                            $query->whereNotNull('contract_number');
                            //$query->whereBetween();
                            $query->whereNull('end_real_at');

                        }
                        if ((int)$status === 3) {
                            //已结束
                            $query->whereNotNull('contract_number');
                            $query->whereNotNull('end_real_at');
                        }

                        if ((int)$status === 4) {
                            //签约中
                            $query->whereNull('contract_number');
                        }
                    }
                })
                ->with('customers')
                ->with('rentable')
                ->with('agency_contract')
                ->with('user')
                ->with('sign_user')
                ->with('sign_org')
                ->distinct()
                ->orderBy('id', 'desc')
                ->when(($report = $request->input('report')), function ($query) use ($report, $request) {

                    //普通租房
                    if ($report == "bulletin_rent_basic") {
                        $query->whereNotNull('contract_number');
                        $query->whereNull('end_real_at');
                        //return $query->take(1)->get();
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //续租
                    if ($report == "bulletin_rent_continued") {
                        $query->whereNotNull('contract_number');
                        $query->whereNull('end_real_at');
                        //return $query->take(1)->get();
                        return $query->paginate($request->input('per_page_number'));

                    }

                    //转租
                    if ($report == "bulletin_rent_trans") {
                        $query->whereNotNull('contract_number');
                        $query->whereNull('end_real_at');
                        //return $query->take(1)->get();
                        return $query->paginate($request->input('per_page_number'));

                    }

                    //调租
                    if ($report == "bulletin_change") {
                        //新地址
                        if ((int)$request->input('able_type') === 1) {
                            $query->whereNotNull('contract_number');
                            $query->whereNull('end_real_at');
                            return $query->take(1)->get();
                        }

                        return $query->paginate($request->input('per_page_number'));

                    }

                    //清退
                    if ($report == "bulletin_banish") {

                        //取消清退
                        if ((int)$request->input('able_type') === 1) {
                            $query->where('end_type', 4);
                        }
                        $query->whereNotNull('contract_number');
                        $query->whereNull('end_real_at');
                        //return $query->take(1)->get();
                        return $query->paginate($request->input('per_page_number'));

                    }


                    if ($report == "bulletin_refund") {
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //房租尾款
                    if ($report == "bulletin_retainage") {
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //退租
                    if ($report == "bulletin_checkout") {
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //炸单
                    if ($report == "bulletin_lose") {

                        //取消炸单
                        if ((int)$request->input('able_type') === 1) {
                            $query->where('end_type', 1);
                        }

                        $query->whereNull('end_real_at');
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //中介费
                    if ($report == "bulletin_agency") {
                        return $query->paginate($request->input('per_page_number'));
                    }

                    //特殊事项
                    if ($report == "bulletin_special") {
                        return $query->paginate($request->input('per_page_number'));
                    }
                })
                ->when(!$request->input('report'), function ($query) use ($request) {
                    return $query->paginate($request->input('per_page_number'));
                });
            return new ContractRenterResourceCollection($ContractRenterCollect);
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(Request $request) {

        try {
            $user = Auth::guard('api')->user();
            if ($user) {
                $request->offsetSet('user_id', $user->id);
                if ($user->orgs->isNotEmpty()) {
                    $request->offsetSet('org_id', $user->orgs[0]->id);
                }
            }

            $request->offsetSet('rentable_id', $request->input('house_id'));
            $request->offsetSet('rentable_type', Houses::class);

            $renter = ContractRenter::create($request->all());

            if ($request->input('customer_id')) {
                $customerIds = explode(',', $request->input('customer_id'));
                $renter->customers()->sync($customerIds);
            }

            return $this->response($renter);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

        $renterInfo = ContractRenter::where('id', $id)
            ->orWhere('contract_number', $id)
            ->with('customers')
            ->with('rentable')
            ->with('agency_contract')
            ->with('user')
            ->with('sign_user')
            ->with('sign_org')
            ->first();

        if (!$renterInfo) {
            return $this->responseNotFound();
        }

        $workflow = Workflow::get($renterInfo, 'contract_doc_verify');
        $transitions = $workflow->getEnabledTransitions($renterInfo);
        $operation = [];
        foreach ($transitions as $transition) {
            $operation[] = $transition->getName();
        }
        $renterInfo->contract_operation = $operation;

        $workflow = Workflow::get($renterInfo, 'visit_verify');
        $transitions = $workflow->getEnabledTransitions($renterInfo);
        $operation = [];
        foreach ($transitions as $transition) {
            $operation[] = $transition->getName();
        }
        $renterInfo->visit_operation = $operation;

        return $this->response(new ContractRenterResource($renterInfo));
    }

    public function destroy($id) {
        try{
            $renter = ContractRenter::find($id);
            if (!$renter) {
                return $this->responseNotFound();
            }

            $data = $renter->delete();
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function op_doc(Request $request, $id) {

        $renter = ContractRenter::find($id);
        if (!$renter) {
            return $this->responseNotFound();
        }

        $workflow = Workflow::get($renter, 'contract_doc_verify');
        if ($workflow->can($renter, $request->input('operation'))) {
            $res = $workflow->apply($renter, $request->input('operation'));
            $renter->save();
            return $this->response($res);
        } else {
            return $this->setMessage("无法跨流程节点操作")->responseTransaction();
        }
    }

    public function op_visit(Request $request, $id) {

        $renter = ContractRenter::find($id);
        if (!$renter) {
            return $this->responseNotFound();
        }
        $workflow = Workflow::get($renter, 'visit_verify');
        if ($workflow->can($renter, $request->input('operation'))) {
            $res = $workflow->apply($renter, $request->input('operation'));
            $renter->save();
            return $this->response($res);
        } else {
            return $this->setMessage("无法跨流程节点操作")->responseTransaction();
        }
    }

    public function update(Request $request, $id) {

        try {
            $renter = ContractRenter::find($id);
            if (!$renter) {
                return $this->responseNotFound();
            }

            //1炸单， 2正常退租，3违约退租  4协商退租， 5转租， 6调租
            if ($request->input('finish_at')) {
                $end_real_at = $request->input('finish_at', Carbon::now());
                $end_type = $request->input('finish_type');
                $renter->update([
                    'end_real_at'   =>  $end_real_at,
                    'end_type'  => $end_type,
                ]);
                return $this->response($renter);
            }

            if ($request->input('customer_id')) {
                $customerIds = explode(',', $request->input('customer_id'));
                $renter->customers()->sync($customerIds);
            }
            $request->offsetSet('rentable_id', $request->input('house_id'));
            $request->offsetSet('rentable_type', Houses::class);

            $data = $renter->update($request->all());

            if ($data) {
                event(new Boss2Event('contract_renter', $renter));
            }

            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function batch(Request $request) {
        try{
            putenv('x-batch=1');
            $data = [];

            if (!($batch = $request->batch) || !($batch = json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }
        }

        putenv('x-batch=0');
        return $this->response($data);
    }

}
