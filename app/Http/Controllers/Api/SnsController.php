<?php
namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Resources\UsersResource;
use App\Models\Users;
use Carbon\Carbon;
use EasyDingTalk\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SnsController extends ApiController {

    public function index(Request $request) {

    }

    public function show($id) {

        try{
            $user = Users::where('ding_user_id', $id)
                ->orWhere('phone', $id)
                ->with('orgs')
                ->with('roles')
                ->first();

            if ($user) {

                $code = str_random(32);
                $expiresAt = Carbon::now()->addMinutes(15);
                $res = $user->update([
                    'password' => bcrypt($code),
                    'phone_check_code_ttl' => $expiresAt
                ]);

                if ($user->ding_user_id) {
                    $app = new Application(['corp_id' => config('services.dingtalk.corp_id'), 'corp_secret' => config('services.dingtalk.corp_secret')]);
                    $dingUserInfo =$app->user->get($user->ding_user_id);
                    $user->avatar = $dingUserInfo['avatar'];
                    $user->save();
                }

                $user->code = $code;
                return $this->response(new UsersResource($user));
            } else {
                return $this->responseNotFound();
            }
        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function store(Request $request) {

    }

    public function update(Request $request, $id) {

    }

    public function destroy($id) {
        try{


        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }


}