<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;

use App\Http\Requests\ModulesCreateRequest;
use App\Http\Requests\ModulesUpdateRequest;
use App\Http\Resources\ModulesResource;
use App\Http\Resources\ModulesResourceCollection;
use App\Models\Modules;
use App\Models\Systems;
use Illuminate\Http\Request;

class ModulesController extends ApiController
{

    public function __construct()
    {
    }

    public function index(Request $request) {

        //第一次获取列表的时候，初始化根组织架构
        try{
            $moduleCollect = Modules::where(function ($query) use ($request){
                    $request->input('sys_id') && $query->where('sys_id', $request->input('sys_id'));
                })
                ->orderBy('id', 'asc')
                ->paginate($request->input('per_page_number'));
            return new ModulesResourceCollection($moduleCollect);
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(ModulesCreateRequest $request) {

        try {
            $system = Systems::find($request->input('sys_id'));
            $data = $system->modules()->create($request->all());
            return $this->response($data);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

        $modules = Modules::find($id);
        if (!$modules) {
            return $this->responseNotFound();
        }
        return $this->response(new ModulesResource($modules));

    }

    public function destroy($id) {
        try{
            $modules = Modules::find($id);
            if (!$modules) {
                return $this->responseNotFound();
            }
            if ($modules->permissions()->count()) {
                return $this->setMessage("该模块下存在关联权限单元，请先移出所有关联的权限单元")->responseTransaction();
            }

            $data = $modules->delete();
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function update(ModulesUpdateRequest $request, $id) {

        try {
            $modules = Modules::find($id);
            if (!$modules) {
                return $this->responseNotFound();
            }

            $data = $modules->update(['display_name'=>$request->input('display_name')]);
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     *
     */
    public function batch(Request $request) {

        try{
            putenv('x-batch=1');
            $data = [];
            if (!($batch = $request->batch) || !($batch = \GuzzleHttp\json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
        if (isset($batch['store']) && count($batch['store'])) {
            $req = new OrganizationsStoreRequest;

            foreach ($batch['store'] as $val) {
                foreach ($val as $k => $v) {
                    $req->offsetSet($k, $v);
                }
                $data[] = $this->store($req);
            }
        }

        if (isset($batch['update']) && count($batch['update'])) {

        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }

        }

        if (isset($batch['destroy']) && count($batch['destroy'])) {
            foreach ($batch['destroy'] as $val) {
                $data[] = $this->destroy($val);
            }

        }

        putenv('x-batch=0');
        return $this->response($data);


        /*
        batch: {
            {
                "store": [{
                    "name": "Marvin",
                    "age": 18
                }, {
                    "name": "Alice",
                    "age": 22
                }],
                "update": {
                    "8": {
                        "name": "David"
                    },
                    "11": {
                        "name": "Dannis",
                        "age": 30
                    }
                },
                "show": [1, 3, 5, 7],
                "destroy": [2, 4, 6]
            }
        }
        */
//        $batch = [
//            'store' => [['name' => 'Marvin', 'age' => 18], ['name' => 'Alice', 'age' => 22]],
//            'update' => ['8' => ['name' => 'David'], '11' => ['name' => 'Dannis', 'age' => 30]],
//            'show' => [1, 3, 5, 7],
//            'destroy' => [2, 4, 6]
//        ];


    }

}
