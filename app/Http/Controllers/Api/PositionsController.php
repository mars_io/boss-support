<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Requests\PositionsStoreRequest;
use App\Http\Requests\PositionsUpdateRequest;
use App\Http\Resources\PositionsResource;
use App\Http\Resources\PositionsResourceCollection;
use App\Models\Organizations;
use App\Models\Positions;
use App\Models\PositionTypes;
use App\Models\Role;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PositionsController extends ApiController
{

    public function __construct()
    {
    }

    public function index(Request $request) {

        try{
            $firstLevelId = false;
            if (($org_id = $request->input('org_id'))) {
                $org = Organizations::find($org_id);

                if (!$org) {
                    return $this->responseNotFound();
                }

                if (!$org->isRoot()) {
                    $firstLevelId = $org->getAncestorsAndSelfWithoutRoot()->first()->id;
                }
            }

            $positionsCollect = Positions::where(function ($query) use ($request){
                $request->input('type') && $query->where('type', $request->input('type'));
                $request->input('parent_id') && $query->where('parent_id', $request->input('parent_id'));
                })
                ->where(function ($query) use ($request, $firstLevelId){
                    if ($firstLevelId) {
                        $pos_ids = PositionTypes::where('org_id', $firstLevelId)->pluck('id')->toArray();
                        $query->whereIn('type', $pos_ids);
                    }
                })
                ->with('positiontypes')
                ->with('roles')
                ->orderBy('parent_id', 'asc')->paginate($request->input('per_page_number'));

            return new PositionsResourceCollection($positionsCollect);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(PositionsStoreRequest $request) {

        try {
            $data = DB::transaction(function () use($request){
                $position = Positions::create([
                    'name'  =>  $request->input('name'),
                    'type'  =>  $request->input('type'),
                    'parent_id'  =>  $request->input('parent_id', NULL),
                ]);

                if ($position) {
                    $position->roles()->create([
                        'name'  =>  $request->input('display_name'),
                        'display_name'  =>  $request->input('name'),
                        'description'  =>  $request->input('description'),
                    ]);
                }
                return $position;
            });

            if ($data) {
                $pos = Positions::where('id', $data->id)
                    ->with('positiontypes')
                    ->first();
                return $this->response(new PositionsResource($pos));
            } else {
                return $this->setMessage("新增岗位失败")->responseTransaction();
            }

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

        $position = Positions::where('id', $id)
            ->with('positiontypes')
            ->with('roles')
            ->first();
        if (!$position) {
            return $this->responseNotFound();
        }
        return $this->response(new PositionsResource($position));
    }

    public function destroy($id) {
        try{
            $position = Positions::find($id);
            if (!$position) {
                return $this->responseNotFound();
            }
            if (!$position->isLeaf()) {
                return $this->setMessage("请先删除其下级岗位")->responseTransaction();
            }

            $role_name = $position->roles()->first()->name;
            if (Users::whereRoleIs($role_name)->count() > 0 ) {
                return $this->setMessage("当前岗位下存在员工，请先移出该员工")->responseTransaction();
            }else {
                Role::where('name', $role_name)->first()->delete();
            }
            $data = $position->delete();
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function update(PositionsUpdateRequest $request, $id) {

        try {
            $position = Positions::find($id);
            if (!$position) {
                return $this->responseNotFound();
            }

            if ($position->update(['name' => $request->input('name')])) {
                $data = $position->roles()->update(['display_name' => $request->input('name')]);
                return $this->response($data);
            } else {
                return $this->setMessage("岗位名称修改失败")->responseTransaction();
            }

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     *
     */
    public function batch(Request $request) {

        try{
            putenv('x-batch=1');
            $data = [];
            if (!($batch = $request->batch) || !($batch = \GuzzleHttp\json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
        if (isset($batch['store']) && count($batch['store'])) {

        }

        if (isset($batch['update']) && count($batch['update'])) {

        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }

        }

        if (isset($batch['destroy']) && count($batch['destroy'])) {
            foreach ($batch['destroy'] as $val) {
                $data[] = $this->destroy($val);
            }

        }

        putenv('x-batch=0');
        return $this->response($data);


        /*
        batch: {
            {
                "store": [{
                    "name": "Marvin",
                    "age": 18
                }, {
                    "name": "Alice",
                    "age": 22
                }],
                "update": {
                    "8": {
                        "name": "David"
                    },
                    "11": {
                        "name": "Dannis",
                        "age": 30
                    }
                },
                "show": [1, 3, 5, 7],
                "destroy": [2, 4, 6]
            }
        }
        */
//        $batch = [
//            'store' => [['name' => 'Marvin', 'age' => 18], ['name' => 'Alice', 'age' => 22]],
//            'update' => ['8' => ['name' => 'David'], '11' => ['name' => 'Dannis', 'age' => 30]],
//            'show' => [1, 3, 5, 7],
//            'destroy' => [2, 4, 6]
//        ];


    }

}
