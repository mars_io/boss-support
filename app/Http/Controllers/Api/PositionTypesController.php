<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Resources\PositionTypesResource;
use App\Http\Resources\PositionTypesResourceCollection;
use App\Models\Organizations;
use App\Models\PositionTypes;
use Illuminate\Http\Request;

class PositionTypesController extends ApiController
{

    public function __construct()
    {
    }

    public function index(Request $request) {

        //第一次获取列表的时候，初始化根组织架构
        try{
            $firstLevelId = false;
            $org_id = $request->input('org_id');
            $org = Organizations::find($org_id);
            if (!$org) {
                return $this->responseNotFound();
            }
            if (!$org->isRoot()) {
                $firstLevelId = $org->getAncestorsAndSelfWithoutRoot()->first()->id;
            }

            $positionTypesCollect = PositionTypes::where(function ($query) use ($request, $firstLevelId){
                    $request->input('org_id') && $query->where('org_id', $firstLevelId);
                })
                ->with('orgs')
                ->paginate($request->input('per_page_number'));

            return new PositionTypesResourceCollection($positionTypesCollect);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(Request $request) {

        try {
            $org = Organizations::find($request->input('org_id'));
            if (!$org) {
                return $this->responseNotFound();
            }
            if ((int)$org->getLevel() !== 1) {
                return $this->setMessage("职位必须在一级部门")->responseTransaction();
            }
            $data = $org->positionTypes()->create($request->all());
            return $this->response($data);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

        $positionType = PositionTypes::where('id', $id)
            ->with('orgs')
            ->first();
        if (!$positionType) {
            return $this->responseNotFound();
        }
        return $this->response(new PositionTypesResource($positionType));

    }

    public function destroy($id) {
        try{
            $positionType = PositionTypes::find($id);
            if (!$positionType) {
                return $this->responseNotFound();
            }

            if ($positionType->positions()->count() > 0) {
                return $this->setMessage("该职位下存在岗位，请先移出下面的岗位")->responseTransaction();
            }

            $data = $positionType->delete();
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function update(Request $request, $id) {

        try {
            $positionType = PositionTypes::find($id);
            if (!$positionType) {
                return $this->responseNotFound();
            }
            if ($request->input('org_id')){
                $org = Organizations::find($request->input('org_id'));
                if ((int)$org->getLevel() !== 1) {
                    return $this->setMessage("职位必须在一级部门")->responseTransaction();
                }
            }
            $data = $positionType->update($request->all());
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     *
     */
    public function batch(Request $request) {

        try{
            putenv('x-batch=1');
            $data = [];
            if (!($batch = $request->batch) || !($batch = \GuzzleHttp\json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
        if (isset($batch['store']) && count($batch['store'])) {
            $req = new OrganizationsStoreRequest;

            foreach ($batch['store'] as $val) {
                foreach ($val as $k => $v) {
                    $req->offsetSet($k, $v);
                }
                $data[] = $this->store($req);
            }
        }

        if (isset($batch['update']) && count($batch['update'])) {

        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }

        }

        if (isset($batch['destroy']) && count($batch['destroy'])) {
            foreach ($batch['destroy'] as $val) {
                $data[] = $this->destroy($val);
            }

        }

        putenv('x-batch=0');
        return $this->response($data);


        /*
        batch: {
            {
                "store": [{
                    "name": "Marvin",
                    "age": 18
                }, {
                    "name": "Alice",
                    "age": 22
                }],
                "update": {
                    "8": {
                        "name": "David"
                    },
                    "11": {
                        "name": "Dannis",
                        "age": 30
                    }
                },
                "show": [1, 3, 5, 7],
                "destroy": [2, 4, 6]
            }
        }
        */
//        $batch = [
//            'store' => [['name' => 'Marvin', 'age' => 18], ['name' => 'Alice', 'age' => 22]],
//            'update' => ['8' => ['name' => 'David'], '11' => ['name' => 'Dannis', 'age' => 30]],
//            'show' => [1, 3, 5, 7],
//            'destroy' => [2, 4, 6]
//        ];


    }

}
