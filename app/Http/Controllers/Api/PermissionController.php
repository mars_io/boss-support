<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Requests\PermissionCreateRequest;
use App\Http\Requests\PermissionUpdateRequest;
use App\Http\Resources\PermissionResource;
use App\Http\Resources\PermissionResourceCollection;
use App\Models\Modules;
use App\Models\Permission;
use App\Models\Users;
use Illuminate\Http\Request;

class PermissionController extends ApiController
{

    public function __construct()
    {
    }

    public function index(Request $request) {

        //第一次获取列表的时候，初始化根组织架构
        try{
            $permissionCollect = Permission::where(function ($query) use ($request){
                $request->input('mod_id') && $query->where('mod_id', $request->input('mod_id'));
            })
                ->orderBy('id', 'desc')
                ->paginate($request->input('per_page_number'));
            return new PermissionResourceCollection($permissionCollect);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(PermissionCreateRequest $request) {

        try {
            $mod = Modules::find($request->input('mod_id'));
            if (!$mod) {
                return $this->responseNotFound();
            }
            $data = $mod->permissions()->create($request->all());
            return $this->response($data);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

        $permission = Permission::find($id);
        if (!$permission) {
            return $this->responseNotFound();
        }
        return $this->response(new PermissionResource($permission));

    }

    public function destroy($id) {
        try{

            if (!($permission = Permission::find($id))) {
                return $this->responseNotFound();
            }

            if (Users::wherePermissionIs($permission->name)->count()) {
                return $this->setMessage("该权限单元存在关联用户，请先解绑该用户")->responseTransaction();
            }

            $data = $permission->delete();
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function update(PermissionUpdateRequest $request, $id) {

        try {

            if (!($permission = Permission::find($id))) {
                return $this->responseNotFound();
            }

            $data = $permission->update([
                'display_name'=>$request->input('display_name'),
                'mod_id'    =>  $request->input('mod_id')
                ]);
            return $this->response($data);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    /**
     *
     */
    public function batch(Request $request) {

        try{
            putenv('x-batch=1');
            $data = [];
            if (!($batch = $request->batch) || !($batch = \GuzzleHttp\json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
        if (isset($batch['store']) && count($batch['store'])) {
            $req = new OrganizationsStoreRequest;

            foreach ($batch['store'] as $val) {
                foreach ($val as $k => $v) {
                    $req->offsetSet($k, $v);
                }
                $data[] = $this->store($req);
            }
        }

        if (isset($batch['update']) && count($batch['update'])) {

        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }

        }

        if (isset($batch['destroy']) && count($batch['destroy'])) {
            foreach ($batch['destroy'] as $val) {
                $data[] = $this->destroy($val);
            }

        }

        putenv('x-batch=0');
        return $this->response($data);


    }

}
