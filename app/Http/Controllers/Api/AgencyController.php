<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Requests\SystemsCreateRequest;
use App\Http\Requests\SystemsUpdateRequest;
use App\Http\Resources\AgencyResource;
use App\Http\Resources\AgencyResourceCollection;
use App\Http\Resources\CustomersResource;
use App\Http\Resources\CustomersResourceCollection;
use App\Http\Resources\HousesResource;
use App\Http\Resources\HousesResourceCollection;
use App\Http\Resources\SystemsResource;
use App\Http\Resources\SystemsResourceCollection;
use App\Models\Agency;
use App\Models\Customers;
use App\Models\Houses;
use App\Models\Systems;
use Illuminate\Http\Request;

class AgencyController extends ApiController
{
    public function index(Request $request) {
        try{
            $agencyCollect = Agency::where(function ($query) use ($request){
                })
                ->paginate($request->input('per_page_number'));

            return new AgencyResourceCollection($agencyCollect);
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(Request $request) {

        try {


        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {
        $agency = Agency::where('id', $id)->first();

        if (!$agency) {
            return $this->responseNotFound();
        }
        return $this->response(new AgencyResource($agency));
    }

    public function destroy($id) {
        try{


        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function update(Request $request, $id) {



        try {



        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function batch(Request $request) {

        try{
            putenv('x-batch=1');
            $data = [];
            if (!($batch = $request->batch) || !($batch = json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
        if (isset($batch['store']) && count($batch['store'])) {
            $req = new OrganizationsStoreRequest;

            foreach ($batch['store'] as $val) {
                foreach ($val as $k => $v) {
                    $req->offsetSet($k, $v);
                }
                $data[] = $this->store($req);
            }
        }

        if (isset($batch['update']) && count($batch['update'])) {
            foreach ($batch['update'] as $key=>$val) {
                $request->offsetSet('org_id', $val['org_id']);
                $data[] = $this->update($request, $key);
            }
        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }

        }

        if (isset($batch['destroy']) && count($batch['destroy'])) {
            foreach ($batch['destroy'] as $val) {
                $data[] = $this->destroy($val);
            }

        }

        putenv('x-batch=0');
        return $this->response($data);

    }



}
