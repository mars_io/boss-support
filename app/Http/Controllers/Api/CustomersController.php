<?php

namespace App\Http\Controllers\Api;

use App\Events\Boss2Event;
use App\Exceptions\AbortException;
use App\Http\Requests\SystemsCreateRequest;
use App\Http\Requests\SystemsUpdateRequest;
use App\Http\Resources\CustomersResource;
use App\Http\Resources\CustomersResourceCollection;
use App\Http\Resources\HousesResource;
use App\Http\Resources\HousesResourceCollection;
use App\Http\Resources\SystemsResource;
use App\Http\Resources\SystemsResourceCollection;
use App\Models\Customers;
use App\Models\Houses;
use App\Models\Systems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomersController extends ApiController
{
    public function index(Request $request) {
        try{
            $customerCollect = Customers::search($request->input('q'))
                ->where(function ($query) use ($request){

                    //搜索开单人下面的客户
                    if (($userId = $request->input('user_id'))) {
                        $query->where('user_id', $userId);
                    }
                })
                ->with('user')
                ->with('contract_lords')
                ->with('contract_renters')
                ->orderBy('id', 'desc')
                ->paginate($request->input('per_page_number'));

            return new CustomersResourceCollection($customerCollect);
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(Request $request) {

        try {

            if ($request->input('name')) {
                $py = pinyin_abbr($request->input('name'));
                $pinyin = str_replace(' ','', pinyin_sentence($request->input('name')));
                $request->offsetSet('py', $py);
                $request->offsetSet('pinyin', $pinyin);
            }

            $user = Auth::guard('api')->user();
            if ($user) {
                $request->offsetSet('user_id', $user->id);
                if ($user->orgs->isNotEmpty()) {
                    $request->offsetSet('org_id', $user->orgs[0]->id);
                }
            }

            $data = Customers::create($request->all());
            return $this->response($data);

        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {
        $customer = Customers::where('id', $id)
            ->with('user')
            ->with('contract_lords')
            ->with('contract_renters')
            ->first();
        if (!$customer) {
            return $this->responseNotFound();
        }
        return $this->response(new CustomersResource($customer));
    }

    public function destroy($id) {
        try{
            return $this->responseNotCaptured();
            $house = Houses::find($id);
            if (!$house) {
                return $this->responseNotFound();
            }

            $house = $house->delete();
            return $this->response($house);

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function update(Request $request, $id) {

        $customer = Customers::find($id);

        if (!$customer) {
            return $this->responseNotFound();
        }

        try {

            $data = $customer->update($request->all());
            if ($data) {
                event(new Boss2Event('customer', $customer));
                return $this->response($customer);
            } else {
                return $this->setMessage("修改客户信息失败")->responseTransaction();
            }


        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }

    }

    public function batch(Request $request) {

        try{
            putenv('x-batch=1');
            $data = [];
            if (!($batch = $request->batch) || !($batch = json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
        if (isset($batch['store']) && count($batch['store'])) {
            $req = new OrganizationsStoreRequest;

            foreach ($batch['store'] as $val) {
                foreach ($val as $k => $v) {
                    $req->offsetSet($k, $v);
                }
                $data[] = $this->store($req);
            }
        }

        if (isset($batch['update']) && count($batch['update'])) {
            foreach ($batch['update'] as $key=>$val) {
                $request->offsetSet('org_id', $val['org_id']);
                $data[] = $this->update($request, $key);
            }
        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }

        }

        if (isset($batch['destroy']) && count($batch['destroy'])) {
            foreach ($batch['destroy'] as $val) {
                $data[] = $this->destroy($val);
            }

        }

        putenv('x-batch=0');
        return $this->response($data);

    }



}
