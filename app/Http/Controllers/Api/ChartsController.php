<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Requests\SystemsCreateRequest;
use App\Http\Requests\SystemsUpdateRequest;
use App\Http\Resources\CustomersResource;
use App\Http\Resources\CustomersResourceCollection;
use App\Http\Resources\HousesResource;
use App\Http\Resources\HousesResourceCollection;
use App\Http\Resources\SystemsResource;
use App\Http\Resources\SystemsResourceCollection;
use App\Models\Customers;
use App\Models\Houses;
use App\Models\Systems;
use Illuminate\Http\Request;

class ChartsController extends ApiController
{
    public function index(Request $request) {
        try{

            //所有空置房源
            $emptyHouse = Houses::whereNull('rent_start_at')
                ->whereNotNull('lord_start_at')
                ->count();

            //运营中房源
            $lord_end_at = Houses::whereNotNull('lord_start_at')
                ->where('is_nrcy', 0)
                ->count();

            //待收房源
            $wait_lord_at = Houses::whereNull('lord_start_at')
                ->where('is_nrcy', 0)
                ->count();

            //黄色预警
            $yellowHouse = Houses::whereRaw('CURRENT_DATE() between DATE_ADD(warning_init_at, Interval 8 day) and DATE_ADD(warning_init_at, Interval 14 day)')->count();
            $orangeHouse = Houses::whereRaw('CURRENT_DATE() between DATE_ADD(warning_init_at, Interval 15 day) and DATE_ADD(warning_init_at, Interval 21 day)')->count();
            $redHouse = Houses::whereRaw('CURRENT_DATE() > DATE_ADD(warning_init_at, Interval 21 day)')->count();

            return $this->response([
                'house' =>  [
                    'emptyHouse' => $emptyHouse,
                    'lord_end_at' => $lord_end_at,
                    'wait_lord_at' =>  $wait_lord_at,
                    'yellowHouse' => $yellowHouse,
                    'orangeHouse' => $orangeHouse,
                    'redHouse' => $redHouse,
                ]
            ]);


        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function store(Request $request) {

        try {


        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

    }

    public function destroy($id) {
        try{


        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function update(Request $request, $id) {



    }

    public function batch(Request $request) {



    }



}
