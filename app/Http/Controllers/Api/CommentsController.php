<?php
namespace App\Http\Controllers\Api;

use App\Exceptions\AbortException;
use App\Http\Resources\CommentsResourceCollection;
use App\Http\Resources\FilesResource;
use App\Http\Resources\FilesResourceCollection;
use App\Models\Files;
use App\Models\Process;
use BrianFaust\Commentable\Models\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class CommentsController extends ApiController {


    public function index(Request $request) {

        try{
            $comments = Comment::where(function ($query) use ($request){
                    switch ($request->input('type', 'process')) {
                        case "process":
                            $query->where('commentable_type', 'App\Models\Process');
                            $query->where('commentable_id', $request->input('id'));
                            break;

                        case "article":
                            $query->where('commentable_type', 'article');
                            $query->where('commentable_id', $request->input('id'));
                            break;

                        default:
                            $query->where('commentable_type', 'App\Models\Process');
                            $query->where('commentable_id', $request->input('id'));

                    }
            })
                ->with('user')
                ->with('user.orgs')
                ->with('user.roles')
                ->orderBy('id', 'desc')
                ->paginate($request->input('per_page_number'));

            return new CommentsResourceCollection($comments);
        } catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
    }

    public function show($id) {

    }

    public function store(Request $request) {

        return $this->response($request->all());

        if (!$request->input('comment')) {
            return $this->setMessage("评论内容不能为空")->responseTransaction();
        }

        if (!$request->input('commentable_type')) {
            return $this->setMessage("评论标示不能为空")->responseTransaction();
        }

        if (!$request->input('commentable_id')) {
            return $this->setMessage("评论所属标示id不能为空")->responseTransaction();
        }

        $currentUserId = Auth::guard('api')->id();
        $comment = new Comment();
        $comment->body = $request->input('comment');
        $comment->creator_id = $currentUserId;
        $comment->creator_type = "App\Models\Users";
        $comment->commentable_type = $request->input('commentable_type');
        $comment->commentable_id = $request->input('commentable_id');
        $comment->save();

        if ($request->input('album')) {
            $comment->album = json_encode($request->input('album'));
            $comment->save();
        }

        return $this->response($comment);
    }

    public function batch(Request $request) {
        try{

            putenv('x-batch=1');
            $data = [];

            if (!($batch = $request->batch) || !($batch = json_decode($batch, true))) {
                return $this->setMessage('没有获取到有效参数')->responseTransaction();
            }
            //TODO: 增加middleware 来识别批处理

        }catch (\Exception $exception) {
            throw new AbortException($exception->getMessage());
        }
//        $b = ['update' => ['204' => ['display_name' => 'aaaa'], '205' => ['display_name' => 'bbbb'], '206' => ['display_name' => 'cccc']]];
//        dd(json_encode($b));
        //dd($batch['update']);
        if (isset($batch['update']) && count($batch['update'])) {
            foreach ($batch['update'] as $key=>$val) {
                $request->offsetSet('display_name', $val['display_name']);
                $data[] = $this->update($request, $key);
            }
        }

        if (isset($batch['show']) && count($batch['show'])) {
            foreach ($batch['show'] as $val) {
                $data[] = $this->show($val);
            }
        }

        putenv('x-batch=0');
        return $this->response($data);
    }

}