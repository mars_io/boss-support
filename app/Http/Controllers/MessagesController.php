<?php
namespace App\Http\Controllers;

use App\Exceptions\AbortException;
use App\Http\Controllers\Controller;
use EasyDingTalk\Application;
use Illuminate\Http\Request;

class MessagesController extends Controller {

    public $ding;

    public function __construct()
    {
        $options = [
            'corp_id' => config('services.dingtalk.corp_id'),
            'corp_secret' => config('services.dingtalk.corp_secret'),
        ];
        $this->ding = new Application($options);
    }

    public function send(Request $request) {

        try{
            $res = $this->ding->async_message->send([
                'agent_id' => config('services.dingtalk.agent_id'),
                'userid_list' => '066344416238825261',
                'msgtype' => 'action_card',
                'msgcontent' => json_encode([
                    'title' => '一个标题',
                    'markdown' => '一个正文c',
                    'single_title' => '一个详情h',
                    'single_url' => 'http://www.baidu.com',
                ]),
            ]);

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res,
                ]);
            }

        }catch (\Exception $exception) {
            throw new AbortException(401, $exception->getMessage());
        }

    }

    /**
     * 获取消息分发的进度以及结果
     * @param Request $request
     */
    public function process(Request $request) {

        try{
            $res = $this->ding->async_message->result(config('services.dingtalk.agent_id'), $request->input('task_id'));

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res,
                ]);
            }

        }catch (\Exception $exception) {
            throw new AbortException(401, $exception->getMessage());
        }

    }
}