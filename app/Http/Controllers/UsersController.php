<?php

namespace App\Http\Controllers;

use App\Events\Event;
use App\Events\UserGetUserEvent;
use App\Exceptions\AbortException;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserResourceCollection;
use App\Listeners\EventListener;
use App\Mod;
use App\Notifications\Message;
use App\Sys;
use App\Permission;
use App\Role;
use App\Models\Organization;
use Illuminate\Http\Request;
use EasyDingTalk\Application;

use App\Http\Requests;

use App\Models\Users;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;



class UsersController extends Controller
{

    /**
     * @var UserValidator
     */
    protected $validator;

    /**
     * Dingding instance
     * @var Application
     */
    protected $ding;

    public function __construct()
    {

        $options = [
            'corp_id' => config('services.dingtalk.corp_id'),
            'corp_secret' => config('services.dingtalk.corp_secret'),
        ];
        $this->ding = new Application($options);
    }

    /**
     * 添加用户基础信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUser(Request $request) {

        $this->validate($request, [
            'name' => 'bail|required|max:255',
            'phone' => 'bail|required|max:11',
            //'email' => 'bail|required|email|max:255',
            'organization_id' => 'bail|required|integer|exists:'.(new Organization())->getTable().',id,deleted_at,NULL'
        ]);

        try {

            $ding_department_id = Organization::where('id', $request->input('organization_id'))->first()->ding_department_id;

            $res = $this->ding->user->create([
                'name' => $request->input('name'),
                'mobile' => $request->input('phone'),
                'department' => (array)$ding_department_id
            ]);

            if ((int)$res['errcode'] === 0) {
                $res = User::create([
                    'name' => $request->input('name'),
                    'phone' => $request->input('phone'),
                    'ding_user_id' => $res['userid'],
                    'organization_id' => $request->input('organization_id')
                ]);
                if (request()->wantsJson()) {

                    return response()->json([
                        'data' => $res
                    ]);
                }
            }

        }catch (\Exception $exception) {
            throw new AbortException('401', $exception->getMessage());
        }

    }


    /**
     * 修改用户基础信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setUser(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|exists:'.(new User())->getTable().',id,deleted_at,NULL',
            'name' => 'bail|required|max:255',
            'phone' => 'bail|required|max:11',
            //'email' => 'bail|required|email|max:255',
            'organization_id' => 'bail|required|integer|exists:'.(new Organization())->getTable().',id,deleted_at,NULL'
        ]);

        try {
            $ding_user_id = User::where('id', $request->input('id'))->first()->ding_user_id;
            $ding_department_id = Organization::where('id', $request->input('organization_id'))->first()->ding_department_id;

            $res = $this->ding->user->update([
                'userid' => $ding_user_id,
                'name' => $request->input('name'),
                'mobile' => $request->input('phone'),
                'department' => (array)$ding_department_id
            ]);


            if ((int)$res['errcode'] === 0) {

                $res = User::find($request->input('id'))->update([
                    'name' => $request->input('name'),
                    'phone' => $request->input('phone'),
                    'ding_user_id' => $ding_user_id,
                    'organization_id' => $request->input('organization_id')
                ]);
                if (request()->wantsJson()) {

                    return response()->json([
                        'data' => $res
                    ]);
                }
            }
        }catch (\Exception $exception) {
            throw new AbortException('401', $exception->getMessage());
        }

    }

    /**
     * 删除用户
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser(Request $request) {

        //TODO: 删除前，需要检验该用户下的关联数据资料
        $this->validate($request, [
            'id' => 'bail|required|exists:'.(new User())->getTable().',id,deleted_at,NULL',
        ]);

        try {
            $ding_user_id = User::where('id', $request->input('id'))->first()->ding_user_id;
            $res = $this->ding->user->delete((array)$ding_user_id);

            if ((int)$res['errcode'] === 0) {

                $res = User::find($request->input('id'))->delete();
                if (request()->wantsJson()) {

                    return response()->json([
                        'data' => $res
                    ]);
                }
            }

        }catch (\Exception $exception) {
            throw new AbortException('401', $exception->getMessage());
        }

    }


    /**
     * 获取用户
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser(Request $request) {


        //TODO: 删除前，需要检验该用户下的关联数据资料
        $this->validate($request, [
            'id' => 'bail|required|exists:'.(new User())->getTable().',id,deleted_at,NULL',
        ]);

        try {

            $res = User::find($request->input('id'));

            //\Event::fire(new Event($res));
            //\Event::fire(new UserGetUserEvent($res));
            if (request()->wantsJson()) {

                return response()->json([
                    //'data' => new UserResource($res)
                    'data' => new UserResource($res)
                    //'data' => new UserResourceCollection(User::all())
                ]);
            }

        }catch (\Exception $exception) {
            throw new AbortException('401', $exception->getMessage());
        }

    }


    /**
     * 搜索用户
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchUser(Request $request) {

        $this->validate($request, [
            'keyword' => 'bail|required|max:255'
        ]);

        try {

            $res = User::search($request->input('keyword'))->paginate(10);
            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }

        }catch (\Exception $exception) {
            throw new AbortException('401', $exception->getMessage());
        }

    }

    /**
     * 离职复职员工
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dimissionToggleUser(Request $request) {
        //TODO: 复职员工，不需要重复录入员工信息，启用复职即可
        $this->validate($request, [
            'id' => 'bail|required|exists:'.(new User())->getTable().',id,deleted_at,NULL',
        ]);
        try{

            $is_job = User::find($request->input('id'))->is_job;

            if ($is_job) {
                User::find($request->input('id'))->update(['is_job' => false]);
                $ding_user_id = User::where('id', $request->input('id'))->first()->ding_user_id;
                $res = $this->ding->user->delete((array)$ding_user_id);
            }else {
                $res = User::find($request->input('id'))->update(['is_job' => true]);
            }
            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }

        }catch (\Exception $exception) {
            throw new AbortException(401, $exception->getMessage());
        }
    }

    /**
     * 禁用启用用户
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function disableToggleUser(Request $request) {
        $this->validate($request, [
            'id' => 'bail|required|exists:'.(new User())->getTable().',id,deleted_at,NULL',
        ]);
        try{

            $is_enable = User::find($request->input('id'))->is_enable;


            if ($is_enable) {
                $res = User::find($request->input('id'))->update(['is_enable' => false]);

            }else {
                $res = User::find($request->input('id'))->update(['is_enable' => true]);
            }
            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $res
                ]);
            }

        }catch (\Exception $exception) {
            throw new AbortException(401, $exception->getMessage());
        }
    }

    public function checkSms(Request $request) {

        $user = Users::find(1);
        $code = rand(1000, 9999);

        $expiresAt = Carbon::now()->addMinutes(15);
        $res = Users::where('phone', $request->input('phone'))->update([
            'password' => bcrypt($code),
            'phone_check_code_ttl' => $expiresAt
        ]);

        return $code;
    }

    public function loginByPhone(Request $request) {
        $this->validate($request, [
            'phone' => 'bail|required|exists:'.(new User())->getTable().',phone,deleted_at,NULL',
            'code' => 'bail|required'
        ]);

        $code = Cache::get('PHONE_CODE:' . $request->input('phone'));
        if ($code == $request->input('code')) {

        }


    }

}
