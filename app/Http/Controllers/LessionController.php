<?php
namespace App\Http\Controllers;

use App\Transformers\LessionTransformer;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Response;

class LessionController extends ApiController {

    protected $lessionTransformer;

    public function __construct(LessionTransformer $lessionTransformer)
    {
        $this->lessionTransformer = $lessionTransformer;
    }

    public function index(){

        $lession = Lession::all();
        return Response::json([
            'status' => 'success',
            'status_code' => 200,
            'data' => $this->lessionTransformer->transformCollection($lession->toArray())
        ]);
    }
    public function show($id) {
        $lession = Lession::findOrFail($id);
        if (!0) {
            return $this->responseNotFound("没有找到信息");
        }
        return $this->response([
            'status' => 'success',
            'data' => $this->lessionTransformer->transform($lession)
        ]);

//        return Response::json([
//            'status' => 'success',
//            'status_code' => 200,
//            'data' => $this->lessionTransformer->transform($lession)
//        ]);

    }
}