<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\OrganizationCreateRequest;
use App\Http\Requests\OrganizationAddRequest;
use App\Http\Requests\OrganizationUpdateRequest;
use App\Repositories\OrganizationRepository;
use App\Validators\OrganizationValidator;


class OrganizationsController extends Controller
{

    /**
     * @var OrganizationRepository
     */
    protected $repository;

    /**
     * @var OrganizationValidator
     */
    protected $validator;

    public function __construct(OrganizationRepository $repository, OrganizationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function addOrganization(Request $request) {

        $this->validate($request, [
            'name' => 'bail|required',
            'parent_id' => 'bail|required|exists:'.(new Organization())->getTable().',id',
            'order' => 'bail|nullable|integer',
            'is_enable' => 'bail|boolean'
        ]);

        return $this->repository->addOrganization($request);
    }

    public function getOrganization(Request $request) {

        return $this->repository->getOrganization($request->input('id'), $request->input('is_recursion', 0));
    }

    /**
     * 修改组织架构信息
     * @param Request $request
     * @return mixed
     */
    public function setOrganization(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Organization())->getTable().',id',
            'name' => 'bail|required',
            'parent_id' => 'bail|required|exists:'.(new Organization())->getTable().',id',
            'order' => 'bail|nullable|integer',
            'is_enable' => 'bail|boolean'
        ]);
        return $this->repository->setOrganization($request);
    }

    public function deleteOrganization(Request $request) {

        $this->validate($request, [
            'id' => 'bail|required|integer|exists:'.(new Organization())->getTable().',id,deleted_at,NULL',
        ]);

        return $this->repository->deleteOrganization($request);
    }

}
