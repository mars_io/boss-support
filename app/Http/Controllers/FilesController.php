<?php
namespace App\Http\Controllers;

use App\Exceptions\AbortException;
use App\Files;
use Brexis\LaravelWorkflow\Traits\WorkflowTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Request;

class FilesController extends Controller {

    public function upload(Request $request) {

        $this->validate($request, [
            //file size:40MB | video size:100MB
            'file' => 'bail|required|max:40960|mimes:jpeg,bmp,gif,png,zip,rar,doc,docx,xls,xlsx,csv,txt,pdf,gz,tar,ppt,pptx',
            'video' => 'bail|max:102400|mimes:mp4,wmi,avi,swf,flv,rmvb,wmv,3gp,mov,rm,mpeg4,amv'
        ]);

        $file = $request->file('file');

        $info = [
            'bucket' => config('filesystems.disks.oss.bucket'),
            'host' => config('filesystems.disks.oss.endpoint'),
            'ext' => $file->getClientOriginalExtension(),
            'mime' => $file->getMimeType(),
            'size' => $file->getSize()
        ];
        $path = Storage::putFile('', $file);
        $res = Files::create([
            'name' => $path,
            'raw_name' => $file->getClientOriginalName(),
            'info' => \GuzzleHttp\json_encode($info),
            'user_id' => Auth::guard()->id(),
            'uri' => Storage::url($path)
        ]);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $res,
            ]);
        }
    }

    public function getImages(Request $request) {

        $res = Files::find(1);

        $workflow = \Workflow::get($res);
        $transitions = $workflow->getEnabledTransitions($res);
        $workflow->apply($res, 'to_review');
        $res->save();
        //$res->workflow_apply('to_review');
        dd($res);


        //dd($res);

        //dd($transitions);


        $this->validate($request, [
            'ids' => 'bail|required|json'
        ]);
        $ids = \GuzzleHttp\json_decode($request->input('ids'), true);

       $res = Files::whereIn('id', $ids)->get()->toArray();
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $res,
            ]);
        }
    }
}