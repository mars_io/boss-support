<?php

namespace App\Http\Middleware;

use Closure;

class CrossDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        $allow_origin = array(
            'http://test.v3.boss.houses.cn',
            'http://test.v3.mobile.boss.houses.cn',
            'http://localhost:8088',
            'http://localhost:8086',
            'http://192.168.8.132:8088',
            'http://192.168.8.132:8086',
            'http://192.168.0.109:8088',
            'http://192.168.0.115:8088',
            'http://192.168.0.103:8088',
            'http://v3.boss.houses.cn',
            'http://v3.mobile.boss.houses.cn',
            'http://wx.area.houses.cn',
            'http://calc.houses.cn',
            'http://192.168.0.100:8088',
            'http://192.168.0.106:8088',
            'http://192.168.1.119:8088',
            'http://192.168.1.155:8088',
            'http://192.168.1.251:8088',
            'http://192.168.20.59:8088'
        );

        if(in_array($origin, $allow_origin)){
            $response->header('Access-Control-Allow-Origin', $origin);
            $response->header('Access-Control-Allow-Credentials', 'true');
            $response->header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Env, Authorization');
            $response->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, OPTIONS, DELETE');
        }
        return $response;
    }
}
