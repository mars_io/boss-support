<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class FormatResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (str_is('horizon*', $request->path())) {
            return $response;
        }

        if ($request->route()->getPrefix()){

            $content = json_decode($response->getContent(), true);
            if (isset($content['exception'])) {
                return $response;
            }

            if ($content != null && !array_key_exists('data', $content)) {

                $data = [
                    'status' => 'fail',
                    'status_code' => 200,
                ];
                if (isset($content['message'])) {

                    if ($content['message'] == "Unauthenticated.") {
                        $data['status_code'] = 401;
                        $data['message'] = "您尚未登录，请前往登录";
                    }

                    if ($content['message'] == "The user credentials were incorrect.") {
                        $data['status_code'] = 401;
                        $data['message'] = "您的登录验证不合法，请重新登录，或者联系管理员";
                    }
                    $data['data'] = [];
                }

                if (isset($content['token_type'])) {
                    $data = [
                        'status' => 'success',
                        'status_code' => 200,
                    ];

                    $data['message'] = "鉴权成功，请登录";
                    $data['data'] = $content;
                }

                $response->setContent(json_encode($data));
            }
        }

        return $response;
    }
}
