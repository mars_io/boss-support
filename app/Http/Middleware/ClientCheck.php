<?php

namespace App\Http\Middleware;

use App\Exceptions\AbortException;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class ClientCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $whiteList = [
            '47.100.175.18',
            '172.19.25.85'
        ];

        if (getenv("APP_ENV") == "production") {
            if (in_array($request->getClientIp(), $whiteList) == false) {
                throw new AbortException("Illegal client.");
            }
        }

        //TODO: 增加客户端IP白名单
        return $next($request);
    }
}
