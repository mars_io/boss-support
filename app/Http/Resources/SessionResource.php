<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SessionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'avatar'    =>  $this->avatar,
            'is_enable' => $this->is_enable,
            'is_on_job' => $this->is_on_job,
            'org'    =>  $this->orgs()->get(),
            'role'  =>  $this->roles()->get(),
            'py'        =>  $this->py,
            'pinyin'    =>  $this->pinyin,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
        //return parent::toArray($request);
    }
}
