<?php

namespace App\Http\Resources;

use App\Models\Files;
use App\Models\Users;
use Illuminate\Http\Resources\Json\Resource;

class CommentsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'title'  => $this->title,
            'body'  => $this->body,
            'commentable_id' => $this->commentable_id,
            'user_id' => $this->creator_id,
            'user'  => new UsersResource($this->whenLoaded('user')),
            'album'  => $this->when($this->album, function() {
                return FilesResource::collection(Files::whereIn('id', json_decode($this->album, true))->get());
            }),
            'creator_id' => $this->creator_id,
            'created_at' => $this->created_at->format('m-d H:i'),
        ];
    }
}
