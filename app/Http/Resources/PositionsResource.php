<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PositionsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name'  => $this->name,
            'type'  => $this->type,
            'role'  =>  new RoleResource($this->whenLoaded('roles')),
            'position_type'   =>  new PositionTypesResource($this->whenLoaded('positiontypes')),
            'parent_id' => $this->parent_id,
            'lft' => $this->lft,
            'rgt' => $this->rgt,
            'depth' => $this->depth,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
    }
}
