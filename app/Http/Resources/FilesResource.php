<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FilesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name'  => $this->name,
            'raw_name'  => $this->raw_name,
            'display_name'  => $this->display_name,
            'info' => $this->info,
            'user_id' => $this->user_id,
            'uri' => $this->uri,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
    }
}
