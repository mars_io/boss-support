<?php

namespace App\Http\Resources;

use App\Models\Users;
use Illuminate\Http\Resources\Json\Resource;

class CustomersResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $renters = ContractRenterResource::collection($this->whenLoaded('contract_renters'));
        $lords = ContractLordResource::collection($this->whenLoaded('contract_lords'));
        $identity = "";
        /*if ($renters->isNotEmpty()) {
            $identity = $identity . "租客";
        }
        if ($lords->isNotEmpty()) {
            $identity = $identity . ",业主";
        }*/

        return [
            'id'    => $this->id,
            'name' => $this->name,
            'phone'  => $this->phone,
            'sex' => $this->sex,
            'idtype'    =>  $this->idtype,
            'idcard'    =>  $this->idcard,
            'is_corp'        =>  $this->is_corp,
            'is_agent' =>  $this->is_agent,
            'user_id'   => $this->user_id,
            'org_id'    =>  $this->org_id,
            'user'   => new UsersResource($this->whenLoaded('user')),
            'renters' => $renters,
            'lords'   => $lords,
            'identity' => trim($identity, ','),
            'py'    =>  $this->py,
            'pinyin'    =>  $this->pinyin,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
        //return parent::toArray($request);
    }
}
