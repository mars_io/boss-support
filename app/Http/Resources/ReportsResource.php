<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ReportsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'house_id' => $this->house_id,
            'user_id' => $this->user_id,
            'content' => $this->content,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
        //return parent::toArray($request);
    }
}
