<?php

namespace App\Http\Resources;

use App\Models\Organizations;
use App\Models\Users;
use Illuminate\Http\Resources\Json\Resource;

class OrganizationsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            //'ding_department_id' => $this->ding_department_id,
            'order' =>  $this->order,
            'parent_id' => $this->parent_id,
            //'lft' =>  $this->lft,
            //'rgt' =>  $this->rgt,
            'depth' =>  $this->depth,
            'leader_id' =>  $this->leader_id,
            'leader' => new UsersResource($this->whenLoaded('leader')),
            'is_leaf' => $this->isLeaf(),
            'top_org' =>  $this->when($this->showMarket, function(){
                $topOrgs = $this->getAncestorsAndSelfWithoutRoot();
                if ($topOrgs->isNotEmpty()) {
                    return $topOrgs[0];
                }
            }),
            'users' => $this->CountOrgUsers(),
            'ancestors' => $this->when($this->getLevel() === 5, function(){
                $node = $this->parent()->first();
                return ['level-third'=>$node, 'level-second' => $node->parent()->first()];
            }),
            'created_at' =>  $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' =>  $this->updated_at->format('Y-m-d H:i:s'),
            'deleted_at' =>  $this->deleted_at,
        ];

    }
}
