<?php

namespace App\Http\Resources;

use App\Models\Houses;
use App\Models\Users;
use Illuminate\Http\Resources\Json\Resource;

class ContractLordResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'    => $this->id,
            'contract_number'  => $this->contract_number,
            'house_id' => $this->house_id,
            'type' => $this->type,
            'status'    =>  $this->status,
            'first_pay_at'    =>  $this->first_pay_at,
            'second_pay_at'    =>  $this->second_pay_at,
            'sign_month'    =>  $this->sign_month,
            'sign_remainder_day'    =>  $this->sign_remainder_day,
            'sign_at'        =>  $this->sign_at,
            'start_at' =>  $this->start_at,
            'end_at'    =>  $this->end_at,
//            'end_real_at'    =>  $this->end_real_at,
//            'end_type'    =>  $this->end_type,
            'mortgage_price'    =>  $this->mortgage_price,
            'penalty_price'     => $this->penalty_price,
            'customers' => CustomersResource::collection($this->whenLoaded('customers')),
            'house' => new HousesResource($this->whenLoaded('house')),
            'ready_days'    =>  $this->ready_days,
            'vacancy_way'   =>  $this->vacancy_way,
            'vacancy_other' => $this->vacancy_other,
            'vacancy_end_date'  =>  $this->vacancy_end_date,
            'duration_days'    =>  $this->duration_days,
            'month_price'   => $this->month_price,
            'property_payer'    =>  $this->property_payer,
            'property_price'    =>  $this->property_price,
            'pay_way'    =>  $this->pay_way,
            'purchase_way'  =>  $this->purchase_way,
            'pay_account_info'    =>  $this->pay_account_info,
            'is_joint'    =>  $this->is_joint,
            'is_corp'    =>  $this->is_corp,
            'is_agency'    =>  $this->is_agency,
            'agency_info'    =>  $this->agency_info,

            'agency'        => new AgencyResource($this->whenLoaded('agency_contract')),

            'guarantee_days'    =>  $this->guarantee_days,
            'album'    =>  $this->album,
            'remark'    =>  $this->remark,
            'doc_status'    =>  $this->doc_status,
            'visit_status'  =>  $this->visit_status,
            'contract_operation' =>  $this->when($this->contract_operation, function(){
                return $this->contract_operation;
            }),
            'visit_operation' =>  $this->when($this->visit_operation, function(){
                return $this->visit_operation;
            }),
            'user_id'    =>  $this->user_id,
            'user'  => new UsersResource($this->whenLoaded('user')),
            'sign_user_id'    =>  $this->sign_user_id,
            'sign_user'     => new UsersResource($this->whenLoaded('sign_user')),
            'sign_org_id'    =>  $this->sign_org_id,
            'sign_org'      => new OrganizationsResource($this->whenLoaded('sign_org')),
            'org_id'    =>  $this->org_id,
            'remark_clause' => $this->remark_clause,
            'generate_from' => $this->generate_from,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
        //return parent::toArray($request);
    }
}
