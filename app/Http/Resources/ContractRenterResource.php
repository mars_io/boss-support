<?php

namespace App\Http\Resources;

use App\Models\Users;
use Illuminate\Http\Resources\Json\Resource;

class ContractRenterResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'contract_number' => $this->contract_number,
            //'rentable_id'  => $this->rentable_id,
            'house_id'  => $this->rentable_id,
            //'rentable_type' => $this->rentable_type,
            'type'    =>  $this->type,
            'status'    =>  $this->status,

            'sign_month'    =>  $this->sign_month,
            'sign_remainder_day'    =>  $this->sign_remainder_day,
            'sign_at'   =>  $this->sign_at,
            'start_at'    =>  $this->start_at,
            'end_at'        =>  $this->end_at,
//            'end_real_at' =>  $this->end_real_at,
//            'end_type'    =>  $this->end_type,
            'customers' => CustomersResource::collection($this->whenLoaded('customers')),
            'house' => new HousesResource($this->whenLoaded('rentable')),

            'duration_days'    =>  $this->duration_days,
            'month_price'    =>  $this->month_price,
            'mortgage_price'    =>  $this->mortgage_price,
            'penalty_price'     => $this->penalty_price,
            'purchase_way'      =>  $this->purchase_way,
            'pay_way'    =>  $this->pay_way,
            'pay_account_info'    =>  $this->pay_account_info,
            'is_joint'    =>  $this->is_joint,
            'is_corp'    =>  $this->is_corp,
            'is_agency'    =>  $this->is_agency,
            'agency_info'    =>  $this->agency_info,

            'agency'        => new AgencyResource($this->whenLoaded('agency_contract')),

            'final_payment_at'  =>  $this->final_payment_at,
            'receipt_number'    =>  $this->receipt_number,
            'property_payer'    =>  $this->property_payer,
            'property_price'    =>  $this->property_price,
            'album'    =>  $this->album,
            'money_sum' =>  $this->money_sum,
            'money_table'   =>  $this->money_table,
            'remark'    =>  $this->remark,
            'doc_status'    =>  $this->doc_status,
            'visit_status'  =>  $this->visit_status,
            'contract_operation' =>  $this->when($this->contract_operation, function(){
                return $this->contract_operation;
            }),
            'visit_operation' =>  $this->when($this->visit_operation, function(){
                return $this->visit_operation;
            }),
            'user_id'    =>  $this->user_id,
            'user'  => new UsersResource($this->whenLoaded('user')),
            'sign_user_id'    =>  $this->sign_user_id,
            'sign_user' => new UsersResource($this->whenLoaded('sign_user')),
            'sign_org_id'    =>  $this->sign_org_id,
            'sign_org'      => new OrganizationsResource($this->whenLoaded('sign_org')),
            'org_id'    =>  $this->org_id,
            'remark_clause' => $this->remark_clause,
            'generate_from' =>  $this->generate_from,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
        //return parent::toArray($request);
    }
}
