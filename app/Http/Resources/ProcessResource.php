<?php

namespace App\Http\Resources;

use App\Models\Users;
use Illuminate\Http\Resources\Json\Resource;

class ProcessResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'    => $this->id,
            'user_id' => $this->user_id,
            'house_id'  =>  $this->house_id,
            'house' => new HousesResource($this->whenLoaded('house')),
            'user' => new UsersResource($this->whenLoaded('user')),
            'content' => $this->content,
            'processable_id' => $this->processable_id,
            'processable_type'  => $this->processable_type,
            //'process_flow'  => $this->getTasks(),
            'place'     => $this->getPlace(),
            'finish_at' => $this->finish_at,
            'created_at' => $this->created_at->format('m-d H:i'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
        //return parent::toArray($request);
    }
}
