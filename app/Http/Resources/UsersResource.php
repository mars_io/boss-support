<?php

namespace App\Http\Resources;

use App\Models\Role;
use App\Models\Users;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Cache;

class UsersResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'    => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'ding_user_id' => $this->ding_user_id,
            'avatar'    =>  $this->avatar,
            'is_enable' => $this->is_enable,
            'is_on_job' => $this->is_on_job,
            /*'org'    =>  Cache::rememberForever("boss_user_org_{$this->id}", function () {
                return OrganizationsResource::collection($this->whenLoaded('orgs'));
            }),*/
            /*'role'    =>  Cache::rememberForever("boss_user_role_{$this->id}", function () {
                return RoleResource::collection($this->whenLoaded('roles'));
            }),*/
            'org'    => OrganizationsResource::collection($this->whenLoaded('orgs')),
            'role'    =>  RoleResource::collection($this->whenLoaded('roles')),
            'code'  => $this->when($this->code, $this->code),
            'py'        =>  $this->py,
            'pinyin'    =>  $this->pinyin,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];

        //return parent::toArray($request);
    }
}
