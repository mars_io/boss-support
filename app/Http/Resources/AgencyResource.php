<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AgencyResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'    => $this->id,
            'house_id' => $this->house_id,
            'contract_id'  => $this->contract_able_id,
            'agency_price' => $this->agency_price,
            'agency_before_price'    =>  $this->agency_before_price,
            'agency_name'    =>  $this->agency_name,
            'agency_username'        =>  $this->agency_username,
            'agency_phone' =>  $this->agency_phone,
            'agency_account_info'   => $this->agency_account_info,
            'album'   => $this->album,
            'remark' => $this->remark,
            'user_id'   => $this->user_id,
            'sign_user_id' => $this->sign_user_id,
            'sign_org_id'    =>  $this->sign_org_id,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
        //return parent::toArray($request);
    }
}
