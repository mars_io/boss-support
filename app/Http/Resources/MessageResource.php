<?php

namespace App\Http\Resources;

use App\Models\Users;
use Illuminate\Http\Resources\Json\Resource;

class MessageResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id'  => $this->notifiable_id,
            //'user'  => new UsersResource(Users::find($this->notifiable_id)),
            //'type'      =>  $this->type,
            //'is_alert'  =>  $this->is_alert,
            //'raw'       =>  $this->raw,
            'content'  => $this->data,
            'read_at' => $this->read_at,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
    }
}
