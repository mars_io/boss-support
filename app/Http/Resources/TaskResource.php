<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TaskResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'flow_id' => $this->flow_id,
            'flow'  => new ProcessResource($this->whenLoaded('process')),
            'user_id' => $this->user_id,
            'user' => new UsersResource($this->whenLoaded('user')),
            'house_id'  =>  $this->house_id,
            'title'    =>  $this->title,
            'finish_at'    =>  $this->finish_at,
            'is_cc'        =>  $this->is_cc,
            'read_at'    =>  $this->read_at,
            'created_at' => $this->created_at->format('m-d H:i'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
        //return parent::toArray($request);
    }
}
