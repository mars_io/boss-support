<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class HousesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'    => $this->id,
            'name' => $this->name,
            'building'  => $this->building,
            'unit' => $this->unit,
            'house_number'    =>  $this->house_number,
            'village_name'    =>  $this->village_name,
            'village_id'        =>  $this->village_id,
            'room' => $this->room,
            'hall'  => $this->hall,
            'area'  =>  $this->area,
            'floor'  =>  $this->floor,
            'floors'  =>  $this->floors,
            'toilet'    => $this->toilet,
            'decoration'    => $this->decoration,
            'house_identity'    => $this->house_identity,
            'rent_type'    => $this->rent_type,
            'house_state'   => $this->house_state,
            'house_grade'    => $this->house_grade,
            'house_feature'    => $this->house_feature,
            'suggest_price'    => $this->suggest_price,
//            'rent_start_at'    => $this->rent_start_at,
//            'ready_end_at'    => $this->ready_end_at,
//            'again_rent_at'    => $this->again_rent_at,
//            'lord_end_at'    => $this->lord_end_at,
//            'warning_init_at'    => $this->warning_init_at,

            'rent_end_than_days'    => $this->rent_end_than_days,

            'total_ready_days'   =>  $this->total_ready_days,
            'current_ready_days'   =>  $this->current_ready_days,
            'is_again_rent'          =>  $this->is_again_rent,
            'lord_remainder_days'   =>  $this->lord_remainder_days,
            'status'   =>  $this->status,
            'warning_status'   =>  $this->warning_status,
            'user'  =>  new UsersResource($this->whenLoaded('user')),
            'org'  =>  new OrganizationsResource($this->whenLoaded('org')),
            'py'    =>  $this->py,
            'pinyin'    =>  $this->pinyin,
            'album' =>  $this->album,
            'house_res' =>  $this->house_res,
            'rooms' => RoomsResource::collection($this->whenLoaded('rooms')),
            'lords' => ContractLordResource::collection($this->whenLoaded('contract_lords')),
            'renters' => ContractRenterResource::collection($this->whenLoaded('contract_renters')),
            'is_nrcy'   => $this->is_nrcy,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
        //return parent::toArray($request);
    }
}
