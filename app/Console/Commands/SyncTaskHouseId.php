<?php

namespace App\Console\Commands;

use App\Models\Process;
use App\Models\Tasks;
use Illuminate\Console\Command;

class SyncTaskHouseId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task_house_id:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步Task中的House_id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Process::chunk(10, function ($processes) {
            foreach ($processes as $process) {
                Tasks::where('flow_id', $process->id)
                    ->update([
                        'house_id' => $process->house_id
                    ]);
                $this->info("Process({$process->id}) 关联的Task中的house_id同步成功");
            }
        });
    }
}
