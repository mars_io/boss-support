<?php

namespace App\Console\Commands;

use App\Models\ContractRenter;
use App\Models\Users;
use EasyDingTalk\Application;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class SyncDing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ding:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步钉钉信息';

    protected $options;



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function setDingAuth() {
        $this->options = [
            'corp_id' => config('services.dingtalk.corp_id'),
            'corp_secret' => config('services.dingtalk.corp_secret'),
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setDingAuth();
        $users = Users::whereNull('is_on_job')->whereNull('is_enable')->get();

        $app = new Application($this->options);
        $dingUser = $app->user;

        foreach ($users as $user) {


            if ($user->ding_user_id) {
                try{
                    foreach ($user->roles as $role) {
                        if ($role->position->name) {
                            $pos = $role->position->name;
                            break;
                        }
                    }
                    $a = $dingUser->update([
                        'userid' => $user->ding_user_id,
                        'position'  => $pos ? $pos : $user->roles[0]->display_name
                    ]);
                    if ($a['errmsg'] == 'ok') {
                        $this->info("{$user->name} 资料同步成功");
                    }

                }catch(\Exception $e) {
                    continue;
                }
            }
        }
    }
}
