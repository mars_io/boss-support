<?php

namespace App\Console\Commands;

use App\Models\ContractRenter;
use App\Models\Houses;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncHouseStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'house:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步房屋状态';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        #########  更新已出租 ##################
        //查询已出租的房屋
        $collect = DB::table('contract_lord')
            ->whereNull('end_real_at')
            ->whereNotNull('contract_number')
            ->whereExists(function ($query){
                $query->select(DB::raw(1))
                    ->from("contract_renter")
                    ->whereNull("end_real_at")
                    ->whereNotNull("contract_number")
                    ->whereRaw("contract_lord.house_id = contract_renter.rentable_id");
            })->get();

        foreach ($collect as $c) {
            $rent = ContractRenter::where('rentable_id', $c->house_id)
                ->whereNull('end_real_at')
                ->whereNotNull("contract_number")
                ->orderBy('id', 'desc')
                ->first();
            if ($rent) {
                $house = Houses::find($c->house_id);
                if ($house) {
                    $house->update(['rent_start_at' => $rent->start_at]);
                    $this->info("{$house->name}: 已更新房屋状态");
                }
            }
        }

        ##########  更新未出租  ###################

        $collect = DB::table('contract_lord')
            ->whereNull('end_real_at')
            ->whereNotNull('contract_number')
            ->whereNotExists(function ($query){
                $query->select(DB::raw(1))
                    ->from("contract_renter")
                    ->whereNull("end_real_at")
                    ->whereNotNull("contract_number")
                    ->whereRaw("contract_lord.house_id = contract_renter.rentable_id");
            })->get();

        foreach ($collect as $c) {
            $house = Houses::find($c->house_id);
            if ($house) {
                $house->update(['lord_start_at' => $c->start_at, 'rent_start_at' => NULL]);
                $this->info("{$house->name}: 已更新房屋状态");
            }
        }

    }
}
