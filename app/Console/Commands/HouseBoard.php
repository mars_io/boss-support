<?php

namespace App\Console\Commands;

use App\Models\ContractLord;
use App\Models\ContractRenter;
use App\Models\Process;
use App\Models\Tasks;
use App\Models\Users;
use App\Models\Houses;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;


class HouseBoard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'house:board
                              {list=empty : 列出符合条件的房屋 empty:空置房源，rented:已出租房源，lorded:正在运营的房源，expired:已过期的房源}

                            {--all : 按当前组织架构显示对应的房屋状态分布}
                            {--org= : 数据筛选基于指定部门}
                            {--begin= : 数据筛选指定的开始时间}
                            {--end= : 数据筛选指定的结束时间}
                            {--over= : 尚未出租且超出了指定的天数}
                            {--excel= : 将当前结果导出成Excel}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '房屋相关的数据概览';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arg = $this->argument('list');

        $lord = ContractLord::with([
            "house",
            "customers",
            "user",
            "sign_user",
            "sign_org",
            "agency_contract"
        ]);

        $rent = ContractRenter::with([
            "rentable",
            "customers",
            "user",
            "sign_user",
            "sign_org",
            "agency_contract"
        ]);

        $query = NULL;
        $collect = NULL;

        switch ($arg) {
            case "empty":
                $query = DB::table('contract_lord')->whereNull('end_real_at')
                    ->whereNotNull("contract_number")
                    ->whereExists(function ($q) {
                        $q->select(DB::raw(1))->from('contract_renter')
                            ->whereNull("end_real_at")
                            ->whereNotNull("contract_number")
                            ->whereRaw('contract_lord.house_id = contract_renter.rentable_id');
                    })->count();

                break;
            case "rented":
                //查询正在出租的房子(所有未退租的房子，包含合同到期但是未进行退租的房子）
                $query = $rent->whereNull("end_real_at")
                    ->whereNotNull("contract_number");
                break;

            case "lorded":
                $query = $lord->whereNull('end_real_at')
                    ->whereNotNull("contract_number");
                break;

            case "expired":

                break;

            default:
                break;
        }

        //查询指定开始时间到当前时间的范围
        if ($this->option('begin')) {
            $query->whereDate('begin_at', '>=', $this->option('begin'));
        }

        //查询之前到指定结束时间范围
        if ($this->option('end')) {
            $query->whereDate('end_at', '<=', $this->option('end'));
        }

        //查询大于指定过期天数的房屋
        if ($this->option('over')) {

        }

        //查询所有的房源按组织架构分布
        if ($this->option('all')) {
            $collect = $query->get(["contract_number", "end_real_at", "end_at"]);
        }

        //查询指定部门下的房源，按当前部门下的组织架构分布
        if ($this->option('org')) {

        }

        //将查询结果导出成excel
        if ($this->option('excel')) {

        }

        $header = ['合同编号', '实际结束', '结束时间'];

        $co = $collect->toArray();
        foreach ($co as $key => $value) {
            foreach ($value as $k => $v) {
                if ($v == false) {
                    $co[$key][$k] = "";
                }
            }
        }
        $this->table($header, $co);
    }
}
