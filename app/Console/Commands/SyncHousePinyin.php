<?php

namespace App\Console\Commands;

use App\Models\Houses;
use Illuminate\Console\Command;

class SyncHousePinyin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'house_pinyin:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步房屋表拼音';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Houses::chunk(100, function($houses) {
            foreach ($houses as $house) {
                $door_number = [];
                if ($house->building) {
                    array_push($door_number, $house->building);
                }
                if ($house->unit) {
                    array_push($door_number, $house->unit);
                }
                if ($house->house_number) {
                    array_push($door_number, $house->house_number);
                }
                $door_number = trim(implode('-', $door_number), '-');
                if ($house->village_name) {
                    $py = pinyin_abbr($house->village_name) . $door_number;
                    $pinyin = str_replace(' ','', pinyin_sentence($house->village_name)) . $door_number;
                } else {
                    if ($house->name) {
                        $nameInfo = explode('-', $house->name);
                        if (isset($nameInfo[1])) {
                            $py = $nameInfo[0] . '-' . pinyin_abbr($nameInfo[1]);
                            $pinyin = $nameInfo[0] . '-' . str_replace(' ','', pinyin_sentence($nameInfo[1]));
                        }
                    }
                }
                $house->pinyin = $pinyin;
                $house->py = $py;
                $res = $house->save();
                if ($res) {
                    $this->info("{$house->name} 拼音同步完成. 全拼: {$pinyin}, 简拼: {$py}");
                }
            }
        });
    }
}
