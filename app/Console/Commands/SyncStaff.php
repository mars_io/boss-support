<?php

namespace App\Console\Commands;

use App\Models\Organizations;
use App\Models\Positions;
use App\Models\Role;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncStaff extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'staff:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '从Boss2同步组织架构中的部门，职位，人员';


    public $orgRoleMap = [
        '9' =>  [
            '业务人员'  =>  [
                '市场专员'  =>  [
                    'market-marketing-officer',
                    [325,328,484,487,258,113,108,256,481,54,334,331,322,495,139,337,498,137,236,501,191,409,456,504,310,171,316,187,158,185,244,143,145,147,269,507,510,357,297,294,291,307,304,403,406,339,547,125,128,151]
                ],
                '储备片区经理'  =>  [
                    'market-reserve-manager',
                    [326,329,483,486,489,282,284,281,283,480,286,285,335,332,323,494,287,338,497,500,252,408,455,503,311,435,317,434,511,288,433,436,437,268,506,509,356,298,295,292,308,305,402,405,546]
                ],
                '片区经理'  =>  [
                    'market-marketing-manager',
                    [324,327,482,485,488,24,112,107,116,479,228,109,333,330,321,493,138,336,496,136,235,499,190,407,454,502,309,169,315,186,157,184,243,142,144,146,267,505,508,355,296,293,290,306,303,401,404,544]
                ],
                '储备区域经理'  =>  [
                    'market-reserve-regional',
                    [491,492]
                ],
                '区域经理'  =>  [
                    'market-marketing-regional',
                    [289,302,122,477,129,132,141,270]
                ],
                '城市总'  =>  [
                    'market-marketing-city',
                    [478]
                ]
            ]
        ],
        '2' => [
            '物资采购人员'  =>  [
                '物资采购专员'  =>  [
                    'purchase-officer',
                    [388]
                ],
                '物资采购主管'  =>  [
                    'purchase-master',
                    [387]
                ],
                '物资采购经理'  =>  [
                    'purchase-manager',
                    [386]
                ]
            ],
            '行政人员'  =>  [
                '行政专员'  =>  [
                    'administration-officer',
                    [391]
                ],
                '行政主管'  =>  [
                    'administration-master',
                    [390]
                ],
                '行政经理'  =>  [
                    'administration-manager',
                    [389]
                ]
            ],
            '招聘人员'  =>  [
                '招聘专员'  =>  [
                    'recruit-officer',
                    [376]
                ],
                '招聘主管'  =>  [
                    'recruit-master',
                    [375]
                ],
                '招聘经理'  =>  [
                    'recruit-manager',
                    [374]
                ],
            ],
            '薪资绩效人员'  =>  [
                '薪资绩效专员'  =>  [
                    'salary-performance-officer',
                    [379]
                ],
                '薪资绩效主管'  =>  [
                    'salary-performance-master',
                    [378]
                ],
                '薪资绩效经理'  =>  [
                    'salary-performance-manager',
                    [377]
                ],
            ],
            '基础人事人员'  =>  [
                '基础人事专员'  =>  [
                    'basic-personnel-officer',
                    [382]
                ],
                '基础人事主管'  =>  [
                    'basic-personnel-master',
                    [381]
                ],
                '基础人事经理'  =>  [
                    'basic-personnel-manager',
                    [380]
                ],
            ],
            '员工关系人员'  =>  [
                '员工关系专员'  =>  [
                    'employee-relationship-officer',
                    [385]
                ],
                '员工关系主管'  =>  [
                    'employee-relationship-master',
                    [384]
                ],
                '员工关系经理'  =>  [
                    'employee-relationships-manager',
                    [383]
                ],
            ],
            '人力资源人员'  =>  [
                '总监助理'  =>  [
                    'hr-assistant-director',
                    [42]
                ],
                '人事总监'  =>  [
                    'hr-director',
                    [41,199]
                ]
            ],
            '资料线上人员'  =>  [
                '资料线上专员'  =>  [
                    'data-online-officer',
                    [400]
                ],
                '资料线上主管'  =>  [
                    'data-online-master',
                    [399]
                ],
                '资料线上经理'  =>  [
                    'data-online-manager',
                    [398]
                ],
            ],
            '客服信息录入人员'  =>  [
                '客服信息录入专员'  =>  [
                    'customer-information-input-officer',
                    [392]
                ],
                '客户信息录入主管'  =>  [
                    'customer-information-input-master',
                    [393]
                ],
                '客户信息录入经理'  =>  [
                    'customer-information-input-manager',
                    [394]
                ],
            ],
            '合同管理人员'  =>  [
                '合同管理专员'  =>  [
                    'contract-administration-officer',
                    [397]
                ],
                '合同管理主管'  =>  [
                    'contract-administration-master',
                    [396]
                ],
                '合同管理经理'  =>  [
                    'contract-administration-manager',
                    [395]
                ],
            ],
            '资料上传跟踪人员'  =>  [
                '资料上传跟踪专员'  =>  [
                    'upload-tracking-officer',
                    [453]
                ],
                '资料上传跟踪主管'  =>  [
                    'upload-tracking-master',
                    [452]
                ],
                '资料上传跟踪经理'  =>  [
                    'upload-tracking-manager',
                    [451]
                ]
            ]
        ],
        '4'  =>  [
            '财务人员'  =>  [
                '财务备用'  =>  [
                    'finance-spare',
                    [229]
                ],
                '财务专员'  =>  [
                    'finance-officer',
                    [367,543]
                ],
                '财务助理'  =>  [
                    'finance-assistant-master',
                    [366]
                ],
                '财务主管'  =>  [
                    'finance-master',
                    [362]
                ],
                '财务经理'  =>  [
                    'finance-manager',
                    [219]
                ],
                '财务总监'  =>  [
                    'finance-director',
                    [46]
                ]
            ],
            '核算组'   =>  [
                '财务专员'  =>  [
                    'verify-officer',
                    [364,518,520,524,526]
                ],
                '财务核算助理'  =>  [
                    'verify-assistant-master',
                    [363]
                ],
                '应收入账主管'    => [
                    'accounts-payable-master',
                    [525]
                ],
                '押金中介主管'    => [
                    'deposit-intermediary-master',
                    [523]
                ],
                '房租、退款主管' => [
                    'rent-refund-master',
                    [519]
                ],
                '报销组主管' => [
                    'reimbursement-master',
                    [517]
                ],
                '财务核算经理'  =>  [
                    'verify-manager',
                    [360]
                ]
            ],
            '财务资金部'     => [
                '财务专员'  =>  [
                    'fund-officer',
                    [367]
                ],
                '资金部助理'  =>  [
                    'fund-assistant-master',
                    [366]
                ],
                '资金部主管' => [
                    'fund-master',
                    [362]
                ],
                '资金部经理'  =>  [
                    'fund-manager',
                    [361]
                ]
            ],
            '会计部' => [
                '会计'  =>  [
                    'accounting-officer',
                    [529]
                ],
                '出纳'  =>  [
                    'accounting-assistant-master',
                    [530,533]
                ],
                '南京会计主管' => [
                    'accounting-master',
                    [528]
                ],
                '财务会计经理'  =>  [
                    'accounting-manager',
                    [358]
                ]
            ],
        ],

        '10'  =>  [
            '研发人员'  =>  [
                'PHP工程师专员'  =>  [
                    'development-php-officer',
                    [206]
                ],
                'PHP工程师主管'  =>  [
                    'development-php-master',
                    [205]
                ],
                'PHP工程师经理'  =>  [
                    'development-php-manager',
                    [34]
                ],
                '前端工程师专员'  =>  [
                    'development-front-officer',
                    [208]
                ],
                '前端工程师主管'  =>  [
                    'development-front-master',
                    [207]
                ],
                '前端工程师经理'  =>  [
                    'development-front-manager',
                    [35]
                ],
                '产品专员'  =>  [
                    'development-product-officer',
                    [210]
                ],
                '产品主管'  =>  [
                    'development-product-master',
                    [209]
                ],
                '产品经理助理'  =>  [
                    'development-product-assistants',
                    [250]
                ],
                '产品经理'  =>  [
                    'development-product-manager',
                    [36]
                ],
                'UI专员'  =>  [
                    'development-ui-officer',
                    [217]
                ],
                'UI主管'  =>  [
                    'development-ui-master',
                    [216]
                ],
                'UI经理'  =>  [
                    'development-ui-manager',
                    [215]
                ],
                '测试'  =>  [
                    'development-test',
                    [218]
                ]
            ]
        ],
        '90'  =>  [
            '事业人员'  =>  [
                '前台领班'  =>  [
                    'cause-duty-foreman',
                    [216]
                ],
                '酒店员工'  =>  [
                    'cause-hotel-staff',
                    [215]
                ],
                '事业经理'  =>  [
                    'cause-manager',
                    [218]
                ]
            ]
        ],
        '148'  =>  [
            'House大学'  =>  [
                '讲师助理'  =>  [
                    'university-lecturer-assistants',
                    [240]
                ],
                '董事长助理'  =>  [
                    'university-chairman-assistants',
                    [259]
                ],
                'CMO（讲师）'  =>  [
                    'university-cmo',
                    [247]
                ],
                'HouseCEO（讲师）'  =>  [
                    'university-development',
                    [249]
                ],
                'CTO（副校长）'  =>  [
                    'university-cto',
                    [238]
                ],
                'CIO(校长)'  =>  [
                    'university-cio',
                    [237]
                ],
                'CEO（讲师）'  =>  [
                    'university-ceo',
                    [239]
                ],
            ]
        ],
        '148'  =>  [
            '总裁办'  =>  [
                '董事长助理'  =>  [
                    'president-chairman-assistant',
                    [261]
                ],
                'CMO（讲师）'  =>  [
                    'president-cmo',
                    [264]
                ],
                '研发部'  =>  [
                    'president-development',
                    [265]
                ],
                'CTO（副校长）'  =>  [
                    'president-cto',
                    [263]
                ],
                'CIO(校长)'  =>  [
                    'president-cio',
                    [262]
                ],
                'CEO（讲师）'  =>  [
                    'president-ceo',
                    [260]
                ],
            ]
        ],
        '164'  =>  [
            '新媒体运营人员'  =>  [
                '平面设计专员'  =>  [
                    'new-media-designer-officer',
                    [370]
                ],
                '新媒体专员'  =>  [
                    'new-media-officer',
                    [280]
                ],
                '新媒体主管'  =>  [
                    'new-media-master',
                    [279]
                ],
                '新媒体经理'  =>  [
                    'new-media-manager',
                    [278]
                ],
                '新媒体总监'  =>  [
                    'new-media-director',
                    [277]
                ],
            ]
        ],
        '233'  =>  [
            '综合人员'  =>  [
                '综合专员'  =>  [
                    'comprehensive-officer',
                    [472]
                ],
                '综合主管'  =>  [
                    'comprehensive-master',
                    [471]
                ],
                '综合经理'  =>  [
                    'control-comprehensive-manager',
                    [470]
                ],
            ],
            '评审人员'  =>  [
                '评审专员'  =>  [
                    'appraiser-officer',
                    [469]
                ],
                '评审主管'  =>  [
                    'appraiser-master',
                    [468]
                ],
                '评审经理'  =>  [
                    'appraiser-manager',
                    [467]
                ],
            ],
            '信息人员'  =>  [
                '信息专员'  =>  [
                    'information-officer',
                    [466]
                ],
                '信息主管'  =>  [
                    'information-master',
                    [465]
                ],
                '信息经理'  =>  [
                    'information-manager',
                    [464]
                ],
            ],
            '巡视人员'  =>  [
                '巡视专员'  =>  [
                    'patrol-officer',
                    [463]
                ],
                '巡视主管'  =>  [
                    'patrol-master',
                    [462]
                ],
                '巡视经理'  =>  [
                    'patrol-manager',
                    [461]
                ],
            ],
            '产品管控人员'  =>  [
                '产品管控中心专员'  =>  [
                    'control-officer',
                    [515]
                ],
                '产品管控中心主管'  =>  [
                    'control-master',
                    [514]
                ],
                '产品管控中心经理'  =>  [
                    'control-manager',
                    [513]
                ],
                '产品管控中心总监'  =>  [
                    'control-director',
                    [512]
                ],
            ]
        ],
        '88'  =>  [
            '电话客服人员'  =>  [
                '电话客服专员'  =>  [
                    'tel-officer',
                    [416]
                ],
                '电话客服主管'  =>  [
                    'tel-master',
                    [414]
                ],
            ],
            '在线客服人员'  =>  [
                '在线客服专员'  =>  [
                    'online-officer',
                    [418]
                ],
                '在线客服主管'  =>  [
                    'online-master',
                    [417]
                ],
            ],
            '后台处理人员'  =>  [
                '后台处理专员'  =>  [
                    'processing-officer',
                    [420]
                ],
                '后台处理主管'  =>  [
                    'processing-master',
                    [419]
                ],
            ],
            '回访人员'  =>  [
                '收房回访专员'  =>  [
                    'collect-visit-officer',
                    [422]
                ],
                '收房回访主管'  =>  [
                    'collect-visit-master',
                    [421]
                ],
                '租房回访专员'  =>  [
                    'rent-visit-officer',
                    [424]
                ],
                '租房回访主管'  =>  [
                    'rent-visit-master',
                    [423]
                ],
            ],
            '结算人员'  =>  [
                '退租结算员'  =>  [
                    'settlement-retiring-officer',
                    [431]
                ],
                '结算专员'  =>  [
                    'settlement-officer',
                    [426]
                ],
                '结算主管'  =>  [
                    'settlement-master',
                    [425]
                ],
            ],
            '运维人员'  =>  [
                '运维专员'  =>  [
                    'operation-officer',
                    [428]
                ],
                '运维主管'  =>  [
                    'operation-master',
                    [427]
                ],
            ],
            '质检人员'  =>  [
                '质检专员'  =>  [
                    'quality-testing-officer',
                    [429]
                ],
                '质检主管'  =>  [
                    'quality-testing-master',
                    [430]
                ],
            ],
            '客服人员'  =>  [
                '客服经理'  =>  [
                    'customer-manager',
                    [50]
                ],
                '客服总监'  =>  [
                    'customer-director',
                    [49]
                ]
            ]
        ]

    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 同步职位和角色
     *
     * @param $b3
     * @param $org
     * @param $orgId
     */
    public function syncPosition($b3, $org, $orgId) {
        foreach ($this->orgRoleMap as $key => $value) {
            if ((int)$org->id === (int)$key) {
                foreach ($value as $k=>$v) {
                    $postion_type_id = $b3->table('position_type')->insertGetId([
                        'name'  =>  $k,
                        'org_id'    => $orgId,
                        'created_at'    =>  Carbon::now(),
                        'updated_at'    =>  Carbon::now()
                    ]);

                    $pos_id = NULL;
                    foreach ($v as $posName => $posInfo) {
                        $pos_id = $b3->table('positions')->insertGetId([
                            'name' => $posName,
                            'type' => $postion_type_id,
                            'parent_id' => $pos_id,
                            'created_at'    =>  Carbon::now(),
                            'updated_at'    =>  Carbon::now()
                        ]);

                        $role_id = $b3->table('roles')->insertGetId([
                            'name'  =>  $posInfo[0],
                            'display_name'  =>  $posName,
                            'description'  =>  $posName,
                            'position_id'   =>  $pos_id,
                            'created_at'    =>  Carbon::now(),
                            'updated_at'    =>  Carbon::now()
                        ]);

                    }
                }

            }
        }
    }

    /**
     * 返回角色id
     * @param $pos_id
     * @return mixed
     */
    public function associateRole($pos_id) {
        foreach ($this->orgRoleMap as $key => $value) {
            foreach ($value as $k=>$v) {
                foreach ($v as $posName => $posInfo) {
                    if (in_array($pos_id, $posInfo[1])) {
                        $role = Role::where('name', $posInfo[0])->first();
                        if ($role) {
                            $roleId = $role->id;
                            return $roleId;
                        }
                        continue;


                    }
                }
            }
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //初始化Boss2，Boss3的数据库连接
        $b2 = DB::connection('b2');

        $b3 = DB::connection('mysql');

        //初始化清空
        $b3->table('users')->truncate();
        $b3->table('organizations')->truncate();
        //$b3->table('permissions')->truncate();
        //$b3->table('permission_role')->truncate();
        //$b3->table('permission_user')->truncate();
        //$b3->table('roles')->truncate();
        //$b3->table('role_user')->truncate();
        $b3->table('org_user')->truncate();
        $b3->table('positions')->truncate();
        $b3->table('position_type')->truncate();


        //TODO: 生成系统，模块，权限
        /**
         * 租赁管理[整租管理，合租管理]
         *
         * 财务账本[基础管理， 款项管理， 收支流水， 数据统计]
         *
         * 微信管理[微信报销， 微信投诉， 预约看房， 房东加盟， 一键反馈]
         *
         * 人资管理[]
         *
         * OA办公[资产管理， 文章管理， 审批管理， 办公管理， 纸质资料]
         *
         * House大学[House大学， 在线考试]
         *
         * 业绩管理[周期表， 业绩， 工资]
         *
         * 系统设置[短信模板， 登录日志， 用户字典， 角色管理， 权限管理， 小区管理]
         *
         *
         */

        //SELECT * from `position`  where `delete_time` IS NULL  GROUP BY `vocation`
        //1, 同步最新的部门
        $orgs = $b2->select("select * from department where status=1 and delete_time is NULL order by id asc");

        foreach ($orgs as $org) {

            //同步部门
            $orgId = $b3->table('organizations')->insertGetId([
                'id'    => $org->id,
                'name'  =>  $org->name,
                'ding_department_id'  =>  $org->department_id,
                'order'  =>  $org->sort,
                'is_enable'  =>  $org->status,
                'parent_id'  =>  (int)$org->parent_id === 0 ? NULL : $org->parent_id,
                'py'  =>  pinyin_abbr($org->name),
                'pinyin'  =>  str_replace(' ','', pinyin_sentence($org->name)),
                'created_at'    =>  $org->create_time,
                'updated_at'    =>  $org->update_time,
            ]);

            //TODO: 同步该一级部门职位和角色
            if ((int)$org->parent_id === 1) {
                $this->syncPosition($b3, $org, $orgId);
            }

            //TODO: 关联角色和权限之间的关系

            //获取该部门的所有员工
            $orgUsers = $b2->select("select * from staff where department={$org->id} and delete_time is NULL");
            foreach ($orgUsers as $orgUser) {

                //如果新表里存在该用户手机号，则略过该用户（用于House用户的系统测试）
                $existUser = $b3->table('users')->select('id')->where('phone', $orgUser->mobile)->first();
                if ($existUser) {
                    continue;
                }
                //1: 在职， 2: 禁用， 3：离职

                if ($orgUser->status === 1) {
                    $is_enable = NULL;
                    $is_on_job = NULL;
                }

                if ($orgUser->status === 2) {
                    $is_enable = Carbon::now();
                    $is_on_job = NULL;
                }

                if ($orgUser->status === 3) {
                    $is_enable = NULL;
                    $is_on_job = Carbon::now();
                }



                $userId = $b3->table('users')->insertGetId([
                    'id'    =>  $orgUser->id,
                    'name'  =>  $orgUser->name,
                    'phone'  =>  $orgUser->mobile,
                    'is_enable'  =>  $is_enable,
                    'ding_user_id'  =>  $orgUser->user_id,
                    'py'  =>  $orgUser->py_first,
                    'pinyin'  =>  $orgUser->py_all,
                    'is_on_job'  =>  $is_on_job,
                    'created_at'  =>  $orgUser->create_time,
                    'updated_at'  =>  $orgUser->update_time,
                ]);
                $user = Users::find($userId);

                //关联用户和部门关系
                $user->orgs()->sync((array)$orgId);

                //关联用户和角色/职位关系
                $roleId = $this->associateRole($orgUser->position_id);
                $user->roles()->sync((array)$roleId);
            }


            $this->info("同步完成 - {$org->name}.");
        }

        Organizations::rebuild(true);
        $this->info("~部门索引重构完成~");

        Positions::rebuild(true);
        $this->info("~职级索引重构完成~");

    }
}
