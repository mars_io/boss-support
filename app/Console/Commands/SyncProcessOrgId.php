<?php

namespace App\Console\Commands;

use App\Models\Houses;
use App\Models\Organizations;
use App\Models\Process;
use App\Models\Users;
use Illuminate\Console\Command;

class SyncProcessOrgId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process_org_id:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步Process中的org_id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Process::chunk(100, function($processes){
            foreach ($processes as $process) {
                $user = Users::find($process->user_id);
                if ($user && $user->orgs->isNotEmpty()) {
                    $org_id = $user->orgs[0]->id;
                } else {
                    $org_id = NULL;
                }
                if ($org_id) {
                    $res = $process->update(['org_id' => $org_id]);
                    $this->info("{$process->id} :org_id同步成功");
                }else {
                    $this->info("{$process->id} :org_id同步失败");
                }
            }
        });

    }
}
