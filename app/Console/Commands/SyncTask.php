<?php

namespace App\Console\Commands;

use App\Models\Process;
use App\Models\Tasks;
use Illuminate\Console\Command;

class SyncTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '修复待办中包含的已撤销，已拒绝数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $processes = Process::whereNotNull('finish_at')->get();

        foreach ($processes as $process) {
            $tasks = Tasks::where('flow_id', $process->id)
                ->whereNull('finish_at')->get();

            $finish_type = array_keys($process->place)[0];

            foreach ($tasks as $task) {
                Tasks::find($task->id)->update([
                    'finish_at' => $process->created_at,
                    'finish_type'   => $finish_type
                    ]);
                $this->info("{$task->title} 待办筛选更新完成");
            }
        }
    }
}
