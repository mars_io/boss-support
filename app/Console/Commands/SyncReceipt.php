<?php

namespace App\Console\Commands;

use App\Models\ContractRenter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncReceipt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'receipt:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步收据编号，将编号字符串同步为Json数组';

    protected $city = [
        'NJ'    =>  '南京市',
        'HZ'    =>  '杭州市',
        'CQ'    =>  '重庆市',
        'CD'    =>  '成都市',
        'SZ'    =>  '苏州市',
        'HF'    =>  '合肥市',
        'XA'    =>  '西安市',

    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bt = DB::connection('bt');
        $renters = $bt->table('contract_renter')->get();
        foreach ($renters as $renter) {
            $cr = ContractRenter::find($renter->id);
            if ($cr) {
                $receipt = $renter->receipt_number;
                if (starts_with(strtoupper($renter->receipt_number), 'NO')) {
                    $receipt = substr($receipt,2);
                }
                if ($receipt) {
                    $cr->update([
                        'receipt_number' => (array)$receipt
                    ]);
                }

            }
        }
    }
}
