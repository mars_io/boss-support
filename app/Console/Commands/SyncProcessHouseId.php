<?php

namespace App\Console\Commands;

use App\Models\Houses;
use App\Models\Process;
use Illuminate\Console\Command;

class SyncProcessHouseId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process_house_id:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步Process中的house_id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $processes = Process::where('processable_type','bulletin_quality')->get();
        foreach ($processes as $process) {
            $address = $process->content['address'];
            $house = Houses::where('name', $address)->first();
            if ($house) {
                $res = Process::find($process->id)->update(['house_id' => $house->id]);
                $this->info("{$address} 房屋id同步成功");
            }else {
                $this->info("{$address} 房屋id同步失败");
            }
        }
    }
}
