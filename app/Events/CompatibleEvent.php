<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class CompatibleEvent
{

    public $isRent;

    public $contract_id;

    public $process;

    public $process_type;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($isRent, $contract_id, $process, $process_type)
    {
        $this->isRent = $isRent;
        $this->contract_id = $contract_id;
        $this->process = $process;
        $this->process_type = $process_type;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
