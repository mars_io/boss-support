<?php

namespace App\Events;

use App\Models\Users;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Http\Request;

class UserUpdateEvent
{
    public $request;

    public $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Request $request, $user_id)
    {
        $this->request = $request;
        $this->user_id = $user_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
