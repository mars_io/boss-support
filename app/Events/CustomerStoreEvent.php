<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class CustomerStoreEvent
{
    public $process;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($process)
    {
        $this->process = $process;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
