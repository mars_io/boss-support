<?php

namespace App\Events\ReportsEvents;

use App\Models\Users;
use Illuminate\Broadcasting\PrivateChannel;


class XuzubaobeiEvent
{

    public $originEvent;

    public $taskRole;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($originEvent, $taskRole)
    {
        $this->originEvent = $originEvent;
        $this->taskRole = $taskRole;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
