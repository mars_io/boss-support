<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class RoomStoreEvent
{
    public $process;

    public $house;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($process, $house)
    {
        $this->process = $process;
        $this->house    =   $house;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
