<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class ContractLordStoreEvent
{
    public $process;
    public $house;
    public $customer;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($house, $customer, $process)
    {
        $this->process = $process;
        $this->house = $house;
        $this->customer = $customer;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
