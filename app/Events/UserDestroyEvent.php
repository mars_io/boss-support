<?php

namespace App\Events;

use App\Models\Users;
use Illuminate\Broadcasting\PrivateChannel;

class UserDestroyEvent
{
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Users $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
