<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class OrganizationUpdateEvent
{
    public $request;

    public $org_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($request, $org_id)
    {
        $this->request = $request;
        $this->org_id = $org_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
