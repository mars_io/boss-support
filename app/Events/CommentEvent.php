<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class CommentEvent
{
    public $processUser;

    public $processId;

    public $commentUser;

    public $comment;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($commentUser, $processUser, $comment, $processId)
    {
        $this->commentUser = $commentUser;
        $this->processUser = $processUser;
        $this->comment = $comment;
        $this->processId = $processId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
