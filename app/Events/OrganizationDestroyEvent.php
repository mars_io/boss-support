<?php

namespace App\Events;

use App\Models\Organizations;
use Illuminate\Broadcasting\PrivateChannel;

class OrganizationDestroyEvent
{
    public $org_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($org_id)
    {
        $this->org_id = $org_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
