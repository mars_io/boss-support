<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class Boss3Event
{

    public $process;

    public $process_id;

    public $process_type;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($process, $process_id, $process_type)
    {
        $this->process = $process;
        $this->process_id = $process_id;
        $this->process_type = $process_type;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
