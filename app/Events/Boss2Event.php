<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class Boss2Event
{

    public $ableType;

    public $model;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ableType, $model)
    {
        $this->ableType = $ableType;
        $this->model = $model;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
