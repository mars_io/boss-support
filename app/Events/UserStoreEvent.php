<?php

namespace App\Events;

use App\Models\Users;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Support\Facades\Request;

class UserStoreEvent
{
    public $request;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
