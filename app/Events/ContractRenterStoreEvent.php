<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class ContractRenterStoreEvent
{
    public $process;

    public $customer;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($customer, $process)
    {
        $this->customer = $customer;
        $this->process = $process;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
