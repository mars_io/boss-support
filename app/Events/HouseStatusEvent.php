<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;

class HouseStatusEvent
{
    public $report;

    public $house_id;

    public $contract_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($report, $house_id, $contract_id)
    {
        $this->report = $report;
        $this->house_id = $house_id;
        $this->contract_id = $contract_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
