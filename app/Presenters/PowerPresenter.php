<?php

namespace App\Presenters;

use App\Transformers\PowerTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PowerPresenter
 *
 * @package namespace App\Presenters;
 */
class PowerPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PowerTransformer();
    }
}
