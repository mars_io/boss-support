<?php

namespace App\Presenters;

use App\Transformers\HouseTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class HousePresenter
 *
 * @package namespace App\Presenters;
 */
class HousePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new HouseTransformer();
    }
}
