<?php

namespace App\Exceptions;

use Carbon\Exceptions\InvalidDateException;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof AbortException) {
            Log::info("AbortExc:" . $exception->getFile() . '---' . $exception->getLine() . '---' .$exception->getMessage());
            return response()->json([
                'success' => 'fail',
                'status_code' => 451,
                //'message'    =>  $exception->getFile() . '---' . $exception->getLine() . '---' .$exception->getMessage(),
                'message'    =>  $exception->getMessage(),
                'data'  =>  []

            ], $exception->getStatusCode());
        }

        if ($exception instanceof ValidationException) {
            Log::info("AbortExc:" . $exception->getFile() . '---' . $exception->getLine() . '---' .$exception->getMessage());
            return response()->json([
                'status' => 'fail',
                'status_code' => 451,
                'message' => $exception->errors(),
                'data' => []
            ]);
        }
        return parent::render($request, $exception);
    }
}
