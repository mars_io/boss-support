<?php

namespace App\Models;

use App\Http\Resources\UsersResource;
use Brexis\LaravelWorkflow\Traits\WorkflowTrait;
use BrianFaust\Commentable\Traits\HasComments;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


/**
 * App\Models\Report
 *
 * @mixin \Eloquent
 */
class Process extends Model
{
    use WorkflowTrait;
    use HasComments;
    use SearchableTrait;
    use SoftDeletes;
    use LogsActivity;

    protected $table = 'process';
    protected $dates = ['deleted_at'];

    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $casts = [
        'place' => 'array',
        'content'   => 'array'
    ];

    protected $fillable = [
        'user_id',
        'org_id',
        'content',
        'finish_at',
        'place',
        'processable_id',
        'processable_type',
        'house_id'
    ];

    protected $searchable = [
        'columns' => [
            'houses.name' => 10,
//            'houses.pinyin' => 10,
//            'houses.py' => 10,
            'users.name'    => 10,
//            'users.pinyin'  => 10,
//            'users.py'      => 10
        ],
        'joins' => [
            'houses' => ['process.house_id','houses.id'],
            'users' => ['process.user_id', 'users.id']
        ],
    ];

    public function tasks(){
        return $this->hasMany("App\Models\Tasks", 'flow_id');
    }

    public function user() {
        return $this->belongsTo("App\Models\Users", 'user_id');
    }


    public function house() {
        return $this->belongsTo("App\Models\Houses", "house_id");
    }

    public function scopeGetPlace()
    {

        if ($this->place) {
            $place['name'] = array_keys($this->place)[0];
            if (str_contains($place['name'], 'cancelled')) {
                $place['status'] = "cancelled";
            }

            if (str_contains($place['name'], 'published')) {
                $place['status'] = "published";
            }

            if (str_contains($place['name'], 'review')) {
                $place['status'] = "review";
            }

            if (str_contains($place['name'], 'rejected')) {
                $place['status'] = "rejected";
            }

        } else {
            $place['name'] = "draft";
            $place['status'] = "draft";
        }

        $place['display_name'] = $this->getPlaceLocal($place['name']);
        if (str_contains($place['name'], '_review')) {
            $role = explode('_', $place['name'])[0];
            if (str_contains($role,'market-') && $role == 'market-marketing-manager') {
                $place['auditors'] = Users::find($this->user_id)->orgs[0]->leader()->get();
            } else {
                $place['auditors'] = Users::whereHas('roles', function ($roleQuery) use ($role) {
                    $roleQuery->where('name', $role);
                })->get();
            }
        }

        return $place;
    }

    public function getPlaceLocal($place) {

        $holder = [
            'draft' => '草稿',
            'market-reserve-manager_review'   => '片区经理审批中',
            'market-marketing-manager_review'   => '片区经理审批中',
            'market-marketing-regional_review'   => '区长审批中',
            'market-reserve-regional_review'   => '区长审批中',
            'data-online-officer_review'    => '审核员审批中',
            'appraiser-officer_review'    => '产品评审审批中',
            'finance-officer_review' => '财务专员审批中',
            'finance-master_review' => '财务主管审批中',
            'verify-officer_review'    =>  '财务专员审批中',
            'verify-manager_review'     =>  '核算经理审批中',
            'fund-master_review'        =>  '资金主管审批中',

            'market-reserve-manager_rejected' => '片区经理已拒绝',
            'market-marketing-manager_rejected' => '片区经理已拒绝',
            'market-marketing-regional_rejected' => '区长已拒绝',
            'market-reserve-regional_rejected' => '区长已拒绝',
            'data-online-officer_rejected'  => '审核员已拒绝',
            'appraiser-officer_rejected'    => '产品评审已拒绝',
            'finance-officer_rejected'   => '财务专员已拒绝',
            'finance-master_rejected' => '财务主管已拒绝',
            'verify-officer_rejected'    =>  '财务专员已拒绝',
            'verify-manager_rejected'     =>  '核算经理已拒绝',
            'fund-master_rejected'        =>  '资金主管已拒绝',

            'published'         => '审批已通过',
            'cancelled'         => '审批已撤销'
        ];

        return $holder[$place];
    }

    public function scopeGetTasks($query) {

        $tasks = Tasks::where('flow_id', $this->id)
            ->orderBy('id', 'asc')
            ->get();
        return $tasks;
    }

    public function setCurrentPlaceAttribute($value)
    {
        $this->attributes['place'] = json_encode($value);
    }

    public function getCurrentPlaceAttribute()
    {
        if (isset($this->attributes['place'])) {
            return json_decode($this->attributes['place'], true);
        }
    }



}
