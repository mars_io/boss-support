<?php

namespace App\Models;

use Baum\Node;
use Spatie\Activitylog\Traits\LogsActivity;

class Positions extends Node
{

    use LogsActivity;

    protected $table = 'positions';
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = ['name', 'type', 'parent_id'];

    public function roles() {
        return $this->hasOne("App\Models\Role", 'position_id');
    }

    public function positiontypes() {
        return $this->belongsTo("App\Models\PositionTypes", 'type');
    }



}
