<?php

namespace App\Models;

use App\Http\Resources\UsersResource;
use Brexis\LaravelWorkflow\Traits\WorkflowTrait;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Report
 *
 * @mixin \Eloquent
 */
class Tasks extends Model
{
    use WorkflowTrait;
    use SearchableTrait;
    use LogsActivity;

    protected $table = 'tasks';

    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $casts = [
        'is_cc' => 'bool'
    ];

    protected $fillable = [
        'flow_id',
        'flow_type',
        'user_id',
        'house_id',
        'title',
        'finish_type',
        'begin_at',
        'notice_at',
        'finish_at',
        'read_at',
        'important_level',
        'is_cc'
        ];

    protected $searchable = [
        'columns' => [
            'houses.name' => 10,
            'houses.pinyin' => 10,
            'houses.py' => 10,
//            'users.name'    => 10,
//            'users.pinyin'  => 10,
//            'users.py'      => 10
        ],
        'joins' => [
            'houses' => ['tasks.house_id','houses.id'],
//            'users' => ['tasks.user_id', 'users.id']
        ],
    ];

    public function process() {
        return $this->belongsTo('App\Models\Process', 'flow_id');
    }

    public function user() {
        return $this->belongsTo("App\Models\Users", 'user_id');
    }

    public function scopeGetFlow($query) {

        $process = Process::where('id', $this->flow_id)->first();
        $process->user = new UsersResource(Users::where('id', $this->user_id)->first());
        $process->place = $process->getPlace();
        $process->content = json_decode($process->content, true);
        return $process;
    }



}
