<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;


class Files extends Model
{
    use SoftDeletes;
    //use LogsActivity;

    protected $table = "files";

//    protected static $logFillable = true;
//    protected static $logOnlyDirty = true;

    protected $fillable = [
        'name',
        'display_name',
        'raw_name',
        'info',
        'user_id',
        'uri'
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'info' => 'array'
    ];

}
