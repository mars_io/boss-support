<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PositionTypes extends Model
{

    use LogsActivity;

    protected $table = 'position_type';
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = ['name', 'org_id'];

    /**
     * 获得此角色下的所有用户
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function positions()
    {
        return $this->hasMany("App\Models\Positions", 'type');
    }

    public function orgs() {
        return $this->belongsTo("App\Models\Organizations", 'org_id');
    }

}
