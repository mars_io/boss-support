<?php

namespace App\Models;

use Brexis\LaravelWorkflow\Traits\WorkflowTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\Report
 *
 * @mixin \Eloquent
 */
class Reports extends Model
{
    use WorkflowTrait;
    use SoftDeletes;
    protected $table = 'reports';
    protected $dates = ['deleted_at'];

    protected $casts = [
        'place' => 'array',
        'content' => 'array'
    ];

    protected $fillable = ['house_id', 'user_id', 'content'];

    public function setCurrentPlaceAttribute($value)
    {
        $this->attributes['currentPlace'] = \GuzzleHttp\json_encode($value);
    }

    public function getCurrentPlaceAttribute()
    {
        if (isset($this->attributes['currentPlace'])) {
            return \GuzzleHttp\json_decode($this->attributes['currentPlace'], true);
        }
    }

}
