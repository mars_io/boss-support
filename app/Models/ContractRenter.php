<?php

namespace App\Models;

use Brexis\LaravelWorkflow\Traits\WorkflowTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class ContractRenter extends Model
{
    use SearchableTrait;
    use WorkflowTrait;
    use SoftDeletes;
    use LogsActivity;

    protected $table = "contract_renter";

    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'contract_number',
        'rentable_id',
        'rentable_type',
        'type',
        'sign_month',
        'sign_remainder_day',
        'sign_at',
        'start_at',
        'end_at',
        'end_real_at',
        'end_type',
        'end_handover_id',
        'duration_days',
        'month_price',
        'mortgage_price',
        'purchase_way',
        'pay_way',
        'pay_account_info',
        'is_agency',
        'agency_info',
        'penalty_price',
        'property_payer',
        'property_price',
        'money_sum',
        'money_table',
        'final_payment_at',
        'receipt_number',
        'is_corp',
        'is_joint',
        'album',
        'remark',
        'doc_status',
        'visit_status',
        'user_id',
        'org_id',
        'city_name',
        'generate_from',
        'sign_user_id',
        'sign_org_id',
        'remark_clause',
        'boss2_contract_id'
    ];

    protected $casts = [
        'month_price' => 'array',
        'pay_way' => 'array',
        'pay_account_info' => 'array',
        'agency_info' => 'array',
        'album' => 'array',
        'receipt_number'    =>  'array',
        'doc_status'    =>  'array',
        'visit_status' =>   'array',
        'money_table'  =>   'array'
    ];

    protected $dates = [
        'end_at',
        'sign_at',
        'start_at',
        'deleted_at'
    ];

    protected $searchable = [
        'columns' => [
            'contract_renter.contract_number' => 10,
            'houses.name' => 10,
            'houses.pinyin' => 10,
            'houses.py' => 10,
//            'customers.name' => 10,
//            'customers.py' => 10,
//            'customers.pinyin' => 10,
//            'customers.phone' => 10,
//            'users.name'    => 10,
//            'users.pinyin'  => 10,
//            'users.py'      => 10
        ],

        'joins' => [
            'houses' => ['contract_renter.rentable_id','houses.id'],
//            'customer_renter' => ['contract_renter.id', 'customer_renter.renter_id'],
//            'customers' => ['customer_renter.customer_id', 'customers.id'],
//            'users' => ['contract_renter.sign_user_id', 'users.id']
        ],
    ];

    protected $appends = [
        'status'
    ];

    /**
     * 获取拥有此租房合同的模型
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function rentable() {

        return $this->morphTo();
    }

    public function user() {
        return $this->belongsTo("App\Models\Users", 'user_id');
    }

    public function sign_user() {
        return $this->belongsTo("App\Models\Users", 'sign_user_id');
    }

    public function sign_org() {
        //TODO: 临时关联org_id，改为负责部门，此处关联原为开单部门
        return $this->belongsTo("App\Models\Organizations", 'org_id');
    }


    /**
     * 获取此租房合同下的所有客户
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function customers() {

        return $this->belongsToMany("App\Models\Customers", 'customer_renter', 'renter_id', 'customer_id');
    }


    public function agency_contract() {

        return $this->MorphOne("App\Models\Agency", "contract_able");
    }


    public function setDocStatusAttribute($value)
    {
        $this->attributes['doc_status'] = json_encode($value);
    }

    public function getDocStatusAttribute()
    {
        if (isset($this->attributes['doc_status'])) {
            return json_decode($this->attributes['doc_status'], true);
        }
    }

    public function setVisitStatusAttribute($value)
    {
        $this->attributes['visit_status'] = json_encode($value);
    }

    public function getVisitStatusAttribute()
    {
        if (isset($this->attributes['visit_status'])) {
            return json_decode($this->attributes['visit_status'], true);
        }
    }

    public function getSignAtAttribute(){
        if (isset($this->attributes['sign_at'])){
            return date("Y-m-d", strtotime($this->attributes['sign_at']));
        }
    }

    public function getStartAtAttribute(){
        if (isset($this->attributes['start_at'])){
            return date("Y-m-d", strtotime($this->attributes['start_at']));
        }
    }

    public function getEndAtAttribute(){
        if (isset($this->attributes['end_at'])){
            return date("Y-m-d", strtotime($this->attributes['end_at']));
        }
    }

    public function getEndRealAtAttribute(){
        if (isset($this->attributes['end_real_at'])){
            return date("Y-m-d", strtotime($this->attributes['end_real_at']));
        }
    }

    public function getFinalPaymentAtAttribute(){
        if (isset($this->attributes['final_payment_at'])){
            return date("Y-m-d", strtotime($this->attributes['final_payment_at']));
        }
    }


    /**
     * 获取合同状态，//1:未签约， 2：已签约， 3：快到期（60天内）， 4：已结束
     * @return int
     */
    public function getStatusAttribute(){


        if (isset($this->attributes['end_real_at'])){
            //已经结束
            if ($this->attributes['end_real_at'] !== NULL) {
                return 4;
            }
        }

        if (isset($this->attributes['contract_number'])) {
            if (!$this->attributes['contract_number']) {
                return 1;
            }

        }

        if ($this->attributes['contract_number']) {
            $end_timestamp = strtotime($this->attributes['end_at']) - strtotime(date("Y-m-d", time()));
            $diffDays = $end_timestamp/3600/24;
            if ($end_timestamp < 0) {
                return 5;
            }

            if ($end_timestamp > 0) {
                if ($diffDays < 60) {
                    return 3;
                }else {
                    return 2;
                }
            }
        }
    }


}
