<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;


class Agency extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected $table = "agency";
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'house_id',
        'contract_able_id',
        'contract_able_type',
        'agency_price',
        'agency_before_price',
        'agency_name',
        'agency_username',
        'agency_phone',
        'agency_account_info',
        'album',
        'remark',
        'user_id',
        'sign_user_id',
        'sign_org_id'
    ];

    protected $casts = [
        'album' => 'array',
        'agency_account_info' => 'array',
    ];

    public function user() {
        return $this->belongsTo("App\Models\Users", 'user_id');
    }

    public function sign_user() {
        return $this->belongsTo("App\Models\Users", 'sign_user_id');
    }

    public function sign_org() {
        return $this->belongsTo("App\Models\Organizations", 'sign_org_id');
    }

    /**
     * 获取拥有此中介费的收租合同
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function contract_able() {

        return $this->morphTo();
    }

}
