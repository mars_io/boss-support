<?php

namespace App\Models;

use Brexis\LaravelWorkflow\Traits\WorkflowTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class ContractLord extends Model
{
    use SearchableTrait;
    use WorkflowTrait;
    use SoftDeletes;
    use LogsActivity;

    protected $table = "contract_lord";

    protected $dates = ['deleted_at'];

    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'house_id',
        'contract_number',
        'type',
        'first_pay_at',
        'second_pay_at',
        'sign_month',
        'sign_remainder_day',
        'sign_at',
        'start_at',
        'end_at',
        'end_real_at',
        'end_type',
        'end_handover_id',
        'mortgage_price',
        'penalty_price',
        'ready_days',
        'vacancy_end_date',
        'vacancy_way',
        'vacancy_other',
        'purchase_way',
        'duration_days',
        'month_price',
        'pay_way',
        'pay_account_info',
        'is_agency',
        'property_payer',
        'property_price',
        'guarantee_days',
        'is_corp',
        'is_agency',
        'is_joint',
        'agency_info',
        'album',
        'remark',
        'doc_status',
        'visit_status',
        'user_id',
        'org_id',
        'city_name',
        'generate_from',
        'sign_user_id',
        'sign_org_id',
        'remark_clause',
        'boss2_contract_id'
    ];

    protected $casts = [
        'month_price' => 'array',
        'pay_way' => 'array',
        'pay_account_info' => 'array',
        'album' => 'array',
        'agency_info' => 'array',
        'doc_status'    =>  'array',
        'visit_status' =>   'array',
    ];

    protected $searchable = [
        'columns' => [
            'contract_lord.contract_number' => 10,
            'houses.name' => 10,
            'houses.pinyin' => 10,
            'houses.py' => 10,
//            'customers.name' => 10,
//            'customers.py' => 10,
//            'customers.pinyin' => 10,
//            'customers.phone' => 10,
//            'users.name'    => 10,
//            'users.pinyin'  => 10,
//            'users.py'      => 10
        ],
        'joins' => [
            'houses' => ['contract_lord.house_id','houses.id'],
//            'customer_lord' => ['contract_lord.id', 'customer_lord.lord_id'],
//            'customers' => ['customer_lord.customer_id', 'customers.id'],
//            'users' => ['contract_lord.sign_user_id', 'users.id']
        ],

    ];

    protected $appends = [
        'status'
    ];

    /**
     * 获取此收房合同的关联房屋
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function house() {

        return $this->belongsTo("App\Models\Houses", "house_id");
    }

    /**
     * 获取此收房合同下的所有客户
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function customers() {

        return $this->belongsToMany("App\Models\Customers", 'customer_lord', 'lord_id', 'customer_id');
    }

    public function user() {
        return $this->belongsTo("App\Models\Users", "user_id");

    }

    public function sign_user() {
        return $this->belongsTo("App\Models\Users", "sign_user_id");
    }

    public function sign_org() {
        //TODO: 临时关联org_id，改为负责部门，此处关联原为开单部门
        return $this->belongsTo("App\Models\Organizations", 'org_id');
    }


    public function agency_contract() {

        return $this->MorphOne("App\Models\Agency", "contract_able");
    }


    public function setDocStatusAttribute($value)
    {
        $this->attributes['doc_status'] = json_encode($value);
    }

    public function getDocStatusAttribute()
    {
        if (isset($this->attributes['doc_status'])) {
            return json_decode($this->attributes['doc_status'], true);
        }
    }

    public function setVisitStatusAttribute($value)
    {
        $this->attributes['visit_status'] = json_encode($value);
    }

    public function getVisitStatusAttribute()
    {
        if (isset($this->attributes['visit_status'])) {
            return json_decode($this->attributes['visit_status'], true);
        }
    }

    public function getFirstPayAtAttribute(){
        if (isset($this->attributes['first_pay_at'])){
            return date("Y-m-d", strtotime($this->attributes['first_pay_at']));
        }
    }

    public function getSecondPayAtAttribute(){
        if (isset($this->attributes['second_pay_at'])){
            return date("Y-m-d", strtotime($this->attributes['second_pay_at']));
        }
    }

    public function getSignAtAttribute(){
        if (isset($this->attributes['sign_at'])){
            return date("Y-m-d", strtotime($this->attributes['sign_at']));
        }
    }

    public function getStartAtAttribute(){
        if (isset($this->attributes['start_at'])){
            return date("Y-m-d", strtotime($this->attributes['start_at']));
        }
    }

    public function getEndAtAttribute(){
        if (isset($this->attributes['end_at'])){
            return date("Y-m-d", strtotime($this->attributes['end_at']));
        }
    }

    public function getEndRealAtAttribute(){
        if (isset($this->attributes['end_real_at'])){
            return date("Y-m-d", strtotime($this->attributes['end_real_at']));
        }
    }

    /**
     * 获取合同状态，//1:未签约， 2：已签约， 3：快到期（60天内）， 4：已结束
     * @return int
     */
    public function getStatusAttribute(){

        if (isset($this->attributes['end_real_at'])){
            //已经结束
            if ($this->attributes['end_real_at'] !== NULL) {
                return 4;
            }
        }

        if ($this->attributes['contract_number'] === NULL) {
            return 1;
        }

        if ($this->attributes['contract_number']) {
            $end_timestamp = strtotime($this->attributes['end_at']) - strtotime(date("Y-m-d", time()));
            $diffDays = $end_timestamp/3600/24;

            if ($end_timestamp < 0) {
                return 5;
            }

            if ($end_timestamp > 0) {
                if ($diffDays < 60) {
                    return 3;
                }else {
                    return 2;
                }
            }
        }
    }

}
