<?php

namespace App\Models;

use Laratrust\Models\LaratrustPermission;
use Spatie\Activitylog\Traits\LogsActivity;

class Permission extends LaratrustPermission
{

    use LogsActivity;

    protected $fillable = ['mod_id', 'name', 'display_name', 'description', 'type'];

    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * 获得此权限关联的所有用户
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany('App\Models\Users', 'permission_user', 'permission_id', 'user_id');
    }

    /**
     * 获得此权限关联的所有角色
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles() {
        return $this->belongsToMany('App\Models\Role', 'permission_role', 'permission_id', 'role_id');
    }
}
