<?php

namespace App\Models;

use App\Http\Resources\OrganizationsResource;
use App\Http\Resources\OrganizationsResourceCollection;
use Baum\Node;
use BrianFaust\Commentable\Traits\HasComments;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class Organizations extends Node
{
    use SearchableTrait;
    use SoftDeletes;
    use HasComments;
    use LogsActivity;

    protected $table = 'organizations';
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'name',
        'parent_id',
        'leader_id',
        'ding_department_id',
        'is_enable',
        'order',
        'py',
        'pinyin',
        'created_at',
        'updated_at'
    ];

    protected $searchable = [

        'columns' => [
            'name' => 10,
            'py'   => 10,
            'pinyin' => 10
        ],

    ];

    public function users() {
        return $this->belongsToMany('App\Models\Users', 'org_user', 'org_id', 'user_id');
    }

    public function leader() {
        return $this->belongsTo("App\Models\Users", "leader_id");
    }

    public function roles() {
        return $this->hasMany('App\Models\Role', 'org_id');
    }

    public function positionTypes() {
        return $this->hasMany('App\Models\PositionTypes', 'org_id');
    }

    public function scopeCountOrgUsers($query) {

        $childAndSelf = $this->getDescendantsAndSelf();
        $ids = [];
        foreach ($childAndSelf as $item) {
            $ids[] = $item->id;
        }

        return DB::table('org_user')->whereIn('org_id', $ids)->count();
    }

}
