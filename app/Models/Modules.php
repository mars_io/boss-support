<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Report
 *
 * @mixin \Eloquent
 */
class Modules extends Model
{
    use LogsActivity;

    protected $table = 'modules';
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = ['sys_id', 'name', 'display_name', 'description'];

    public function permissions() {
        return $this->hasMany('App\Models\Permission', 'mod_id');
    }
}
