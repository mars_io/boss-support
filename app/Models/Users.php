<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laratrust\Traits\LaratrustUserTrait;
use Laravel\Passport\HasApiTokens;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Users extends Authenticatable
{
    use LaratrustUserTrait;
    use HasApiTokens;
    use Notifiable;
    use SearchableTrait;
    use SoftDeletes;
    use LogsActivity;

    protected $table = 'users';

    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'phone',
        'avatar',
        'ding_user_id',
        'is_enable',
        'is_on_job',
        'py',
        'pinyin',
        'password',
        'phone_check_code_ttl'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $searchable = [

        'columns' => [
            'name'  => 10,
            'py'    => 10,
            'pinyin'=> 10,
            'phone' => 10
        ],

    ];


    public function orgs() {
        return $this->belongsToMany('App\Models\Organizations', 'org_user', 'user_id', 'org_id');
    }

    public function roles() {
        return $this->belongsToMany('App\Models\Role', 'role_user', 'user_id', 'role_id');
    }

    public function permissions() {
        return $this->belongsToMany('App\Models\Permission', 'permission_user', 'user_id', 'permission_id');
    }

    /**
     * 多类型账号自动判断
     * @param $username
     * @return mixed
     */
    public function findForPassport($username) {

        return self::where('phone', $username)->first();
    }

    /**
     * 获取多个角色下的所有用户
     * @param $roles
     * @return array
     */
    public static function getRoleUsers($roles) {

        $user_ids = [];
        foreach ($roles as $role) {
            $users = Users::whereRoleIs($role)->get();
            foreach ($users as $user) {
                $user_ids[] = $user->id;
            }
        }
        return array_unique($user_ids);
    }


    /**
     * 获取用户的领导
     *
     * @param $level
     */
    public function getUserLeader($level = NULL) {
        //$role = $this->roles()->first();
        $org = $this->orgs()->first();
        $users = $org->users;
        $myRole = $this->roles()->first()->name;
        $roles = [];
        foreach ($users as $user) {
            //市场部
            if (str_contains($myRole, 'market-')) {
                if ($user->roles()->first()->name == "market-manager") {
                    return $user;
                }
            }

            //财务部
            if (str_contains($myRole, 'finance-')) {
                if ($user->roles()->first()->name == "finance-master") {
                    return $user;
                }
            }

            //人资线下合同
            if (str_contains($myRole, 'human-contract-offline')) {
                if ($user->roles()->first()->name == "human-contract-offline-master") {
                    return $user;
                }
            }

            //人资线上合同
            if (str_contains($myRole, 'human-contract-online')) {
                if ($user->roles()->first()->name == "human-contract-online-master") {
                    return $user;
                }
            }
        }
        return Null;
    }

    public function scopeCountOrgUsers($query, $org_id) {

        $node = Organizations::find($org_id);
        $childAndSelf = $node->getDescendantsAndSelf();
        $ids = [];
        foreach ($childAndSelf as $item) {
            $ids[] = $item->id;
        }

        return $query->whereIn('org_id', $ids)->count();
    }

    public function scopeGetOrgUsers($query, $org_id) {
        $node = Organizations::find($org_id);
        $childAndSelf = $node->getDescendantsAndSelf();
        $ids = [];
        foreach ($childAndSelf as $item) {
            $ids[] = $item->id;
        }
        return array_unique(DB::table('org_user')->whereIn('org_id', $ids)->pluck('user_id')->toArray());
    }

}
