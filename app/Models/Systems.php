<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Report
 *
 * @mixin \Eloquent
 */
class Systems extends Model
{
    use LogsActivity;

    protected $table = 'systems';

    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = ['name', 'display_name', 'description'];

    public function modules() {

        return $this->hasMany('App\Models\Modules', 'sys_id');
    }

}
