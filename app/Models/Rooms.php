<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Rooms extends Model
{
    use LogsActivity;

    protected $table = "rooms";

    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = ['house_id', 'name'];

    /**
     * 获取此房间的房屋
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function house() {

        return $this->belongsTo("App\Models\Houses", 'house_id');
    }

    /**
     * 获取此房间的所有租房合同
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function contract_renters() {

        return $this->morphMany("App\Models\ContractRenter", "rentable");
    }

}
