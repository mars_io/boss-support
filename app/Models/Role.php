<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends LaratrustRole
{

    use LogsActivity;

    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = ['name', 'display_name', 'description','org_id', 'type'];

    /**
     * 获得此角色下的所有用户
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\Users', 'role_user', 'role_id', 'user_id');
    }

    /**
     * 获取此角色下的所有权限
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permission() {
        return $this->belongsToMany('App\Models\Permission', 'permission_user', 'user_id', 'permission_id');
    }


    public function position() {
        return $this->belongsTo("App\Models\Positions", "position_id");
    }

    public function getRoleMap($role) {

        $roles = [
            'market'    =>  [
                'market-commissioner'    =>  '市场专员',
                'market-reserve-manager'    =>  '储备片区经理',
                'market-manager'    =>  '片区经理',
                'market-reserve-region'    =>  '储备区域经理',
                'market-region'    =>  '区域经理',
                'market-city'       =>  '城市总'
            ],

            'finance'   =>  [
                'finance-accountant'    =>  '会计',
                'finance-cashier'       =>  '出纳',
                'finance-ledger'        =>  '总账',
                'finance-commissioner'  =>  '财务专员',
                'finance-master'  =>  '财务主管',
                'finance-manager'  =>  '财务经理'
            ],

            //人力，行政，招聘
            'human'     =>  [

                'human-commissioner'    =>  '人事专员',
                'human-chief-assistant'    =>  '人力总监助理',
                'human-chief'    =>  '人力总监',

                'human-recruit-commissioner'  =>  '招聘专员',
                'human-recruit-master'  =>  '招聘主管',

                'human-receptionist-commissioner' =>  '行政专员',
                'human-receptionist-master' =>  '行政主管',

                'human-contract-offline-master' =>  '合同线下主管',
                'human-contract-offline-commissioner' =>  '合同线下专员',

                'human-contract-online-master' =>  '合同线上主管',
                'human-contract-online-commissioner' =>  '合同线上专员',


            ],


            'customer'  =>  [
                'customer-manager'  =>  '客服经理',
                'customer-commissioner-online'  =>  '在线客服专员',
                'customer-commissioner-phone'  =>  '电话客服专员',
                'customer-backend'  =>  '后台专员',
                'customer-operation-commissioner'  =>  '运维专员',
                'customer-operation-master'  =>  '运维主管',
                'customer-checkout-commissioner'  =>  '结算专员',
            ],

            'hotel'     =>  [
                'hotel-manager' =>  '酒店经理',
                'hotel-commissioner' =>  '酒店专员',
                'hotel-foreman' =>  '酒店领班',
            ],

            'dev'       =>  [
                'dev-commissioner'   =>  '工程师',
            ],

            'college'   =>  [
                'college-commissioner'  =>  '讲师专员',
                'college-principal'  =>  '讲师校长',
            ],

            'president' =>  [
                'president-commissioner'    =>  '总裁办',
                'president-assistant'    =>  '总裁办助理',
            ],

            'media'     =>  [
                'media-commissioner'    =>  '新媒体'
            ],

            'house'     =>  [
                'house-commissioner'    =>  '管控专员'
            ]

        ];



    }
}
