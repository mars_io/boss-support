<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class Customers extends Model
{
    use SearchableTrait;
    use LogsActivity;

    protected $table = "customers";
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'name',
        'phone',
        'sex',
        'idtype',
        'idcard',
        'is_corp',
        'is_agent',
        'user_id',
        'org_id',
        'py',
        'pinyin'
    ];

    protected $searchable = [
        'columns' => [
            'customers.name' => 10,
            'customers.phone' => 10,
            'customers.idcard' => 10,
            'customers.py'   => 10,
            'customers.pinyin' => 10,
            //'contract_renter.contract_number' => 10,
            //'contract_lord.contract_number' => 10,
        ],
        'joins' => [
            //'customer_renter' => ['contract_renter.id', 'customer_renter.renter_id'],
            //'contract_renter' => ['customer_renter.renter_id', 'contract_renter.id'],
            //'customer_lord' => ['contract_lord.id', 'customer_lord.lord_id'],
            //'contract_lord' => ['customer_lord.lord_id', 'contract_lord.id'],
        ],

    ];

    public function user() {
        return $this->belongsTo("App\Models\Users", 'user_id');
    }

    /**
     * 获取此客户下的所有收房合同
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contract_lords() {

        return $this->belongsToMany("App\Models\ContractLord", 'customer_lord', 'customer_id', 'lord_id');
    }

    /**
     * 获取此客户下的所有租房合同
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contract_renters() {

        return $this->belongsToMany("App\Models\ContractRenter", 'customer_renter', 'customer_id', 'renter_id');
    }

}
