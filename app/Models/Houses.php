<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class Houses extends Model
{

    use SearchableTrait;
    use SoftDeletes;
    use LogsActivity;

    protected $table = "houses";
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'name',
        'building',
        'unit',
        'house_number',
        'village_name',
        'village_id',
        'room',
        'hall',
        'area',
        'floor',
        'floors',
        'toilet',
        'house_res',
        'decoration',
        'house_identity',
        'rent_type',
        'house_grade',
        'house_state',
        'house_feature',
        'suggest_price',
        'rent_start_at',
        'ready_end_at',
        'again_rent_at',
        'rent_end_than_days',
        'lord_end_at',
        'warning_init_at',
        'lord_start_at',
        'album',
        'user_id',
        'org_id',
        'city_name',
        'is_nrcy',
        'py',
        'pinyin',
        'boss2_house_id'
        ];

    protected $casts = [
        'house_res' => 'array',
        'album' =>  'array',
        'house_state' => 'array'
    ];

    protected $searchable = [
        'columns' => [
            'name' => 10,
            'pinyin' => 10,
            'py' => 10,
        ]
    ];

    protected $appends = [
        'total_ready_days',
        'current_ready_days',
        'is_again_rent',
        'lord_remainder_days',
        'status',
        'warning_status'
    ];


    public function user() {
        return $this->belongsTo("App\Models\Users", "user_id");
    }

    public function org() {
        return $this->belongsTo("App\Models\Organizations", "org_id");
    }

    /**
     * 获取此房屋的所有房间
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms() {
        return $this->hasMany("App\Models\Rooms", 'house_id');
    }

    /**
     * 获取此房屋的所有租房合同
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function contract_renters() {

        return $this->morphMany("App\Models\ContractRenter", "rentable");
    }

    /**
     * 获取此房屋的所有收房合同
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contract_lords() {

        return $this->hasMany("App\Models\ContractLord", 'house_id');
    }

    /**
     * 房屋状态，0：待收房 -> 1：待出租 -> 2：待签约 -> 3：待入住 -> 4：已出租， 5：已结束 -> 1：待出租（回到起点）
     *
     * 涉及字段
     * lord_start_at    收房合同开始时间
     * lord_end_at      收房合同结束时间
     * lord_real_end_at 收房实际结束时间
     * ready_end_at     收房空置期结束时间
     *
     * rent_start_at    租房合同开始时间(每次租房合同第一步审核，更新该字段）
     * rent_end_at      租房合同结束时间（每次租房合同第一步审核，更新该字段）
     * rent_report_at   租房报备时间（每一次报备，并且最终审核通过时， 更新该字段）
     *
     * 0：待收房 (lord_start_at = NULL)     [房屋质量报备时，重置NULL | 收房和续收时，更新该字段]
     *
     * 1：待出租 (rent_start_at = NULL)     [第一步审核通过后，更新该字段 | 租房退租结算时，重置NULL]
     *
     * //2：待签约 (rent_report_at  = NULL)     [报备 -> 第一步合同审核（期间） | 审核通过后，重置为NULL]
     *
     * //3: 待入住 (rent_sign_at < now() < rent_start_at) [签约合同的时候变更 rent_sign_at, 合同上的租房合同开始时间 rent_start_at]
     *
     * 4：已出租 (rent_start_at != NULL)    [(第一步审核通过后)租，转，续，调，更新该字段 | 退租时，重置NULL]
     *
     * 5：已结束 (lord_real_end_at != NULL) [收房退租结算时，更新该字段 | 收房和续收时，重置NULL]
     *
     * @return int
     */
    public function getStatusAttribute(){

        if (!isset($this->attributes['lord_start_at'])) {
            return 2;
        }

        if (isset($this->attributes['rent_start_at'])){
            return $this->attributes['rent_start_at'] ? 1 : 0;
        }

        /*
        //待收房
        if (!isset($this->attributes['lord_start_at'])) {
            return 0;
        }

        //收房后
        if (isset($this->attributes['lord_start_at']) && $this->attributes['lord_start_at'] !== NULL) {

            //待出租
            if (!isset($this->attributes['rent_start_at'])) {
                return 1;
            }

            //已出租
            if (isset($this->attributes['rent_start_at'])) {
                return 4;
            }

            //待签约
            if (isset($this->attributes['rent_report_at'])) {
                return 2;
            }

            //收房已结束
            if (isset($this->attributes['lord_real_end_at'])) {
                return 5;
            }
        }
        //房屋状态未知
        return 100;
        */
    }

    /**
     * 获取房屋预警信息
     * @return int
     */
    public function getWarningStatusAttribute(){
        if (isset($this->attributes['warning_init_at'])){
            $nowtimestamp = strtotime(date("Y-m-d", time()));
            $warning_init_at = strtotime($this->attributes['warning_init_at']);
            $warning_days = ($nowtimestamp - $warning_init_at)/3600/24;
            if ($warning_days <= 7) {
                return 1;
            }
            if ($warning_days > 7 && $warning_days <= 14) {
                return 2;
            }
            if ($warning_days > 14 && $warning_days <= 21) {
                return 3;
            }
            if ($warning_days > 21) {
                return 4;
            }
        }
    }

    /**
     * 设置房屋预警信息
     * @return int
     */
    public function setWarningInitAtAttribute($value){

        switch ((int)$value) {
            case 1:
                $this->attributes['warning_init_at'] = date("Y-m-d", time());
                break;

            case 2:
                $this->attributes['warning_init_at'] = date("Y-m-d", strtotime("-8 days"));
                break;

            case 3:
                $this->attributes['warning_init_at'] = date("Y-m-d", strtotime("-15 days"));
                break;
            case 4:
                $this->attributes['warning_init_at'] = date("Y-m-d", strtotime("-22 days"));
                break;
            default:
                $this->attributes['warning_init_at'] = $value;

        }

        /*if ((int)$value === 1) {
            $this->attributes['warning_init_at'] = date("Y-m-d", time());
        }

        if ((int)$value === 2) {
            $this->attributes['warning_init_at'] = date("Y-m-d", strtotime("-8 days"));
        }

        if ((int)$value === 3) {
            $this->attributes['warning_init_at'] = date("Y-m-d", strtotime("-15 days"));
        }

        if ((int)$value === 4) {
            $this->attributes['warning_init_at'] = date("Y-m-d", strtotime("-22 days"));
        }*/


    }

    /**
     * 获取空置期总天数
     * @return float|int
     */
    public function getTotalReadyDaysAttribute(){

        if (isset($this->attributes['ready_end_at'])){
            $nowtimestamp = strtotime(date("Y-m-d", time()));
            if ($this->attributes['lord_start_at']) {
                if ($nowtimestamp >= strtotime($this->attributes['lord_start_at'])) {

                    $ready_end_at = strtotime($this->attributes['ready_end_at']);
                    $ready_days = ($ready_end_at - $nowtimestamp)/3600/24;
                    return $ready_days;
                }
            }
            return "合同未开始";
        }

    }

    /**
     * 获取当前消耗空置期天数
     * @return float|int
     */
    public function getCurrentReadyDaysAttribute(){

        if (isset($this->attributes['ready_end_at'])){
            $nowtimestamp = strtotime(date("Y-m-d", time()));
            $lord_start_timestamp = strtotime($this->attributes['lord_start_at']);

            if ($lord_start_timestamp) {
                if ($nowtimestamp >= $lord_start_timestamp) {
                    $ready_days = ($nowtimestamp - $lord_start_timestamp)/3600/24;
                    return $ready_days;
                }
            }
            return '合同未开始';
        }
    }

    /**
     * 是否二次出租
     */
    public function getIsAgainRentAttribute(){

    }

    /**
     * 获取收房合同剩余时间
     * @return float|int
     */
    public function getLordRemainderDaysAttribute(){

        if (isset($this->attributes['lord_end_at'])){
            $nowtimestamp = strtotime(date("Y-m-d", time()));
            $lord_end_timestamp = strtotime($this->attributes['lord_end_at']);
            $remainderDays = ($lord_end_timestamp - $nowtimestamp)/3600/24;
            return $remainderDays;
        }
    }





}
