@component('mail::message')
 ## 您有{{$count}}条待办需要处理，请尽快处理哦~

@component('mail::table')
    | 报备类型              | 房屋地址         | 姓名         |  部门        | 报备时间         | 操作     |
    | :- |:--   | :--| :--| :--|
    @foreach ($tasks as $task)
    | {{$task->title}}             | Centered        | {{$task->user->name}}      | {{$task->user->orgs[0]->name}}      | {{$task->created_at}}      |[前往处理](/about/)
    @endforeach

@endcomponent

@endcomponent